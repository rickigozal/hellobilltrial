<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::post('get_gulu_report','GuluGuluReportController@getReport2');

Route::post('convert_account_permission','PermissionConvertController@convertAccountPermission');

Route::post('get_product_account','OnlineRegistrationController@getProduct');
Route::post('get_account_registration','OnlineRegistrationController@getAccountRegistration');
Route::post('get_account_registration_detail','OnlineRegistrationController@getAccountRegistrationDetail');
Route::post('update_online_registration','OnlineRegistrationController@updateOnlineRegistration');

Route::post('get_tax_report_for_bri','ReportController@getTaxReportForBRI');
Route::post('get_branch_for_tax_report','ReportController@getBranchForTaxReport');

Route::post('import_old_quotation','ImportOldQuotationController@importOldQuotationTest');

Route::post('clone_delete_data','CloneController@cloneDeleteData');

Route::post('check_email','CheckEmailController@checkEmail');

Route::post('get_log_transaction','LogTransactionController@getLogTransaction');
Route::post('get_pagination_log_transaction','LogTransactionController@getPaginationLogTransaction');
Route::post('get_branch_for_log_transaction','LogTransactionController@getBranch');

Route::post('get_branch_by_account_id','ManagePermissionController@getBranchByAccountID');
Route::post('get_branch_all_for_permission','ManagePermissionController@getBranchAll');
Route::post('get_branch_by_account_id_detail','ManagePermissionController@getBranchDetail');
Route::post('get_system_permission','ManagePermissionController@getSystemPermission');
Route::post('get_branch_tab','ManagePermissionController@getBranchTab');
Route::post('get_account_for_permission','ManagePermissionController@getAccount');
Route::post('insert_update_account_permission','ManagePermissionController@insertUpdateAccountPermission');
Route::post('get_product_id','ManagePermissionController@getProductID');

Route::post('get_branch_for_copy','CopyMasterController@getBranchByAccountID');
Route::post('get_copy_data_type','CopyMasterController@getCopyDataType');
Route::post('copy_master_data','CopyMasterController@copyMasterData');
Route::post('get_account_for_copy','CopyMasterController@getAccount');

Route::post('get_ticketing_status','TicketingController@getTicketingStatus');
Route::post('get_customer','TicketingController@getCustomer');
Route::post('get_platform','TicketingController@getPlatform');
Route::post('get_ticketing','TicketingController@getTicketing');
Route::post('insert_update_ticketing','TicketingController@InsertUpdateTicket');
Route::post('get_ticketing_detail_by_id','TicketingController@getTicketDetailbyTicketID');
Route::post('get_ticketing_detail','TicketingController@getTicketingDetail');
Route::post('get_report_type','TicketingController@getReportType');
Route::post('get_reporter_user','TicketingController@getReporterUser');
Route::post('get_resolver_user','TicketingController@getResolverUser');

Route::post('get_branch_otp','OtpController@getBranch');
Route::post('get_user_otp','OtpController@getUser');
Route::post('insert_update_otp_request','OtpController@insert');
Route::post('get_otp_request','OtpController@getOTPRequest');
Route::post('get_otp_request_detail','OtpController@getOTPRequestDetail');
Route::post('approve_otp','OtpController@approveOTP');
Route::post('reject_otp','OtpController@rejectOTP');

Route::post('ban_unban_branch','OtpController@banUnbanBranch');
Route::post('get_ban_unban_log','OtpController@getBanUnbanLog');
Route::post('get_ban_unban_history','OtpController@getBanUnbanHistory');
Route::post('get_active_branch','OtpController@getActiveBranch');
Route::post('export_brand_pos','CronBrandController@exportBrandPOS');
Route::post('get_trial_branch','OtpController@getTrialBranch');
Route::post('get_trial_branch_detail','OtpController@getTrialBranchDetail');
Route::post('get_account','OtpController@getAccount');
Route::post('get_account_detail','OtpController@getAccountDetail');
Route::post('insert_update_account','OtpController@insertUpdateAccount');


Route::post('upload_file','PdfController@uploadFile');

Route::post('send_invoice_pdf','PdfController@sendInvoicePDF');
Route::post('send_pdf','BranchController@sendPDF');

Route::post('login','LoginController@login');
Route::post('get_brand_pdf','BrandController@getBrandPDF2');

Route::post('change_password','UserController@changePassword');
Route::post('set_forget_password','ForgetPasswordController@setPassword');
Route::post('forget_password', 'LoginController@forgetPassword');
Route::post('set_password','LoginController@setPassword');

Route::post('get_region','RegionController@getRegion');
Route::post('get_channel','ChannelController@getChannel');

Route::post('get_service','ServiceController@getService');
Route::post('get_service_detail','ServiceController@getServiceDetail');
Route::post('insert_update_service','ServiceController@InsertUpdateService');
Route::post('activate_deactivate_service','ServiceController@ActivateDeactivateService');
Route::post('delete_service','ServiceController@DeleteService');

Route::post('get_other','OtherController@getOther');
Route::post('get_other_detail','OtherController@getOtherDetail');
Route::post('insert_update_other','OtherController@InsertUpdateOther');
Route::post('delete_other','OtherController@DeleteOther');

Route::post('get_permission','UserTypePermissionController@getPermission');
Route::post('get_permission_by_user_type_id','UserTypePermissionController@getPermissionbyUserTypeID');
Route::post('get_menu_permission','UserTypePermissionController@getMenuPermission');

Route::post('get_report','ReportController@getCommission');
Route::post('get_transaction','ReportController@getTransaction');
Route::post('get_transaction_by_user_id','ReportController@getTransactionByUserID');
Route::post('get_transaction_query','ReportController@getTransactionQuery');
Route::post('get_sales_commission','ReportController@getSalesCommission');
Route::post('get_sales_commission_detail','ReportController@getSalesCommissionDetail');
Route::post('get_branch_license','ReportController@getBranchLicense');

Route::post('get_branch','BranchController@getBranch');
Route::post('get_branch_dashboard','BranchController@getBranchDashboard');
Route::post('get_branch_detail', 'BranchController@getBranchDetail');
Route::post('delete_branch', 'BranchController@DeleteBranch');
Route::post('get_branch_by_brand_id','BranchController@getBranchByBrandID');
Route::post('insert_update_branch','BranchController@InsertUpdateBranch');
Route::post('import_branch','BranchController@importBranch');

Route::post('get_license','LicenseController@getLicense');
Route::post('activate_deactivate_license','LicenseController@ActivateDeactivateLicense');
Route::post('get_license_detail','LicenseController@getLicenseDetail');
Route::post('insert_update_license','LicenseController@InsertUpdateLicense');
Route::post('delete_license','LicenseController@DeleteLicense');

Route::post('get_user_type','UserTypeController@getUserType');
Route::post('get_user_type_detail','UserTypeController@getUserTypeDetail');
Route::post('insert_update_user_type','UserTypeController@InsertUpdateUserType');
Route::post('delete_user_type','UserTypeController@DeleteUserType');

Route::post('get_user','UserController@getUser');
Route::post('get_user_passed_pic_true','UserController@getUserPassedPicTrue');
Route::post('get_user_detail','UserController@getUserDetail');
Route::post('insert_update_user','UserController@InsertUpdateUser');
Route::post('delete_user','UserController@DeleteUser');

Route::post('get_brand','BrandController@getBrand');
Route::post('get_brand_by_product_id','BrandController@getBrandbyProductID');
Route::post('insert_update_brand','BrandController@InsertUpdateBrand');
Route::post('get_brand_detail','BrandController@getBrandDetail');
Route::post('delete_brand','BrandController@DeleteBrand');
Route::post('import_brand','BrandController@importBrand');

Route::post('get_product','ProductController@getProduct');
Route::post('get_product_detail','ProductController@getProductDetail');
Route::post('insert_update_product','ProductController@InsertUpdateProduct');
Route::post('delete_product','ProductController@DeleteProduct');

Route::post('get_discount','DiscountController@getDiscount');
Route::post('get_discount_for_quotation','DiscountController@getDiscountforQuotation');
Route::post('insert_update_discount','DiscountController@InsertUpdateDiscount');
Route::post('delete_discount','DiscountController@DeleteDiscount');
Route::post('get_discount_detail_by_discount_id','DiscountController@getDiscountDetailByDiscountID');
Route::post('insert_discount_detail','DiscountController@InsertDiscountDetail');
Route::post('get_discount_detail','DiscountController@getDiscountDetail');
Route::post('get_discount_detail_by_hardware_id','DiscountController@getDiscountDetailByHardwareID');
Route::post('get_discount_detail_by_license_id','DiscountController@getDiscountDetailByLicenseID');
Route::post('get_discount_detail_by_service_id','DiscountController@getDiscountDetailByServiceID');
Route::post('get_discount_detail_by_other_id','DiscountController@getDiscountDetailByOtherID');

Route::post('get_hardware','HardwareController@getHardware');
Route::post('get_hardware_detail','HardwareController@getHardwareDetail');
Route::post('insert_update_hardware','HardwareController@InsertUpdateHardware');
Route::post('delete_hardware','HardwareController@DeleteHardware');
Route::post('activate_deactivate_hardware','HardwareController@ActivateDeactivateHardware');

Route::post('get_sales_visitation','SalesVisitationController@getSalesVisitation');
Route::post('get_sales_visitation_status','SalesVisitationController@getVisitationStatus');
Route::post('insert_update_sales_visitation','SalesVisitationController@insertUpdateSalesVisitation');
Route::post('insert_update_sales_visitation_detail','SalesVisitationController@insertUpdateSalesVisitationDetail');
Route::post('delete_sales_visitation','SalesVisitationController@DeleteSalesVisitation');
Route::post('get_sales_visitation_detail','SalesVisitationController@getSalesVisitationDetail');
Route::post('get_sales_visitation_detail_by_id','SalesVisitationController@getSalesVisitationDetailByID');
Route::post('get_sales_visitation_detail_by_sales_id','SalesVisitationController@getSalesVisitationDetailBySalesVisitationID');
Route::post('get_branch_for_visitation','BranchController@getBranchForVisitation');


Route::post('get_quotation','QuotationController@getQuotation');
Route::post('get_quotation_status','QuotationController@getQuotationStatus');
Route::post('get_quotation_detail','QuotationController@getQuotationDetail');
Route::post('get_quotation_detail_by_quotation_id','QuotationController@getQuotationDetailByQuotationID');
Route::post('get_quotation_detail_excel_revision','QuotationController@getQuotationDetailExcel');
Route::post('get_quotation_detail_excel','QuotationController@getQuotationDetailExcelRevised');
Route::post('insert_update_quotation','QuotationController@insertUpdateQuotation');
Route::post('delete_quotation','QuotationController@DeleteQuotation');
Route::post('insert_update_quotation_detail','QuotationController@insertUpdateQuotationDetail');
Route::post('update_quotation_status','QuotationController@updateQuotationStatus');
Route::post('update_quotation_date','QuotationController@updateQuotationDate');
Route::post('update_quotation_pic','QuotationController@updateQuotationPIC');

Route::post('get_invoice','InvoiceController@getInvoice');
Route::post('get_invoice_detail','InvoiceController@getInvoiceDetail');
Route::post('get_invoice_detail_excel_old','InvoiceController@getInvoiceDetailExcel');
Route::post('get_invoice_detail_excel','InvoiceController@getInvoiceDetailExcelRevised');
Route::post('insert_update_invoice','InvoiceController@insertUpdateInvoice');
Route::post('update_invoice_status','InvoiceController@updateInvoiceStatus');
Route::post('get_invoice_preview','InvoiceController@getInvoicePreview');
Route::post('generate_invoice_id','InvoiceController@generateInvoiceID');
Route::post('generate_invoice_commission','InvoiceController@generateInvoiceCommission');

Route::post('insert_update_invoice_payment','InvoicePaymentController@insertUpdateInvoicePayment');
Route::post('get_invoice_payment_by_invoice_id','InvoicePaymentController@getInvoicePaymentDetailByInvoiceID');
Route::post('get_invoice_payment_detail','InvoicePaymentController@getInvoicePaymentDetail');

Route::post('get_invoice_reseller','InvoiceResellerController@getInvoiceReseller');
Route::post('get_invoice_reseller_detail','InvoiceResellerController@getInvoiceResellerDetail');
Route::post('update_invoice_reseller_status','InvoiceResellerController@updateInvoiceResellerStatus');
Route::post('insert_update_invoice_reseller','InvoiceResellerController@insertUpdateInvoiceReseller');
