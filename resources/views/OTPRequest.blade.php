
<div>
    Dear {{$demo->receiver}}, Terlampir link untuk OTP Request dengan detail:<br><br>
    OTPRequestID = {{$demo->OTPRequestID}}<br>
    Quantity = {{$demo->RequestQty}}<br>
    Description = {{$demo->Description}}<br><br>
    @for ($i = 0; $i < count($demo->detailrequest); $i++)
        @if (@$demo->detailrequest[$i]['RestaurantID'] != null)
        RestaurantID = {{$demo->detailrequest[$i]['RestaurantID']}}<br>
        RestaurantName = {{$demo->detailrequest[$i]['RestaurantName']}}<br>
        BranchID = {{$demo->detailrequest[$i]['BranchID']}}<br>
        BranchID = {{$demo->detailrequest[$i]['BranchName']}}<br><br>
        @endif

        @if (@$demo->detailrequest[$i]['StoreID'] != null)
        StoreID = {{$demo->detailrequest[$i]['StoreID']}}<br>
        StoreName = {{$demo->detailrequest[$i]['StoreName']}}<br>
        BranchID = {{$demo->detailrequest[$i]['BranchID']}}<br>
        BranchID = {{$demo->detailrequest[$i]['BranchName']}}<br><br>
        @endif
    @endfor
    <br>Terima kasih.<br><br>
    Salam,<br>
    PT. Nova Teknologi Awani <br> <br>

    <B style="color:green">To Approve Request click link below:</B><br>
             http://vica.hb.id/hellobill-tracking-sales/otp-approved.html?id={{$demo->OTPRequestID}}&token={{$demo->tokenkokevin}}<br><br>
    <B style="color:red">To Reject Request click link below:</B><br>
             http://vica.hb.id/hellobill-tracking-sales/otp-declined.html?id={{$demo->OTPRequestID}}&token={{$demo->tokenkokevin}}<br>
