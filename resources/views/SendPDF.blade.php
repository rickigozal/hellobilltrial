
<div>
    Dear Bpk/Ibu {{$demo->receiver}},<br><br>
    Terlampir invoice untuk {{$demo->branchname}}.<br>
    Bukti transfer harap diinformasikan agar segera kami proses.<br>
    <br>
    <b>Note: Untuk pemasangan outlet baru akan dijadwalkan H+3 hari kerja sejak pembayaran diterima,<br>
     dan mohon cantumkan nama outlet di bukti pembayarannya.</b>
    <br><br>
    Kami tunggu kabar baiknya.
    <br><br>
    Terima kasih.<br><br>
    Salam,<br>
    PT. Nova Teknologi Awani
