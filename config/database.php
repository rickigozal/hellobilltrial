<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'pgsql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'pgsqlretail' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST_RET'),
            'port' => env('DB_PORT_RET', '5432'),
            'database' => env('DB_DATABASE_RET', 'forge'),
            'username' => env('DB_USERNAME_RET', 'forge'),
            'password' => env('DB_PASSWORD_RET', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'pgsqlretaildev' => [
            'driver' => 'pgsql',
            'host' => '10.130.20.83',
            'port' => env('DB_PORT', '5432'),
            'database' => 'retail',
            'username' => 'kevin',
            'password' => 'Pr0m1n3ns42015',
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'pgsqlfnb' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST_RES'),
            'port' => env('DB_PORT_RES', '5432'),
            'database' => env('DB_DATABASE_RES','FORGE'),
            'username' => env('DB_USERNAME_RES', 'forge'),
            'password' => env('DB_PASSWORD_RES', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'pgsqlfnbdev' => [
            'driver' => 'pgsql',
            'host' => '10.130.20.83',
            'port' => env('DB_PORT', '5432'),
            'database' => 'pos',
            'username' => 'kevin',
            'password' => 'Pr0m1n3ns42015',
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'pgsqlsalon' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST_SER'),
            'port' => env('DB_PORT_SER', '5432'),
            'database' => env('DB_DATABASE_SER','FORGE'),
            'username' => env('DB_USERNAME_SER', 'forge'),
            'password' => env('DB_PASSWORD_SER', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'pgsqlsalondev' => [
            'driver' => 'pgsql',
            'host' => '10.130.20.83',
            'port' => env('DB_PORT', '5432'),
            'database' => 'service',
            'username' => 'kevin',
            'password' => 'Pr0m1n3ns42015',
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'mongoadmindev' => [
           'driver'   => 'mongodb',
           'host'     => '10.130.20.83',
           'port'     => 27017,
           'database' => 'AdminLogging',
           'username' => 'kingopongo',
           'password' => 'Wh1t3.RustY!P4ng0l1N',
           'options'  => []
       ],

       'mongoadmin' => [
          'driver'   => 'mongodb',
          'host'     => '10.130.22.147',
          'port'     => 27017,
          'database' => 'AdminLogging',
          'username' => 'AdminKevin2',
          'password' => 'K3v1nP4ss12345',
          'options'  => []
      ],

      'mongotransaction' => [
         'driver'   => 'mongodb',
         'host'     => '10.130.22.147',
         'port'     => 27017,
         'database' => 'admin3',
         'username' => 'AdminKevin2',
         'password' => 'K3v1nP4ss12345',
         'options'  => []
     ],

        'sqlsrv' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '1433'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],

];
