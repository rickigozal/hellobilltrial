<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OTPRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $demo;

    public function __construct($demo)
    {
        $this->demo = $demo;
    }

    public function build()
    {
        $address = 'noreply@hellobill.com';
        $subject = "OTP Request From ".$this->demo->requestername;
        $name = 'Ricki Gozal';



            return $this->view('OTPRequest')
                        ->from($address, $this->demo->sender)
                        // ->cc($address2, $name2)
                        // ->cc($address3, $name3)
                        // ->cc($address4, $name4)
                        // ->cc($address5, $name5)
                        ->replyTo($address, $name)
                        ->subject($subject)
                        ->with([ 'message' => 'Sukses']);
        }
    }
