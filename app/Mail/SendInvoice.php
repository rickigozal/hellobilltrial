<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendInvoice extends Mailable
{
    use Queueable, SerializesModels;

    public $demo;

    public function __construct($demo)
    {
        $this->demo = $demo;
    }

    public function build()
    {
        $address = 'noreply@hellobill.com';
        $subject = "[HelloBill POS] Invoice for ".$this->demo->brandname;
        $name = 'Ricki Gozal';

        if($this->demo->countlicense == 0){
            return $this->view('SendPDF')
                        ->from($address, $this->demo->sender)
                        ->cc('ricki.gozal@hellobill.co.id', 'martin')
                        // ->cc($address2, $name2)
                        // ->cc($address3, $name3)
                        // ->cc($address4, $name4)
                        // ->cc($address5, $name5)
                        ->cc($this->demo->pic)
                        ->replyTo($address, $name)
                        ->subject($subject)
                        ->with([ 'message' => 'Sukses'])
                        ->attach($this->demo->attachment,[
                            'as' => $this->demo->filename,
                            'mime' => 'application/pdf']);
        }
        else{
                return $this->view('SendPDF')
                            ->from($address, $this->demo->sender)
                            ->cc('ricki.gozal@hellobill.co.id', 'martin')
                            // ->cc($address2, $name2)
                            // ->cc($address3, $name3)
                            // ->cc($address4, $name4)
                            // ->cc($address5, $name5)
                            ->cc($this->demo->pic)
                            ->replyTo($address, $name)
                            ->subject($subject)
                            ->with([ 'message' => 'Sukses'])
                            ->attach('https://s3-us-west-2.amazonaws.com/hellobill-assets/lisensipenggunaakhir/18/00/1531804914LisensiPenggunaAkhir.pdf',[
                                'as' => 'Lisensi Pengguna Akhir',
                                'mime' => 'application/pdf'
                            ])
                            ->attach($this->demo->attachment,[
                                'as' => $this->demo->filename,
                                'mime' => 'application/pdf']);

        }
    }
}
