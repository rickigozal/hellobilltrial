<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;
use PDF;

class ImportOldQuotationController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
        $link = $request->url();
        $Username = DB::table('User')
        ->where('UserID',$this->param->UserID)
        ->first()->Username;
        $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
        $log = DB::table('LogActivity')
        ->insert(array('UserID' => $this->param->UserID, 'Activity' => $link,
        'Parameter' => $request->getContent(), 'Time' => $now,'Username' => $Username));
    }
    public function importOldQuotationTest(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [

        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');




        for($i=0;$i<count($input['Data']);$i++)
        {
            $Header = $input['Data'][$i];
            $code = 'QUO';
            $count = DB::table('OldQuotation')
            ->max('QuotationID');
            $count = $count+1;
            $Delivery = null;
            $code = $code.$count;
            $param = array(
                'QuotationNO' => $code,
                'GrandTotal' => $Header['GrandTotal'],
                'StatusID' => "I",
                'GrandTotalReseller' => 0,
                'BrandID' => $Header['BrandID'],
                'QuotationDate' => $Header['Date'],
                'QuotationDueDate' => $Header['Due'],
                'GrandTotalDiscount' => $Header['Discount']
            );
            $Quotation = DB::table('OldQuotation')
            ->insert($param);
            $id = $this->getLastVal();
            //insert user PIC nya
            if($Header['UserID'] == "-")
            {
                //kalo -, user id nya di isi ko ponky.
                $Header['UserID'] = 17;
            }
            $result = DB::table('OldQuotationDetailUser')
            ->insert(array('QuotationID' => $id, 'UserID' => $Header['UserID']));

            //insert branch nya
            $result = DB::table('OldQuotationDetail')
            ->insert(array('QuotationID' => $id, 'Total' => $Header['GrandTotal'],
            'BranchID' => $Header['BranchID'], 'DiscountTotalBranch' => $Header['Discount']));
            $detailid = $this->getLastVal();
            //input detail nya.
            for($j=0;$j<count($Header['Detail']);$j++)
            {
                $Detail = $Header['Detail'][$j];
                if($Detail['Price'] != "-" )
                {
                    if($Detail['Type'] == "License")
                    {
                        if($Detail['Discount'] == "-")
                        $Detail['Discount'] = 0;
                        $paramLicense = array(
                            'QuotationDetailID' => $detailid,
                            'LicenseID' => $Detail['MainID'],
                            'Price' => $Detail['Price'],
                            'DiscountID' => null,
                            'DiscountName' => "Custom",
                            'DiscountValue' => $Detail['Discount'],
                            'DiscountPercentage' => null,
                            'SubTotal' => $Detail['Sub'],
                            'Quantity' => $Detail['Qty'],
                            'DiscountTotal' => $Detail['Discount'],
                            'PriceTotal' => $Detail['Price']*$Detail['Qty'],
                            'StartDate' => $Detail['StartDate'],
                            'EndDate' => $Detail['EndDate']
                        );
                        $result = DB::table('OldQuotationDetailLicense')
                        ->insert($paramLicense);
                    }
                    elseif($Detail['Type'] == "Hardware")
                    {
                        if($Detail['Discount'] == "-")
                        $Detail['Discount'] = 0;
                        $paramHardware = array(
                            'QuotationDetailID' => $detailid,
                            'HardwareID' => $Detail['MainID'],
                            'Price' => $Detail['Price'],
                            'DiscountID' => null,
                            'DiscountName' => "Custom",
                            'DiscountValue' => $Detail['Discount'],
                            'DiscountPercentage' => null,
                            'SubTotal' => $Detail['Sub'],
                            'Quantity' => $Detail['Qty'],
                            'DiscountTotal' => $Detail['Discount'],
                            'PriceTotal' => $Detail['Price']*$Detail['Qty']
                        );
                        $result = DB::table('OldQuotationDetailHardware')
                        ->insert($paramHardware);
                    }
                    elseif($Detail['Type'] == "Service")
                    {
                        if($Detail['Discount'] == "-")
                        $Detail['Discount'] = 0;
                        $paramService = array(
                            'QuotationDetailID' => $detailid,
                            'ServiceID' => $Detail['MainID'],
                            'Price' => $Detail['Price'],
                            'DiscountID' => null,
                            'DiscountName' => "Custom",
                            'DiscountValue' => $Detail['Discount'],
                            'DiscountPercentage' => null,
                            'SubTotal' => $Detail['Sub'],
                            'Quantity' => $Detail['Qty'],
                            'DiscountTotal' => $Detail['Discount'],
                            'PriceTotal' => $Detail['Price']*$Detail['Qty']
                        );
                        $result = DB::table('OldQuotationDetailService')
                        ->insert($paramService);
                    }
                    elseif($Detail['Type'] == "Other")
                    {
                        if($Detail['Discount'] == "-")
                        $Detail['Discount'] = 0;
                        $paramOther = array(
                            'QuotationDetailID' => $detailid,
                            'OtherID' => $Detail['MainID'],
                            'Price' => $Detail['Price'],
                            'DiscountID' => null,
                            'DiscountName' => "Custom",
                            'DiscountValue' => $Detail['Discount'],
                            'DiscountPercentage' => null,
                            'SubTotal' => $Detail['Sub'],
                            'Quantity' => $Detail['Qty'],
                            'DiscountTotal' => $Detail['Discount'],
                            'PriceTotal' => $Detail['Price']*$Detail['Qty']
                        );
                        $result = DB::table('OldQuotationDetailOther')
                        ->insert($paramOther);
                    }
                    elseif($Detail['Type'] == "Delivery")
                    {
                        $Delivery = $Detail['Sub'];
                        $GrandTotal = $Header['GrandTotal']-$Delivery;
                        $QuotatonGrandTotal = DB::table('OldQuotation')
                        ->where('QuotationID',$id)
                        ->update(array('GrandTotal' => $GrandTotal));
                    }
                }
            }

            $year = substr($Header['PaidDate'],0,4);
            $month = substr($Header['PaidDate'],5,2);
            $StatusID = "P2";
            if($Header['PaidDate'] == "-")
            {
                $StatusID = "P";
                $Header['PaidDate'] = null;
            }
            elseif($Header['PaidDate'] != "-")
            {
                $yearreceipt = $year;
                $monthreceipt = $month;
                $numreceipt = DB::table('OldInvoice')
                ->whereraw('CAST(SUBSTRING("ReceiptNO", 8,4) AS INTEGER)  ='.$yearreceipt.' and CAST(SUBSTRING("ReceiptNO", 13,2) AS INTEGER) ='.$monthreceipt)
                ->max('ReceiptID');
                $numreceipt = $numreceipt+1;
                    $countreceipt = str_pad($numreceipt, 6, '0', STR_PAD_LEFT);
                    $codereceipt = 'HB-REC-'.$yearreceipt.'-'.$monthreceipt.($countreceipt);
                    $ReceiptNO = $codereceipt;
            }
            $Invoiceyear = substr($Header['Date'],0,4);
            $Invoicemonth = substr($Header['Date'],5,2);
            $num2 = DB::table('OldInvoice')
            ->whereraw('CAST(SUBSTRING("ProformaNO", 8,4) AS INTEGER)  ='.$Invoiceyear.' and CAST(SUBSTRING("ProformaNO", 13,2) AS INTEGER) ='.$Invoicemonth)
            ->max('ProformaID');
            $num2 = $num2+1;
                $count2 = str_pad($num2, 6, '0', STR_PAD_LEFT);
                $code2 = 'HB-PINV-'.$Invoiceyear.'-'.$Invoicemonth.($count2);
                $ProformaNO = $code2;

            $paramInvoice = array(
                'InvoiceID' => @$count,
                'InvoiceNO' => $Header['InvoiceNo'],
                'QuotationID' => $id,
                'DeliveryDate' => $Header['Date'],
                'DueDate' => $Header['Due'],
                'StatusID' => $StatusID,
                'PaidDate' => $Header['PaidDate'],
                'BranchID' => $Header['BranchID'],
                'GrandTotal' => $Header['GrandTotal'],
                'GrandTotalDiscount' => $Header['Discount'],
                'ProformaNO' => $ProformaNO,
                'ReceiptID' => @$numreceipt,
                'ReceiptNO' => @$ReceiptNO,
                'Delivery' => @$Delivery
            );
            $result = DB::table('OldInvoice')
            ->insert($paramInvoice);
            $invoiceID = $this->getLastVal();
        }

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success"
      );

      return Response()->json($endresult);

    }

    public function generatePDFQuotation($ID){
        $Token = DB::table('Authenticator')
        ->where('AuthenticatorID',$this->param->AuthenticatorID)
        ->first()->AuthenticatorToken;
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/json',
                'content' => '{}'
            ));
        $context = stream_context_create($opts);
        $test = 'http://10.130.23.124/hellobill-tracking-sales/services/index.php/quotation/print_quotation/'.$ID.'/mime'.'/'.$Token;

        $data = file_get_contents($test,false,$context);
        return $data;
    }
}
