<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;
use PDF;

class CheckEmailController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
        $link = $request->url();
        $Username = DB::table('User')
        ->where('UserID',$this->param->UserID)
        ->first()->Username;
        $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
        $log = DB::table('LogActivity')
        ->insert(array('UserID' => $this->param->UserID, 'Activity' => $link,
        'Parameter' => $request->getContent(), 'Time' => $now,'Username' => $Username));
    }

    public function checkEmail(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
          'Email' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $Email = @$input['Email'];

        $email = $DBFnB->table('Account')
        ->where(DB::raw('lower("Email")'),strtolower($Email))
        ->where('AccountID','>',0)
        ->wherenull('DeletedDate')
        ->get();
        if(count($email)>0){
            $email[0]->status = new \stdclass;
            if($email[0]->IsMaster != null && $email[0]->IsMaster != 0)
            {
                $email[0]->status = 'Master';
            }
            elseif($email[0]->SecondaryFrom != null && $email[0]->SecondaryFrom != 0)
            {
                $email[0]->status = 'Secondary';
            }
            elseif($email[0]->FromUserID != null){
                $email[0]->status = 'InviteUser';
            }

            $branch = $DBFnB->table('AccountOutlet AS AO')
            ->where('AccountID',$email[0]->AccountID)
            ->wherenotnull('BranchID')
            ->where('BranchID','!=',0)
            ->get();


            for($i=0;$i<count($branch);$i++)
            {

                if($branch[$i]->ProductID == "RES" || $branch[$i]->ProductID == "HQF")
                {
                    $BranchName = $DBFnB->table('Branch')
                    ->where('BranchID',$branch[$i]->BranchID)
                    ->first();
                    $RestaurantName = $DBFnB->table('RestaurantMaster')
                    ->where('RestaurantID',$BranchName->RestaurantID)
                    ->first();

                    // return gettype($BranchName->BranchName);
                    // return gettype($branch[$i]->BranchName);
                    $branch[$i]->BranchName = $BranchName->BranchName;
                    if($branch[$i]->ProductID == "RES")
                    {$branch[$i]->ProductName = "Food And Baverage";}
                    else{$branch[$i]->ProductName = "HQ Food And Baverage";}
                    $branch[$i]->BrandName = $RestaurantName->RestaurantName;

                }
                elseif($branch[$i]->ProductID == "RET" || $branch[$i]->ProductID == "HQR")
                {
                    $BranchName = $DBRetail->table('Branch')
                    ->where('BranchID',$branch[$i]->BranchID)
                    ->first();
                    $StoreName = $DBRetail->table('StoreMaster')
                    ->where('StoreID',$BranchName->StoreID)
                    ->first();
                    // return gettype($BranchName->BranchName);
                    // return gettype($branch[$i]->BranchName);
                    $branch[$i]->BranchName = $BranchName->BranchName;
                    $branch[$i]->BrandName = $StoreName->StoreName;
                    if($branch[$i]->ProductID == "RET")
                    {$branch[$i]->ProductName = "Retail";}
                    else{$branch[$i]->ProductName = "HQ Retail";}

                }
                else
                {
                    $BranchName = $DBSalon->table('Branch')
                    ->where('BranchID',$branch[$i]->BranchID)
                    ->first();
                    $BrandName = $DBSalon->table('BrandMaster')
                    ->where('BrandID',$BranchName->BrandID)
                    ->first();
                    // return gettype($BranchName->BranchName);
                    // return gettype($branch[$i]->BranchName);
                    $branch[$i]->BranchName = $BranchName->BranchName;
                    $branch[$i]->BrandName = $BrandName->BrandName;
                    $branch[$i]->ProductName = "Salon";
                }

            }
            // $email = $DBFnB->select('select * from "Account"
            // where lower("Email") = lower(\''.$Email.'\') and "AccountID" > 0 and "DeletedDate" is null');
        }
        else{$branch = array();}




      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Data' => array(
              'Email' => $email,
              'Branch' => $branch
          )
      );

      return Response()->json($endresult);

    }

    public function getBrand(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
          'isGetAll' => 'required|boolean'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        if($input['isGetAll'] === true)
        {
            $result = DB::table('Brand')
            ->leftjoin('Product', 'Brand.ProductID','=','Product.ProductID')
            ->select(['BrandID','BrandName','Email','Brand.ProductID','ProductName','Contact','Image','Phone','Address','CreatedBy'])
            ->where('Brand.Archived',null)
            ->orderby('BrandID','desc')
            ->get();
        }
        else{
            // $result = DB::select(
            //     'select *
            //     from "Brand"
            //     left join "Product" on "Brand"."ProductID" = "Product"."ProductID"
            //     where ("CreatedBy" is null or "CreatedBy" = '.$this->param->UserID.') and "Brand"."Archived" is null'
            // );

            $result = DB::table('Brand')
            ->leftjoin('Product','Brand.ProductID','=','Product.ProductID')
            ->wherenull('Brand.Archived')
            ->where(function ($query) {
              $query->where('CreatedBy', null)
                    ->orWhere('CreatedBy', $this->param->UserID);})
            ->get();
        }


      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Brand' => $result
      );

       return Response()->json($endresult);
    }

    public function importBrand(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
          'Data' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        for($i=0;$i<count($input['Data']);$i++){
            $Brand = $input['Data'][$i];
            $param = array(
                    'BrandName' => $Brand['BrandName'],
                    'ProductID' => $Brand['ProductID'],
                    'Email' => $Brand['Email'],
                    'Address' => $Brand['Address'],
                    'Contact' => $Brand['Contact'],
                    'Phone' => $Brand['Phone'],
                    'CreatedBy' => $Brand['UserID']
            );
            $result = DB::table('Brand')
            ->insert($param);
        }

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Brand' => $result
      );

       return Response()->json($endresult);
    }

    public function getBrandbyProductID(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
          'ProductID' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $result = DB::table('Brand')
        ->where('ProductID',$input['ProductID'])
        ->wherenull('Archived')
        ->get();

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Brand' => $result
      );

       return Response()->json($endresult);
    }

    public function getBrandDetail(Request $request){

      $input = json_decode($this->request->getContent(),true);
      $rules = [
        'BrandID' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $BrandID = $input['BrandID'];
      $result = DB::table('Brand')
      ->leftjoin('Product', 'Brand.ProductID','=','Product.ProductID')
      ->leftjoin('User','Brand.CreatedBy','=','User.UserID')
      ->select(['BrandID','BrandName','Brand.Email','Brand.ProductID','Contact','ProductName','Image','Brand.Phone','Brand.Address','CreatedBy'])
      ->where('BrandID',$BrandID)
      ->get();
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Brand' => $result
      );
      return Response()->json($endresult);
}

public function InsertUpdateBrand(Request $request){
    $input = json_decode($request->getContent(),true);
    $rules = [
        'BrandName' => 'required',
        'ProductID' => 'required',
        'Email' => 'required|email',
        'Phone' => 'required',
        'Address' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }
    $ID = @$input['BrandID'];
    $unique = array(
        'Table' => "Brand",
        'ID' => $ID,
        'Column' => "BrandName",
        'String' => $input['BrandName']
    );
    $uniqueBrandName = $this->unique($unique);
    // $unique['Column'] = "Email";
    // $unique['String'] = $input['Email'];
    // $uniqueEmail = $this->unique($unique);
    $param = array(
        'BrandName' => $input['BrandName'],
        'ProductID' => @$input['ProductID'],
        'Email' => $input['Email'],
        'Phone' => $input['Phone'],
        'Contact' => @$input['Contact'],
        'Address' => $input['Address'],
        'CreatedBy' => $this->param->UserID);
        if(@$input['UserID'] != null)
        {
            $param['CreatedBy'] = $input['UserID'];
        }



      if ($ID == null){
        $result = DB::table('Brand')->insert($param);
        $ID = $this->getLastVal();
        }
      else{$param = array(
          'BrandName' => $input['BrandName'],
          'ProductID' => @$input['ProductID'],
          'Email' => $input['Email'],
          'Phone' => $input['Phone'],
          'Contact' => @$input['Contact'],
          'Address' => $input['Address'],
          'CreatedBy' => $input['UserID']);

          $result = DB::table('Brand')->where('BrandID',$ID)->update($param);}
      if(@$input['DeleteImage'] === true){
            $param = array("Image" => null);
            $result = DB::table('Brand')->where('BrandID',$ID)->update($param);
        }

      if(@$input['Images'] !== null){
              $arr = array(
                  'UserID' => $this->param->UserID,
                  'ObjectID' => $ID,
                  'Folder' => 'Brand',
                  'Filename' => @$input['Images']['Filename'],
                  'Data' =>  @$input['Images']['Data']
              );
              // return(@$input['Images']['Data']);
              // die();
              $path = $this->upload_to_s3($arr);
              $data = ['Image' => $path];
              $result = DB::table('Brand')->where('BrandID',$ID)->update($data);

              $response = $this->generateResponse(0, [], "Success", ['Brand'=>$result]);
          }

          $result = $this->checkReturn($result);
          // dd($ID);
          // $result['BrandID'] = $ID;
          return Response()->json($result);

  }

  public function DeleteBrand(Request $request){
       $input = json_decode($this->request->getContent(),true);
       $rules = [
         'BrandID' => 'required'
       ];

       $validator = Validator::make($input, $rules);
       if ($validator->fails()) {
           $errors = $validator->errors();
           $errorList = $this->checkErrors($rules, $errors);
           $additional = null;
           $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
           return response()->json($response);
       }
       $BrandID = @$input['BrandID'];
       $param = array('Status' => 'D','Archived' => now());
       $result = DB::table('Brand')->where('BrandID', $BrandID)->update($param);
       $result2 = DB::table('Branch')->where('BrandID',$BrandID)->update($param);
      $result = $this->checkReturn($result);

      return Response()->json($result);

  }

}
