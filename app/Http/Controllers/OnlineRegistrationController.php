<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;
use PDF;

class OnlineRegistrationController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
        $link = $request->url();
        $Username = DB::table('User')
        ->where('UserID',$this->param->UserID)
        ->first()->Username;
        $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
        $log = DB::table('LogActivity')
        ->insert(array('UserID' => $this->param->UserID, 'Activity' => $link,
        'Parameter' => $request->getContent(), 'Time' => $now,'Username' => $Username));
    }

    public function getAccountRegistration(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
            'metaData' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');
        $MetaData = @$input['metaData'];
        $dtime = strtotime($MetaData['StartDate']);
        $StartDate = date('Y-m-d 00:00:00',$dtime);
        $dtime = strtotime($MetaData['EndDate']);
        $EndDate = date('Y-m-d 23:59:59',$dtime);

        $StartFrom = @$input['StartFrom'];
        $PageSize = @$input['PageSize'];
        $OrderDirection = $input['OrderDirection'];
        $OrderBy = $input['OrderBy'];

        if($PageSize == null)
        {$PageSize = 100;}
        $PageSize = (int)$PageSize;
        $StartFrom = (int)$StartFrom;

        $AccountRegistration = $DBFnB->table('AccountRegistration')
        ->where('RegisterDate','>=',$StartDate)
        ->where('RegisterDate','<=',$EndDate);

        if(@$MetaData['Platform'] != null)
        {
            if($MetaData['Platform'] == 'iOS')
            $AccountRegistration->where('Platform', 'iOS');
            elseif($MetaData['Platform'] == 'Android')
            $AccountRegistration->where('Platform','Android');
        }
        
        if(@$MetaData['ProductID'] != null)
        {
            if($MetaData['ProductID'] == '1')
            $AccountRegistration->where('ProductID','1');
            elseif($MetaData['ProductID'] == '2')
            $AccountRegistration->where('ProductID','2');
            elseif($MetaData['ProductID'] == '3')
            $AccountRegistration->where('ProductID','3');
        }
        $AccountRegistration->orderBy($OrderBy,$OrderDirection);
        // $result = $AccountRegistration;
        $DataCount = $AccountRegistration->count();

        $AccountRegistration->limit($PageSize);
        $AccountRegistration->skip($StartFrom);
        $AccountRegistration->select(DB::raw('"AccountRegistrationID", "Email", "ContactPerson", "RestaurantName", "OutletNumber",
        "RegisterDate", "CreatedDate", "CouponCode", "Username", "ContactNumber", "Brand", "SurveyAddress", "ProductID",case "ProductID"
        WHEN \'1\' THEN \'Restaurant\'
		WHEN \'2\' THEN \'Retail\'
		WHEN \'3\' THEN \'Salon\'
		END as "ProductName", "Platform", "Notes", "ProcessedBy", "ProcessedDate", "Status", "City", "ISP", "Referrer", "ViralSource",coalesce("VisitationProcessed",\'0\') as "VisitationProcessed"'));

        $result = $AccountRegistration->get();
      
        $endresult = array(
            'Status' => 0,
            'Errors' => array(),
            'Message' => "Success",
            'recordsFiltered' => $DataCount,
            'recordsTotal' => $DataCount,
            'AccountRegistration' => $result
  
        );
  
        return Response()->json($endresult);
    }

    public function getProduct(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');
        
        $Product = [];
        for($i=1;$i<=3;$i++)
        {
            $Product[$i] = new \stdClass();
            if($i == 1)
            {$Product[$i]->ProductID = 1;
             $Product[$i]->ProductName = 'Restaurant';}
            elseif($i == 2)
            {$Product[$i]->ProductID = 2;
             $Product[$i]->ProductName = 'Retail';}
             elseif($i == 3)
            {$Product[$i]->ProductID = 3;
             $Product[$i]->ProductName = 'Salon';}
        }
        $Platform = [];
        for($i=1;$i<=2;$i++)
        {
            $Platform[$i] = new \stdClass();
            if($i == 1)
            {$Platform[$i]->PlatformID = 'iOS';}
            else{
                $Platform[$i]->PlatformID = 'Android';
            }

        }

        $Status = [];
        for($i=0;$i<3;$i++)
        {
            $Status[$i] = new \stdClass();
            if($i==0)
            {$Status[$i]->StatusID = '1';
             $Status[$i]->StatusName = 'Unhandled';}
             elseif($i == 1){
                 $Status[$i]->StatusID = '2';
                 $Status[$i]->StatusName = 'Bogus';
             }
             else{
                $Status[$i]->StatusID = '3';
                $Status[$i]->StatusName = 'Followed Up';
             }
        }
       

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Product' => array_values($Product),
          'Platform' => array_values($Platform),
          'Status' => array_values($Status)
      );

      return Response()->json($endresult);

    }

    public function getAccountRegistrationDetail(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
            'AccountRegistrationID' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');

        $AccountRegistrationID = $input['AccountRegistrationID'];
        $AccountRegistration = $DBFnB->table('AccountRegistration')
        ->where('AccountRegistrationID',$AccountRegistrationID)
        ->select(DB::raw('"AccountRegistrationID", "Email", "ContactPerson", "RestaurantName", "OutletNumber",
        "RegisterDate", "CreatedDate", "CouponCode", "Username", "ContactNumber", "Brand", "SurveyAddress", "ProductID",case "ProductID"
        WHEN \'1\' THEN \'Restaurant\'
		WHEN \'2\' THEN \'Retail\'
		WHEN \'3\' THEN \'Salon\'
        END as "ProductName", "Platform", "Notes", "ProcessedBy", "ProcessedDate", "Status", case "Status"
        WHEN \'1\' THEN \'Unhandled\'
		WHEN \'2\' THEN \'Bogus\'
        WHEN \'3\' THEN \'Followed Up\'
        ELSE \'\'
        END as "StatusName"
        , "City", "ISP", "Referrer", "ViralSource"'))
        ->get();
      
        $endresult = array(
            'Status' => 0,
            'Errors' => array(),
            'Message' => "Success",
            'AccountRegistration' => $AccountRegistration
  
        );
  
        return Response()->json($endresult);
    }
 
    public function updateOnlineRegistration(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
            'AccountRegistrationID' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');

        $param = array(
            'Notes' => @$input['Notes'],
            'Status' => @$input['Status']
        );

        $AccountRegistrationID = $input['AccountRegistrationID'];
        $AccountRegistration = $DBFnB->table('AccountRegistration')
        ->where('AccountRegistrationID',$AccountRegistrationID)
        ->update($param);
      
        $endresult = array(
            'Status' => 0,
            'Errors' => array(),
            'Message' => "Success",
            'AccountRegistration' => $AccountRegistration
  
        );
  
        return Response()->json($endresult);
    }

}
