<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;

class LicenseController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
        $link = $request->url();
        $Username = DB::table('User')
        ->where('UserID',$this->param->UserID)
        ->first()->Username;
        $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
        $log = DB::table('LogActivity')
        ->insert(array('UserID' => $this->param->UserID, 'Activity' => $link,
        'Parameter' => $request->getContent(), 'Time' => $now,'Username' => $Username));
    }

    public function getLicense(request $request){
        $input = json_decode($request->getContent(),true);
        $rules = [
            'ProductID' => 'nullable'
        ];
        $isGetAll = @$input['isGetAllLicense'];
        if(@$input['ProductID'] === null)
        {
            if($isGetAll == true){
                $result = DB::table('License')
                ->leftjoin('Product','License.ProductID','=','Product.ProductID')
                ->select(['LicenseID','LicenseCode','LicenseName','License.ProductID','ProductCode','ProductName','Price','Duration','ResellerPrice',
                        'Free','CommissionPercentage','Active'])
                ->where('License.Status',null)
                ->orderby('LicenseID','desc')
                ->get();
            }
            else{
                $result = DB::table('License')
                ->leftjoin('Product','License.ProductID','=','Product.ProductID')
                ->select(['LicenseID','LicenseCode','LicenseName','License.ProductID','ProductCode','ProductName','Price','Duration','ResellerPrice',
                        'Free','CommissionPercentage','Active'])
                ->where('License.Status',null)
                ->whereraw('"Active" is null or "Active" = true')
                ->orderby('LicenseID','desc')
                ->get();
            }
            
        }
        else{
            if($isGetAll == true){
                $result = DB::table('License')
                ->leftjoin('Product','License.ProductID','=','Product.ProductID')
                ->select(['LicenseID','LicenseCode','LicenseName','License.ProductID','ProductCode','ProductName','Price','Duration','ResellerPrice',
                        'Free','CommissionPercentage','Active'])
                ->where('License.Status',null)
                ->where('License.ProductID', @$input['ProductID'])
                ->orderby('LicenseID','desc')
                ->get();
            }
            else{
                $result = DB::table('License')
                ->leftjoin('Product','License.ProductID','=','Product.ProductID')
                ->select(['LicenseID','LicenseCode','LicenseName','License.ProductID','ProductCode','ProductName','Price','Duration','ResellerPrice',
                        'Free','CommissionPercentage','Active'])
                ->where('License.Status',null)
                ->where('License.ProductID', @$input['ProductID'])
                ->whereraw('"Active" is null or "Active" = true')
                ->orderby('LicenseID','desc')
                ->get();
            }
        }

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'License' => $result
      );
    return Response()->json($endresult);
    }

    public function getLicenseDetail(Request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'LicenseID' => 'required',
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $LicenseID = $input['LicenseID'];
      $result = DB::table('License')
      ->leftjoin('Product','License.ProductID','=','Product.ProductID')
      ->select(['LicenseID','LicenseCode','LicenseName','License.ProductID','ProductCode','ProductName','Price','Duration','ResellerPrice','Free',
                'CommissionPercentage'])
      ->where('LicenseID',$LicenseID)
      ->get();

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'License' => $result
      );
      return Response()->json($endresult);

    }

    public function InsertUpdateLicense(Request $request){
      $input = json_decode($request->getContent(), true);
      $rules = [
      'LicenseName' => 'required',
      'ResellerPrice' => 'numeric|required',
      'Price' => 'required|numeric',
      'Duration' => 'required|numeric',
      'ProductID' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $id = @$input['LicenseID'];
      $unique = array(
          'Table' => "License",
          'ID' => $id,
          'Column' => "LicenseName",
          'String' => $input['LicenseName']
      );
      $uniqueLicenseName = $this->unique($unique);
      $unique['Column'] = "LicenseCode";
      $unique['String'] = $input['LicenseCode'];
      $uniqueLicenseCode = $this->unique($unique);

      $LicenseCode = $input['LicenseCode'];
      $LicenseName = $input['LicenseName'];
      $Price = $input['Price'];
      $ResellerPrice = $input['ResellerPrice'];
      $Duration = $input['Duration'];
      $ProductID = $input['ProductID'];


      if ($id == null){$result = DB::table('License')->insert(array(
      'LicenseCode' => $LicenseCode,
      'LicenseName' => $LicenseName,
      'Price' => $Price,
      'ResellerPrice' => $ResellerPrice,
      'Duration' => $Duration,
      'ProductID' => $ProductID,
      'Free' => @$input['Free']
      ));}
      else {$result = DB::table('License')->where('LicenseID', $id)->update(array(
      'LicenseCode' => $LicenseCode,
      'LicenseName' => $LicenseName,
      'Price' => $Price,
      'ResellerPrice' => $ResellerPrice,
      'Duration' => $Duration,
      'ProductID' => $ProductID,
      'Free' => @$input['Free']
      ));}

        $result = $this->checkReturn($result);
        return Response()->json($result);

    }

    public function ActivateDeactivateLicense(Request $request){
      $input = json_decode($request->getContent(), true);
      $rules = [
      'LicenseID' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $Active = @$input['Active'];
      $LicenseID = @$input['LicenseID'];
      $Status = DB::table('License')
      ->where('LicenseID',$LicenseID)
      ->first();
      if ($Status->Active === null || $Status->Active === true){
          $ActiveStatus = false;
      }
      else{
          $ActiveStatus = true;
      }
      if($ActiveStatus == true)
      $param = array('Active' => null);
      else
      $param = array('Active' => false);
      
        $result = DB::table('License')
        ->where('LicenseID',$LicenseID)
        ->update($param);

        $result = $this->checkReturn($result);
        return Response()->json($result);

    }

    public function DeleteLicense(Request $request){
         $input = json_decode($this->request->getContent(),true);
         $LicenseID = @$input['LicenseID'];
         $result = DB::table('License')->where('LicenseID', $LicenseID)->update(array(
                    'Status' => 'D', 'Archived' => now()
             ));

             $result = $this->checkReturn($result);

             return Response()->json($result);


    }
}
