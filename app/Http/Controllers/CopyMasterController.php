<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;
use PDF;

class CopyMasterController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
        $link = $request->url();
        $Username = DB::table('User')
        ->where('UserID',$this->param->UserID)
        ->first()->Username;
        $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
        $log = DB::table('LogActivity')
        ->insert(array('UserID' => $this->param->UserID, 'Activity' => $link,
        'Parameter' => $request->getContent(), 'Time' => $now,'Username' => $Username));
    }
    public function getBranchTab(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [

        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');
        $Tab = array();
        $prID = ["Restaurant", "Retail", "Services"];
        $prName = ["RES", "RET", "SER"];
        $count = count($prID);
        for($i=0;$i<$count;$i++)
        {
            $Tab[] = array(
                'ProductName' => $prID[$i],
                'ProductID' => $prName[$i]
            );
        }

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Tab' => $Tab
      );

      return Response()->json($endresult);

    }

    public function getAccount(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [

        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');

        $Account = $DBFnB->table('Account')
        ->where('DeletedDate',null)
        ->get();

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Account' => $Account
      );

      return Response()->json($endresult);

    }

    public function getCopyDataType(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
            'DataTypeParent' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');
        $DataTypeParent = $input['DataTypeParent'];
        $CopyDataTypeHeader = $DBFnB->table('CopyDataType')
        ->where('Number',null)
        ->orderby('CopyDataTypeID','desc')
        ->get();


        for($i=0;$i<count($CopyDataTypeHeader);$i++)
        {
            $CopyDataTypeHeader[$i]->node = array();
            $CopyType = $DBFnB->table('CopyDataType')
            ->where('DataType',$CopyDataTypeHeader[$i]->CopyDataTypeID)
            ->where('DataTypeParent',$DataTypeParent)
            ->where('Number','like','%00')
            ->wherenull('Status')
            ->orderby('Number')
            // ->where(DB::raw('right("Number",3) like \'000\''))
            ->get();
            for($j=0;$j<count($CopyType);$j++)
            {
                $CopyType[$j]->node = array();
                $number = substr($CopyType[$j]->Number,0,2);

                $CopyType2 = $DBFnB->table('CopyDataType')
                ->where('DataType',$CopyDataTypeHeader[$i]->CopyDataTypeID)
                ->where('DataTypeParent',$DataTypeParent)
                ->where('Number','like',$number.'%0')
                ->where('Number','not like',$number.'00')
                ->wherenull('Status')
                ->orderby('Number')
                // ->where(DB::raw('right("Number",3) like \'000\''))
                ->get();
                for($k=0;$k<count($CopyType2);$k++)
                {
                    $CopyType2[$k]->node = array();
                    $number2 = substr($CopyType2[$k]->Number,0,3);

                    $CopyType3 = $DBFnB->table('CopyDataType')
                    ->where('DataType',$CopyDataTypeHeader[$i]->CopyDataTypeID)
                    ->where('DataTypeParent',$DataTypeParent)
                    ->where('Number','like',$number2.'%')
                    ->where('Number','not like',$number2.'0')
                    ->orderby('Number')
                    // ->where(DB::raw('right("Number",3) like \'000\''))
                    ->get();
                    // return $CopyType3;
                    $CopyType2[$k]->node = $CopyType3;
                }
                $CopyType[$j]->node = $CopyType2;
            }
            $CopyDataTypeHeader[$i]->node = $CopyType;
        }

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'CopyDataType' => $CopyDataTypeHeader
      );

      return Response()->json($endresult);

    }

    public function copyMasterData(Request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
          'BranchIDFrom' => 'required',
          'BranchIDTo' => 'required',
          'CopyList' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
        $CopyList = $input['CopyList'];
        $BranchIDTo = $input['BranchIDTo'];
        $BranchIDFrom = $input['BranchIDFrom'];
        $ProductID = $input['ProductID'];

        if($ProductID == "RES")
        {
            $CategoryDelete = $DBFnB->table('Category')
            ->where('BranchID',$BranchIDTo)
            ->update(array('Archived' => "Y"));
            $ItemDelete = $DBFnB->table('Menu')
            ->where('BranchID',$BranchIDTo)
            ->update(array('Archived' => "Y"));
            $ModifierGroupDelete = $DBFnB->table('ModifierGroup')
            ->where('BranchID',$BranchIDTo)
            ->update(array('Archived' => "Y"));
            $ModifierItemDelete = $DBFnB->table('ModifierItem')
            ->where('BranchID',$BranchIDTo)
            ->update(array('Archived' => "Y"));
            $MenuModifierDelete = $DBFnB->table('MenuModifier')
            ->where('BranchID',$BranchIDTo)
            ->delete();
            $IngredientDelete = $DBFnB->table('Ingredient')
            ->where('BranchID',$BranchIDTo)
            ->delete();
            $InventoryCategory = $DBFnB->table('InventoryCategory')
            ->where('BranchID',$BranchIDTo)
            ->update(array('Archived' => "Y"));
            $Inventory = $DBFnB->table('Inventory')
            ->where('BranchID',$BranchIDTo)
            ->update(array('Archived' => "Y"));
            $RESID = $DBFnB->table('Branch')
            ->where('BranchID',$BranchIDTo)
            ->first();
            $NewModifierGroupID = array();
            $OldModifierGroupID = array();
            $NewModifierItemID = array();
            $OldModifierItemID = array();
            $OldMenuID = array();
            $NewMenuID = array();
            $OldInventoryID = array();
            $NewInventoryID = array();
            $RestaurantIDTo = $RESID->RestaurantID;
            $Modifier = false;
            $Item = false;
            $Category = false;
            $Inventory = false;
            $InventoryCategory = false;
            $Ingredients = false;
            for($i =0;$i<count($CopyList);$i++)
            {
                if($CopyList[$i] == "Modifier")
                $Modifier = true;
                elseif($CopyList[$i] == "Item")
                $Item = true;
                else {
                    $Category = true;
                }
                if($CopyList[$i] == "Inventory")
                {
                    $Inventory = true;
                }
                elseif($CopyList[$i] == "InventoryCategory")
                {
                    $InventoryCategory = true;
                }
                if($CopyList[$i] == "Ingredients")
                {
                    $Ingredients = true;
                }
            }

            // if($Modifier == true)
            // {
            //
            //     $category = $DBFnB->table('Category')
            //     ->where('BranchID',$BranchIDFrom)
            //     ->whereRaw('("Archived" is null or "Archived" = \'N\')')
            //     ->get();
            //     $ModifierGroup = $DBFnB->table('ModifierGroup')
            //     ->where('BranchID',$BranchIDFrom)
            //     ->whereRaw('("Archived" is null or "Archived" = \'N\')')
            //     ->get();
            //     $MenuModifier = $DBFnB->table('MenuModifier')
            //     ->where('BranchID',$BranchIDFrom)
            //     ->get();
            //
            //
            //     for($j=0;$j<count($category);$j++)
            //     {
            //         $menu = $DBFnB->table('Menu')
            //         ->where('CategoryID',$category[$j]->CategoryID)
            //         ->whereRaw('("Archived" is null or "Archived" = \'N\')')
            //         ->get();
            //
            //
            //         $paramCategory = $this->cloneParameter($category[$j]);
            //         $paramCategory['BranchID'] = $BranchIDTo;
            //         $paramCategory['RestaurantID'] = $RestaurantIDTo;
            //         unset($paramCategory['CategoryID']);
            //
            //         $result = $DBFnB->table('Category')
            //         ->insert($paramCategory);
            //
            //         $CategoryID = $DBFnB->select('select lastval() AS id')[0]->id;
            //
            //
            //         for($k=0;$k<count($menu);$k++)
            //         {
            //
            //             $paramMenu = $this->cloneParameter($menu[$k]);
            //             $paramMenu['CategoryID'] = $CategoryID;
            //             $paramMenu['BranchID'] = $BranchIDTo;
            //             $paramMenu['RestaurantID'] = $RestaurantIDTo;
            //             unset($paramMenu['MenuID']);
            //
            //             $result = $DBFnB->table('Menu')
            //             ->insert($paramMenu);
            //
            //             array_push($NewMenuID, $DBFnB->select('select lastval() AS id')[0]->id);
            //             array_push($OldMenuID, $menu[$k]->MenuID);
            //
            //         }
            //
            //     }
            //
            //     for($i=0;$i<count($ModifierGroup);$i++)
            //     {
            //         $paramModifierGroup = $this->cloneParameter($ModifierGroup[$i]);
            //         unset($paramModifierGroup['ModifierGroupID']);
            //         $paramModifierGroup['BranchID'] = $BranchIDTo;
            //         $paramModifierGroup['RestaurantID'] = $RestaurantIDTo;
            //         $result = $DBFnB->table('ModifierGroup')
            //         ->insert($paramModifierGroup);
            //
            //         array_push($NewModifierGroupID, $DBFnB->select('select lastval() AS id')[0]->id);
            //         array_push($OldModifierGroupID, $ModifierGroup[$i]->ModifierGroupID);
            //         $ModifierItem = $DBFnB->table('ModifierItem')
            //         ->where('ModifierGroupID',$ModifierGroup[$i]->ModifierGroupID)
            //         ->get();
            //
            //         // $ModifierItem = $DBFnB->select('select * FROM "public"."ModifierItem" WHERE "ModifierGroupID" = '.$ModifierGroup[$i]->ModifierGroupID);
            //
            //         for($j=0;$j<count($ModifierItem);$j++)
            //         {
            //             $paramModifierItem = $this->cloneParameter($ModifierItem[$j]);
            //             unset($paramModifierItem['ModifierItemID']);
            //             $paramModifierItem['BranchID'] = $BranchIDTo;
            //             $paramModifierItem['RestaurantID'] = $RestaurantIDTo;
            //             $paramModifierGroup['ModifierGroupID'] = $NewModifierGroupID[$i];
            //             $result = $DBFnB->table('ModifierItem')
            //             ->insert($paramModifierItem);
            //         }
            //
            //     }
            //
            //     for($i=0;$i<count($MenuModifier);$i++)
            //     {
            //         $paramMenuModifier = $this->cloneParameter($MenuModifier[$i]);
            //         unset($paramMenuModifier['MenuModifierID']);
            //         $paramMenuModifier['BranchID'] = $BranchIDTo;
            //         $paramMenuModifier['RestaurantID'] = $RestaurantIDTo;
            //         $result = $DBFnB->table('MenuModifier')
            //         ->insert($paramMenuModifier);
            //     }
            //     for($i=0;$i<count($NewMenuID);$i++)
            //     {
            //         $DBFnB->table('MenuModifier')
            //         ->where('MenuID',$OldMenuID[$i])
            //         ->where('BranchID',$BranchIDTo)
            //         ->update(array('MenuID' => $NewMenuID[$i]));
            //     }
            //     for($i=0;$i<count($NewModifierGroupID);$i++)
            //     {
            //         $DBFnB->table('MenuModifier')
            //         ->where('ModifierGroupID',$OldModifierGroupID[$i])
            //         ->where('BranchID',$BranchIDTo)
            //         ->update(array('ModifierGroupID' => $NewModifierGroupID[$i]));
            //     }
            // }
            // elseif($Item == true)
            // {
            //
            //     $category = $DBFnB->table('Category')
            //     ->where('BranchID',$BranchIDFrom)
            //     ->whereRaw('("Archived" is null or "Archived" = \'N\')')
            //     ->get();
            //
            //     for($j=0;$j<count($category);$j++)
            //     {
            //         $menu = $DBFnB->table('Menu')
            //         ->where('CategoryID',$category[$j]->CategoryID)
            //         ->whereRaw('("Archived" is null or "Archived" = \'N\')')
            //         ->get();
            //
            //         $paramCategory = $this->cloneParameter($category[$j]);
            //         $paramCategory['BranchID'] = $BranchIDTo;
            //         $paramCategory['RestaurantID'] = $RestaurantIDTo;
            //         unset($paramCategory['CategoryID']);
            //
            //         $result = $DBFnB->table('Category')
            //         ->insert($paramCategory);
            //
            //         $CategoryID = $DBFnB->select('select lastval() AS id')[0]->id;
            //         for($k=0;$k<count($menu);$k++)
            //         {
            //             $paramMenu = $this->cloneParameter($menu[$k]);
            //             $paramMenu['CategoryID'] = $CategoryID;
            //             $paramMenu['BranchID'] = $BranchIDTo;
            //             $paramMenu['RestaurantID'] = $RestaurantIDTo;
            //             unset($paramMenu['MenuID']);
            //
            //             $result = $DBFnB->table('Menu')
            //             ->insert($paramMenu);
            //         }
            //
            //     }
            // }
            // elseif($Category == true)
            // {
            //     $category = $DBFnB->table('Category')
            //     ->where('BranchID',$BranchIDFrom)
            //     ->whereRaw('("Archived" is null or "Archived" = \'N\')')
            //     ->get();
            //     for($j=0;$j<count($category);$j++)
            //     {
            //         $paramCategory = $this->cloneParameter($category[$j]);
            //         $paramCategory['BranchID'] = $BranchIDTo;
            //         $paramCategory['RestaurantID'] = $RestaurantIDTo;
            //         unset($paramCategory['CategoryID']);
            //
            //         $result = $DBFnB->table('Category')
            //         ->insert($paramCategory);
            //
            //         $CategoryID = $DBFnB->select('select lastval() AS id')[0]->id;
            //     }
            // }

            if($Ingredients == true)
            {
                if($Modifier == true)
                {

                    $category = $DBFnB->table('Category')
                    ->where('BranchID',$BranchIDFrom)
                    ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                    ->get();
                    $ModifierGroup = $DBFnB->table('ModifierGroup')
                    ->where('BranchID',$BranchIDFrom)
                    ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                    ->get();
                    $MenuModifier = $DBFnB->table('MenuModifier')
                    ->where('BranchID',$BranchIDFrom)
                    ->get();


                    for($j=0;$j<count($category);$j++)
                    {
                        $menu = $DBFnB->table('Menu')
                        ->where('CategoryID',$category[$j]->CategoryID)
                        ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                        ->get();


                        $paramCategory = $this->cloneParameter($category[$j]);
                        $paramCategory['BranchID'] = $BranchIDTo;
                        $paramCategory['RestaurantID'] = $RestaurantIDTo;
                        unset($paramCategory['CategoryID']);

                        $result = $DBFnB->table('Category')
                        ->insert($paramCategory);

                        $CategoryID = $DBFnB->select('select lastval() AS id')[0]->id;


                        for($k=0;$k<count($menu);$k++)
                        {

                            $paramMenu = $this->cloneParameter($menu[$k]);
                            $paramMenu['CategoryID'] = $CategoryID;
                            $paramMenu['BranchID'] = $BranchIDTo;
                            $paramMenu['RestaurantID'] = $RestaurantIDTo;
                            unset($paramMenu['MenuID']);

                            $result = $DBFnB->table('Menu')
                            ->insert($paramMenu);

                            array_push($NewMenuID, $DBFnB->select('select lastval() AS id')[0]->id);
                            array_push($OldMenuID, $menu[$k]->MenuID);

                        }

                    }
                    // dd($ModifierGroup);
                    for($i=0;$i<count($ModifierGroup);$i++)
                    {
                        $paramModifierGroup = $this->cloneParameter($ModifierGroup[$i]);
                        unset($paramModifierGroup['ModifierGroupID']);
                        $paramModifierGroup['BranchID'] = $BranchIDTo;
                        $paramModifierGroup['RestaurantID'] = $RestaurantIDTo;
                        $result = $DBFnB->table('ModifierGroup')
                        ->insert($paramModifierGroup);

                        array_push($NewModifierGroupID, $DBFnB->select('select lastval() AS id')[0]->id);
                        array_push($OldModifierGroupID, $ModifierGroup[$i]->ModifierGroupID);
                        $ModifierItem = $DBFnB->table('ModifierItem')
                        ->where('ModifierGroupID',$ModifierGroup[$i]->ModifierGroupID)
                        ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                        ->get();
                        // print_r($ModifierGroup[$i]);
                        // print_r($ModifierItem);
                        for($j=0;$j<count($ModifierItem);$j++)
                        {
                                $paramModifierItem = $this->cloneParameter($ModifierItem[$j]);
                                unset($paramModifierItem['ModifierItemID']);
                                $paramModifierItem['BranchID'] = $BranchIDTo;
                                $paramModifierItem['RestaurantID'] = $RestaurantIDTo;
                                $paramModifierItem['ModifierGroupID'] = $NewModifierGroupID[$i];
                                $result = $DBFnB->table('ModifierItem')
                                ->insert($paramModifierItem);

                                array_push($NewModifierItemID, $DBFnB->select('select lastval() AS id')[0]->id);
                                array_push($OldModifierItemID, $ModifierItem[$j]->ModifierItemID);
                        }
                    }

                    for($i=0;$i<count($MenuModifier);$i++)
                    {
                        $paramMenuModifier = $this->cloneParameter($MenuModifier[$i]);
                        unset($paramMenuModifier['MenuModifierID']);
                        $paramMenuModifier['BranchID'] = $BranchIDTo;
                        $paramMenuModifier['RestaurantID'] = $RestaurantIDTo;
                        $result = $DBFnB->table('MenuModifier')
                        ->insert($paramMenuModifier);
                    }
                    for($i=0;$i<count($NewMenuID);$i++)
                    {
                        $DBFnB->table('MenuModifier')
                        ->where('MenuID',$OldMenuID[$i])
                        ->where('BranchID',$BranchIDTo)
                        ->update(array('MenuID' => $NewMenuID[$i]));
                    }
                    for($i=0;$i<count($NewModifierGroupID);$i++)
                    {
                        $DBFnB->table('MenuModifier')
                        ->where('ModifierGroupID',$OldModifierGroupID[$i])
                        ->where('BranchID',$BranchIDTo)
                        ->update(array('ModifierGroupID' => $NewModifierGroupID[$i]));
                    }
                }
                elseif($Item == true)
                {

                    $category = $DBFnB->table('Category')
                    ->where('BranchID',$BranchIDFrom)
                    ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                    ->get();

                    for($j=0;$j<count($category);$j++)
                    {
                        $menu = $DBFnB->table('Menu')
                        ->where('CategoryID',$category[$j]->CategoryID)
                        ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                        ->get();

                        $paramCategory = $this->cloneParameter($category[$j]);
                        $paramCategory['BranchID'] = $BranchIDTo;
                        $paramCategory['RestaurantID'] = $RestaurantIDTo;
                        unset($paramCategory['CategoryID']);

                        $result = $DBFnB->table('Category')
                        ->insert($paramCategory);

                        $CategoryID = $DBFnB->select('select lastval() AS id')[0]->id;
                        for($k=0;$k<count($menu);$k++)
                        {
                            $paramMenu = $this->cloneParameter($menu[$k]);
                            $paramMenu['CategoryID'] = $CategoryID;
                            $paramMenu['BranchID'] = $BranchIDTo;
                            $paramMenu['RestaurantID'] = $RestaurantIDTo;
                            unset($paramMenu['MenuID']);

                            $result = $DBFnB->table('Menu')
                            ->insert($paramMenu);
                        }

                    }
                }
                elseif($Category == true)
                {
                    $category = $DBFnB->table('Category')
                    ->where('BranchID',$BranchIDFrom)
                    ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                    ->get();
                    for($j=0;$j<count($category);$j++)
                    {
                        $paramCategory = $this->cloneParameter($category[$j]);
                        $paramCategory['BranchID'] = $BranchIDTo;
                        $paramCategory['RestaurantID'] = $RestaurantIDTo;
                        unset($paramCategory['CategoryID']);

                        $result = $DBFnB->table('Category')
                        ->insert($paramCategory);

                        $CategoryID = $DBFnB->select('select lastval() AS id')[0]->id;
                    }
                }

                if($Inventory == true)
                {
                    $inventoryCategory = $DBFnB->table('InventoryCategory')
                    ->where('BranchID',$BranchIDFrom)
                    ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                    ->get();

                    for($i=0;$i<count($inventoryCategory);$i++)
                    {
                        $paramInventoryCategory = $this->cloneParameter($inventoryCategory[$i]);
                        $paramInventoryCategory['BranchID'] = $BranchIDTo;
                        $paramInventoryCategory['RestaurantID'] = $RestaurantIDTo;
                        unset($paramInventoryCategory['InventoryCategoryID']);

                        $result = $DBFnB->table('InventoryCategory')
                        ->insert($paramInventoryCategory);

                        $InventoryCategoryID = $DBFnB->select('select lastval() AS id')[0]->id;

                        $inventory = $DBFnB->table('Inventory')
                        ->where('BranchID',$BranchIDFrom)
                        ->where('InventoryCategoryID',$inventoryCategory[$i]->InventoryCategoryID)
                        ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                        ->get()->toArray();

                        for($j=0;$j<count($inventory);$j++)
                        {

                            $paramInventory = $this->cloneParameter($inventory[$j]);
                            $paramInventory['BranchID'] = $BranchIDTo;
                            $paramInventory['RestaurantID'] = $RestaurantIDTo;
                            $paramInventory['InventoryCategoryID'] = $InventoryCategoryID;
                            unset($paramInventory['CurrentStock']);
                            unset($paramInventory['InventoryID']);

                            $result = $DBFnB->table('Inventory')
                            ->insert($paramInventory);

                            $InventoryID = $DBFnB->select('select lastval() AS id')[0]->id;
                            array_push($NewInventoryID, $DBFnB->select('select lastval() AS id')[0]->id);
                            array_push($OldInventoryID, $inventory[$j]->InventoryID);
                            $ingredient = $DBFnB->table('Ingredient')
                            ->where('BranchID',$BranchIDFrom)
                            ->where('InventoryID',$inventory[$j]->InventoryID)
                            ->get()->toArray();

                            for($k=0;$k<count($ingredient);$k++)
                            {
                                $paramIngredient = $this->cloneParameter($ingredient[$k]);
                                $paramIngredient['BranchID'] = $BranchIDTo;
                                $paramIngredient['RestaurantID'] = $RestaurantIDTo;
                                $paramIngredient['InventoryID'] = $InventoryID;

                                unset($paramIngredient['IngredientID']);

                                $result = $DBFnB->table('Ingredient')
                                ->insert($paramIngredient);
                            }

                        }

                    }

                    for($i=0;$i<count($OldMenuID);$i++)
                    {
                        $result = $DBFnB->table('Ingredient')
                        ->where('BranchID',$BranchIDTo)
                        ->where('IngredientForType',1)
                        ->where('IngredientForID', $OldMenuID[$i])
                        ->update(array('IngredientForID' => $NewMenuID[$i]));
                    }

                    for($i=0;$i<count($OldInventoryID);$i++)
                    {
                        $result = $DBFnB->table('Ingredient')
                        ->where('BranchID',$BranchIDTo)
                        ->where('IngredientForType',3)
                        ->where('IngredientForID', $OldInventoryID[$i])
                        ->update(array('IngredientForID' => $NewInventoryID[$i]));
                    }

                    for($i=0;$i<count($OldModifierItemID);$i++)
                    {
                        $result = $DBFnB->table('Ingredient')
                        ->where('BranchID',$BranchIDTo)
                        ->where('IngredientForType',2)
                        ->where('IngredientForID', $OldModifierItemID[$i])
                        ->update(array('IngredientForID' => $NewModifierItemID[$i]));
                    }

                }
                elseif($InventoryCategory == true)
                {
                    $inventoryCategory = $DBFnB->table('InventoryCategory')
                    ->where('BranchID',$BranchIDFrom)
                    ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                    ->get();

                    for($i=0;$i<count($inventoryCategory);$i++)
                    {
                        $paramInventoryCategory = $this->cloneParameter($inventoryCategory[$i]);
                        $paramInventoryCategory['BranchID'] = $BranchIDTo;
                        $paramInventoryCategory['RestaurantID'] = $RestaurantIDTo;
                        unset($paramInventoryCategory['InventoryCategoryID']);

                        $result = $DBFnB->table('InventoryCategory')
                        ->insert($paramInventoryCategory);
                    }
                }


            }
            else{
                if($Modifier == true)
                {

                    $category = $DBFnB->table('Category')
                    ->where('BranchID',$BranchIDFrom)
                    ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                    ->get();
                    $ModifierGroup = $DBFnB->table('ModifierGroup')
                    ->where('BranchID',$BranchIDFrom)
                    ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                    ->get();
                    $MenuModifier = $DBFnB->table('MenuModifier')
                    ->where('BranchID',$BranchIDFrom)
                    ->get();


                    for($j=0;$j<count($category);$j++)
                    {
                        $menu = $DBFnB->table('Menu')
                        ->where('CategoryID',$category[$j]->CategoryID)
                        ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                        ->get();


                        $paramCategory = $this->cloneParameter($category[$j]);
                        $paramCategory['BranchID'] = $BranchIDTo;
                        $paramCategory['RestaurantID'] = $RestaurantIDTo;
                        unset($paramCategory['CategoryID']);

                        $result = $DBFnB->table('Category')
                        ->insert($paramCategory);

                        $CategoryID = $DBFnB->select('select lastval() AS id')[0]->id;


                        for($k=0;$k<count($menu);$k++)
                        {

                            $paramMenu = $this->cloneParameter($menu[$k]);
                            $paramMenu['CategoryID'] = $CategoryID;
                            $paramMenu['BranchID'] = $BranchIDTo;
                            $paramMenu['RestaurantID'] = $RestaurantIDTo;
                            unset($paramMenu['MenuID']);

                            $result = $DBFnB->table('Menu')
                            ->insert($paramMenu);

                            array_push($NewMenuID, $DBFnB->select('select lastval() AS id')[0]->id);
                            array_push($OldMenuID, $menu[$k]->MenuID);

                        }

                    }
                    for($i=0;$i<count($ModifierGroup);$i++)
                    {
                        $paramModifierGroup = $this->cloneParameter($ModifierGroup[$i]);
                        unset($paramModifierGroup['ModifierGroupID']);
                        $paramModifierGroup['BranchID'] = $BranchIDTo;
                        $paramModifierGroup['RestaurantID'] = $RestaurantIDTo;
                        $result = $DBFnB->table('ModifierGroup')
                        ->insert($paramModifierGroup);

                        array_push($NewModifierGroupID, $DBFnB->select('select lastval() AS id')[0]->id);
                        array_push($OldModifierGroupID, $ModifierGroup[$i]->ModifierGroupID);
                        $ModifierItem = $DBFnB->table('ModifierItem')
                        ->where('ModifierGroupID',$ModifierGroup[$i]->ModifierGroupID)
                        ->get();

                        for($j=0;$j<count($ModifierItem);$j++)
                        {
                            $paramModifierItem = $this->cloneParameter($ModifierItem[$j]);
                            unset($paramModifierItem['ModifierItemID']);
                            $paramModifierItem['BranchID'] = $BranchIDTo;
                            $paramModifierItem['RestaurantID'] = $RestaurantIDTo;
                            $paramModifierGroup['ModifierGroupID'] = $NewModifierGroupID[$i];
                            $result = $DBFnB->table('ModifierItem')
                            ->insert($paramModifierItem);
                        }
                    }
                    for($i=0;$i<count($MenuModifier);$i++)
                    {
                        $paramMenuModifier = $this->cloneParameter($MenuModifier[$i]);
                        unset($paramMenuModifier['MenuModifierID']);
                        $paramMenuModifier['BranchID'] = $BranchIDTo;
                        $paramMenuModifier['RestaurantID'] = $RestaurantIDTo;
                        $result = $DBFnB->table('MenuModifier')
                        ->insert($paramMenuModifier);
                    }
                    for($i=0;$i<count($NewMenuID);$i++)
                    {
                        $DBFnB->table('MenuModifier')
                        ->where('MenuID',$OldMenuID[$i])
                        ->where('BranchID',$BranchIDTo)
                        ->update(array('MenuID' => $NewMenuID[$i]));
                    }
                    for($i=0;$i<count($NewModifierGroupID);$i++)
                    {
                        $DBFnB->table('MenuModifier')
                        ->where('ModifierGroupID',$OldModifierGroupID[$i])
                        ->where('BranchID',$BranchIDTo)
                        ->update(array('ModifierGroupID' => $NewModifierGroupID[$i]));
                    }
                }
                elseif($Item == true)
                {

                    $category = $DBFnB->table('Category')
                    ->where('BranchID',$BranchIDFrom)
                    ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                    ->get();

                    for($j=0;$j<count($category);$j++)
                    {
                        $menu = $DBFnB->table('Menu')
                        ->where('CategoryID',$category[$j]->CategoryID)
                        ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                        ->get();

                        $paramCategory = $this->cloneParameter($category[$j]);
                        $paramCategory['BranchID'] = $BranchIDTo;
                        $paramCategory['RestaurantID'] = $RestaurantIDTo;
                        unset($paramCategory['CategoryID']);

                        $result = $DBFnB->table('Category')
                        ->insert($paramCategory);

                        $CategoryID = $DBFnB->select('select lastval() AS id')[0]->id;
                        for($k=0;$k<count($menu);$k++)
                        {
                            $paramMenu = $this->cloneParameter($menu[$k]);
                            $paramMenu['CategoryID'] = $CategoryID;
                            $paramMenu['BranchID'] = $BranchIDTo;
                            $paramMenu['RestaurantID'] = $RestaurantIDTo;
                            unset($paramMenu['MenuID']);

                            $result = $DBFnB->table('Menu')
                            ->insert($paramMenu);
                        }

                    }
                }
                elseif($Category == true)
                {
                    $category = $DBFnB->table('Category')
                    ->where('BranchID',$BranchIDFrom)
                    ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                    ->get();
                    for($j=0;$j<count($category);$j++)
                    {
                        $paramCategory = $this->cloneParameter($category[$j]);
                        $paramCategory['BranchID'] = $BranchIDTo;
                        $paramCategory['RestaurantID'] = $RestaurantIDTo;
                        unset($paramCategory['CategoryID']);

                        $result = $DBFnB->table('Category')
                        ->insert($paramCategory);

                        $CategoryID = $DBFnB->select('select lastval() AS id')[0]->id;
                    }
                }

                if($Inventory == true)
                {
                    $inventoryCategory = $DBFnB->table('InventoryCategory')
                    ->where('BranchID',$BranchIDFrom)
                    ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                    ->get();

                    for($i=0;$i<count($inventoryCategory);$i++)
                    {
                        $paramInventoryCategory = $this->cloneParameter($inventoryCategory[$i]);
                        $paramInventoryCategory['BranchID'] = $BranchIDTo;
                        $paramInventoryCategory['RestaurantID'] = $RestaurantIDTo;
                        unset($paramInventoryCategory['InventoryCategoryID']);

                        $result = $DBFnB->table('InventoryCategory')
                        ->insert($paramInventoryCategory);

                        $InventoryCategoryID = $DBFnB->select('select lastval() AS id')[0]->id;

                        $inventory = $DBFnB->table('Inventory')
                        ->where('BranchID',$BranchIDFrom)
                        ->where('InventoryCategoryID',$inventoryCategory[$i]->InventoryCategoryID)
                        ->get()->toArray();
                        for($j=0;$j<count($inventory);$j++)
                        {
                            $paramInventory = $this->cloneParameter($inventory[$j]);
                            $paramInventory['BranchID'] = $BranchIDTo;
                            $paramInventory['RestaurantID'] = $RestaurantIDTo;
                            $paramInventory['InventoryCategoryID'] = $InventoryCategoryID;
                            unset($paramInventory['InventoryID']);

                            $result = $DBFnB->table('Inventory')
                            ->insert($paramInventory);
                        }
                    }
                }
                elseif($InventoryCategory == true)
                {
                    $inventoryCategory = $DBFnB->table('InventoryCategory')
                    ->where('BranchID',$BranchIDFrom)
                    ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                    ->get();

                    for($i=0;$i<count($inventoryCategory);$i++)
                    {
                        $paramInventoryCategory = $this->cloneParameter($inventoryCategory);
                        $paramInventoryCategory['BranchID'] = $BranchIDTo;
                        $paramInventoryCategory['RestaurantID'] = $RestaurantIDTo;
                        unset($paramInventoryCategory['InventoryCategoryID']);

                        $result = $DBFnB->table('InventoryCategory')
                        ->insert($paramInventoryCategory);
                    }
                }

            }

        }
        elseif($ProductID == "RET")
        {
            $CategoryDelete = $DBRetail->table('Category')
            ->where('BranchID',$BranchIDTo)
            ->update(array('Archived' => "Y"));
            $ItemDelete = $DBRetail->table('Item')
            ->where('BranchID',$BranchIDTo)
            ->update(array('Archived' => "Y"));
            $ItemVariantDelete = $DBRetail->table('ItemVariant')
            ->where('BranchID',$BranchIDTo)
            ->update(array('Archived' => "Y"));
            $ItemVariant = false;
            $Item = false;
            $Category = false;
            $NewItemVariantID = array();
            $OldItemVariantID = array();
            $NewItemVariantKeyID = array();
            $OldItemVariantKeyID = array();
            $OldVariantKeyID = array();
            $NewVariantKeyID = array();
            $OldItemID = array();
            $NewItemID = array();
            $OldCategoryID = array();
            $NewCategoryID = array();
            $OldCategorySpecificationID = array();
            $NewCategorySpecificationID = array();
            $OldItemSpecificationID = array();
            $NewItemSpecificationID = array();

            $RETID = $DBRetail->table('Branch')
            ->where('BranchID',$BranchIDTo)
            ->first();
            $StoreIDTo = $RETID->StoreID;
            for($i =0;$i<count($CopyList);$i++)
            {
                if($CopyList[$i] == "Variant")
                $ItemVariant = true;
                elseif($CopyList[$i] == "Item")
                $Item = true;
                else {
                    $Category = true;
                }
            }

            if($ItemVariant == true)
            {
                $category = $DBRetail->table('Category')
                ->where('BranchID',$BranchIDFrom)
                ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                ->get();
                //insert category, item,item variant.
                for($i=0;$i<count($category);$i++)
                {
                    $item = $DBRetail->table('Item')
                    ->where('CategoryID',$category[$i]->CategoryID)
                    ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                    ->get();

                    $paramCategory = $this->cloneParameter($category[$i]);
                    $paramCategory['BranchID'] = $BranchIDTo;
                    $paramCategory['StoreID'] = $StoreIDTo;
                    unset($paramCategory['CategoryID']);

                    $result = $DBRetail->table('Category')
                    ->insert($paramCategory);
                    $CategoryID = $DBRetail->select('select lastval() AS id')[0]->id;
                    array_push($NewCategoryID, $DBRetail->select('select lastval() AS id')[0]->id);
                    array_push($OldCategoryID, $category[$i]->CategoryID);
                    for($j=0;$j<count($item);$j++)
                    {
                        $itemVariant = $DBRetail->table('ItemVariant')
                        ->where('ItemID',$item[$j]->ItemID)
                        ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                        ->get();

                        $itemVariantKey = $DBRetail->table('ItemVariantKey')
                        ->where('ItemID',$item[$j]->ItemID)
                        ->get();


                        $paramItem = $this->cloneParameter($item[$j]);
                        $paramItem['BranchID'] = $BranchIDTo;
                        $paramItem['StoreID'] = $StoreIDTo;
                        $paramItem['CategoryID'] = $CategoryID;
                        unset($paramItem['ItemID']);

                        $result = $DBRetail->table('Item')
                        ->insert($paramItem);
                        $ItemID = $DBRetail->select('select lastval() AS id')[0]->id;
                        array_push($NewItemID, $DBRetail->select('select lastval() AS id')[0]->id);
                        array_push($OldItemID, $item[$j]->ItemID);
                        for($k=0;$k<count($itemVariant);$k++)
                        {
                            $paramItemVariant = $this->cloneParameter($itemVariant[$k]);
                            $paramItemVariant['BranchID'] = $BranchIDTo;
                            $paramItemVariant['StoreID'] = $StoreIDTo;
                            $paramItemVariant['ItemID'] = $ItemID;
                            unset($paramItemVariant['ItemVariantID']);

                            $result = $DBRetail->table('ItemVariant')
                            ->insert($paramItemVariant);
                            array_push($NewItemVariantID, $DBRetail->select('select lastval() AS id')[0]->id);
                            array_push($OldItemVariantID, $itemVariant[$k]->ItemVariantID);
                        }

                        for($k=0;$k<count($itemVariantKey);$k++)
                        {
                            $paramItemVariantKey = $this->cloneParameter($itemVariantKey[$k]);
                            $paramItemVariantKey['BranchID'] = $BranchIDTo;
                            $paramItemVariantKey['StoreID'] = $StoreIDTo;
                            $paramItemVariantKey['ItemID'] = $ItemID;
                            unset($paramItemVariantKey['ItemVariantKeyID']);

                            $result = $DBRetail->table('ItemVariantKey')
                            ->insert($paramItemVariantKey);
                            array_push($NewItemVariantKeyID, $DBRetail->select('select lastval() AS id')[0]->id);
                            array_push($OldItemVariantKeyID, $itemVariantKey[$k]->ItemVariantKeyID);
                        }

                    }
                }
                //insert variant key.
                for($i=0;$i<count($OldItemVariantID);$i++)
                {
                    $VariantKey = $DBRetail->table('VariantKey')
                    ->where('ItemVariantID',$OldItemVariantID[$i])
                    ->get();

                    for($j=0;$j<count($VariantKey);$j++)
                    {
                        $paramVariantKey = $this->cloneParameter($VariantKey[$j]);
                        $paramVariantKey['ItemVariantID'] = $NewItemVariantID[$i];
                        unset($paramVariantKey['VariantKeyID']);

                        $result = $DBRetail->table('VariantKey')
                        ->insert($paramVariantKey);
                        array_push($NewVariantKeyID, $DBRetail->select('select lastval() AS id')[0]->id);
                        array_push($OldVariantKeyID, $VariantKey[$j]->VariantKeyID);
                    }

                }

                //update variant key.
                for($i=0;$i<count($NewVariantKeyID);$i++)
                {
                    for($j=0;$j<count($NewItemVariantKeyID);$j++)
                    {
                        $result = $DBRetail->table('VariantKey')
                        ->where('VariantKeyID',$NewVariantKeyID[$i])
                        ->where('ItemVariantKeyID',$OldItemVariantKeyID[$j])
                        ->update(array('ItemVariantKeyID' => $NewItemVariantKeyID[$j]));
                    }
                }

                //insert categoryspecification.
                for($i=0;$i<count($OldCategoryID);$i++)
                {
                    $categorySpecification = $DBRetail->table('CategorySpecification')
                    ->where('CategoryID', $OldCategoryID[$i])
                    ->get();
                    for($j=0;$j<count($categorySpecification);$j++)
                    {
                        $paramCategorySpecification = $this->cloneParameter($categorySpecification[$j]);
                        $paramCategorySpecification['CategoryID'] = $NewCategoryID[$i];
                        $paramCategorySpecification['BranchID'] = $BranchIDTo;
                        $paramCategorySpecification['StoreID'] = $StoreIDTo;
                        unset($paramCategorySpecification['SpecificationID']);

                        $result = $DBRetail->table('CategorySpecification')
                        ->insert($paramCategorySpecification);
                        array_push($NewCategorySpecificationID, $DBRetail->select('select lastval() AS id')[0]->id);
                        array_push($OldCategorySpecificationID, $categorySpecification[$j]->SpecificationID);
                    }
                }
                //insert itemspecification.
                for($i=0;$i<count($OldCategorySpecificationID);$i++)
                {
                    $itemSpecification = $DBRetail->table('ItemSpecification')
                    ->where('CategorySpecificationID',$OldCategorySpecificationID[$i])
                    ->get();

                    for($j=0;$j<count($itemSpecification);$j++)
                    {
                        $paramItemSpecification = $this->cloneParameter($itemSpecification[$j]);
                        $paramItemSpecification['BranchID'] = $BranchIDTo;
                        $paramItemSpecification['StoreID'] = $StoreIDTo;
                        $paramItemSpecification['CategorySpecificationID'] = $NewCategorySpecificationID[$i];
                        unset($paramItemSpecification['ItemSpecificationID']);

                        $result = $DBRetail->table('ItemSpecification')
                        ->insert($paramItemSpecification);
                        array_push($NewItemSpecificationID, $DBRetail->select('select lastval() AS id')[0]->id);
                        array_push($OldItemSpecificationID, $itemSpecification[$j]->ItemSpecificationID);
                    }
                }

                //update mapingan itemspecification ke itemid nya.
                for($i=0;$i<count($NewItemSpecificationID);$i++)
                {
                    for($j=0;$j<count($NewItemID);$j++)
                    {
                        $result = $DBRetail->table('ItemSpecification')
                        ->where('ItemSpecificationID',$NewItemSpecificationID[$i])
                        ->where('ItemID',$OldItemID[$j])
                        ->update(array('ItemID' => $NewItemID[$j]));
                    }
                }
            }
            elseif($Item == true)
            {
                $category = $DBRetail->table('Category')
                ->where('BranchID',$BranchIDFrom)
                ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                ->get();
                //insert category, item.
                for($i=0;$i<count($category);$i++)
                {
                    $item = $DBRetail->table('Item')
                    ->where('CategoryID',$category[$i]->CategoryID)
                    ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                    ->get();

                    $paramCategory = $this->cloneParameter($category[$i]);
                    $paramCategory['BranchID'] = $BranchIDTo;
                    $paramCategory['StoreID'] = $StoreIDTo;
                    unset($paramCategory['CategoryID']);

                    $result = $DBRetail->table('Category')
                    ->insert($paramCategory);
                    $CategoryID = $DBRetail->select('select lastval() AS id')[0]->id;
                    array_push($NewCategoryID, $DBRetail->select('select lastval() AS id')[0]->id);
                    array_push($OldCategoryID, $category[$i]->CategoryID);

                    for($j=0;$j<count($item);$j++)
                    {
                        $itemVariant = $DBRetail->table('ItemVariant')
                        ->where('ItemID',$item[$j]->ItemID)
                        ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                        ->get();

                        $paramItem = $this->cloneParameter($item[$j]);
                        $paramItem['BranchID'] = $BranchIDTo;
                        $paramItem['StoreID'] = $StoreIDTo;
                        $paramItem['CategoryID'] = $CategoryID;
                        unset($paramItem['ItemID']);

                        $result = $DBRetail->table('Item')
                        ->insert($paramItem);
                        array_push($NewItemID, $DBRetail->select('select lastval() AS id')[0]->id);
                        array_push($OldItemID, $item[$j]->ItemID);
                    }
                }
                //insert categoryspecification.
                for($i=0;$i<count($OldCategoryID);$i++)
                {
                    $categorySpecification = $DBRetail->table('CategorySpecification')
                    ->where('CategoryID', $OldCategoryID[$i])
                    ->get();
                    for($j=0;$j<count($categorySpecification);$j++)
                    {
                        $paramCategorySpecification = $this->cloneParameter($categorySpecification[$j]);
                        $paramCategorySpecification['CategoryID'] = $NewCategoryID[$i];
                        $paramCategorySpecification['BranchID'] = $BranchIDTo;
                        $paramCategorySpecification['StoreID'] = $StoreIDTo;
                        unset($paramCategorySpecification['SpecificationID']);

                        $result = $DBRetail->table('CategorySpecification')
                        ->insert($paramCategorySpecification);
                        array_push($NewCategorySpecificationID, $DBRetail->select('select lastval() AS id')[0]->id);
                        array_push($OldCategorySpecificationID, $categorySpecification[$j]->SpecificationID);
                    }
                }
                //insert itemspecification.
                for($i=0;$i<count($OldCategorySpecificationID);$i++)
                {
                    $itemSpecification = $DBRetail->table('ItemSpecification')
                    ->where('CategorySpecificationID',$OldCategorySpecificationID[$i])
                    ->get();

                    for($j=0;$j<count($itemSpecification);$j++)
                    {
                        $paramItemSpecification = $this->cloneParameter($itemSpecification[$j]);
                        $paramItemSpecification['BranchID'] = $BranchIDTo;
                        $paramItemSpecification['StoreID'] = $StoreIDTo;
                        $paramItemSpecification['CategorySpecificationID'] = $NewCategorySpecificationID[$i];
                        unset($paramItemSpecification['ItemSpecificationID']);

                        $result = $DBRetail->table('ItemSpecification')
                        ->insert($paramItemSpecification);
                        array_push($NewItemSpecificationID, $DBRetail->select('select lastval() AS id')[0]->id);
                        array_push($OldItemSpecificationID, $itemSpecification[$j]->ItemSpecificationID);
                    }
                }

                //update mapingan itemspecification ke itemid nya.
                for($i=0;$i<count($NewItemSpecificationID);$i++)
                {
                    for($j=0;$j<count($NewItemID);$j++)
                    {
                        $result = $DBRetail->table('ItemSpecification')
                        ->where('ItemSpecificationID',$NewItemSpecificationID[$i])
                        ->where('ItemID',$OldItemID[$j])
                        ->update(array('ItemID' => $NewItemID[$j]));
                    }
                }
            }
            elseif($Category == true)
            {
                $category = $DBRetail->table('Category')
                ->where('BranchID',$BranchIDFrom)
                ->whereRaw('("Archived" is null or "Archived" = \'N\')')
                ->get();
                for($i=0;$i<count($category);$i++)
                {
                    $paramCategory = $this->cloneParameter($category[$i]);
                    $paramCategory['BranchID'] = $BranchIDTo;
                    $paramCategory['StoreID'] = $StoreIDTo;
                    unset($paramCategory['CategoryID']);

                    $result = $DBRetail->table('Category')
                    ->insert($paramCategory);

                    array_push($NewCategoryID, $DBRetail->select('select lastval() AS id')[0]->id);
                    array_push($OldCategoryID, $category[$i]->CategoryID);
                }
                //insert categoryspecification.
                for($i=0;$i<count($OldCategoryID);$i++)
                {
                    $categorySpecification = $DBRetail->table('CategorySpecification')
                    ->where('CategoryID', $OldCategoryID[$i])
                    ->get();
                    for($j=0;$j<count($categorySpecification);$j++)
                    {
                        $paramCategorySpecification = $this->cloneParameter($categorySpecification[$j]);
                        $paramCategorySpecification['CategoryID'] = $NewCategoryID[$i];
                        $paramCategorySpecification['BranchID'] = $BranchIDTo;
                        $paramCategorySpecification['StoreID'] = $StoreIDTo;
                        unset($paramCategorySpecification['SpecificationID']);

                        $result = $DBRetail->table('CategorySpecification')
                        ->insert($paramCategorySpecification);
                        array_push($NewCategorySpecificationID, $DBRetail->select('select lastval() AS id')[0]->id);
                        array_push($OldCategorySpecificationID, $categorySpecification[$j]->SpecificationID);
                    }
                }

            }

        }
        elseif($ProductID == "SER")
        {
            $ServiceDelete = $DBSalon->table('Service')
            ->where('BranchID',$BranchIDTo)
            ->update(array('Archived' => $now));
            $SubServiceDelete = $DBSalon->table('SubService')
            ->where('BranchID',$BranchIDTo)
            ->update(array('Archived' => $now));
            $CategoryDelete = $DBSalon->table('Category')
            ->where('BranchID',$BranchIDTo)
            ->update(array('Archived' => $now));
            $ItemDelete = $DBSalon->table('Item')
            ->where('BranchID',$BranchIDTo)
            ->update(array('Archived' => $now));
            $ServiceInventoryUsageDelete = $DBSalon->table('ServiceInventoryUsage')
            ->whereRaw('"ServiceID" in (select "ServiceID" from "Service" where "Archived" is not null) or
            "SubServiceID" in (select "SubServiceID" from "SubService" where "Archived" is not null)')
            ->delete();
            $Service = false;
            $SubService = false;
            $Category = false;
            $Item = false;
            $ServiceInventoryUsage = false;
            $SERID = $DBSalon->table('Branch')
            ->where('BranchID',$BranchIDTo)
            ->first();
            $BrandIDTo = $SERID->BrandID;
            $NewItemID = array();
            $OldItemID = array();
            $NewSubServiceID = array();
            $OldSubServiceID = array();
            $NewServiceID = array();
            $OldServiceID = array();
            for($i =0;$i<count($CopyList);$i++)
            {
                if($CopyList[$i] == "Ingredients")
                $ServiceInventoryUsage = true;
                elseif($CopyList[$i] == "SubService")
                $SubService = true;
                elseif($CopyList[$i] == "Service")
                $Service = true;
                elseif($CopyList[$i] == "Item")
                $Item = true;
                elseif($CopyList[$i] == "Category")
                $Category = true;
            }
            if($ServiceInventoryUsage == true)
            {
                $category = $DBSalon->table('Category')
                ->where('BranchID',$BranchIDFrom)
                ->whereRaw('("Archived" is null)')
                ->get();

                for($i=0;$i<count($category);$i++)
                {
                    $paramCategory = $this->cloneParameter($category[$i]);
                    $paramCategory['BranchID'] = $BranchIDTo;
                    $paramCategory['BrandID'] = $BrandIDTo;
                    unset($paramCategory['CategoryID']);

                    $result = $DBSalon->table('Category')
                    ->insert($paramCategory);
                    $CategoryID = $DBSalon->select('select lastval() AS id')[0]->id;

                    $Item = $DBSalon->table('Item')
                    ->where('BranchID',$BranchIDFrom)
                    ->where('CategoryID',$category[$i]->CategoryID)
                    ->whereRaw('("Archived" is null)')
                    ->get();

                    for($j=0;$j<count($Item);$j++)
                    {
                        $paramItem = $this->cloneParameter($Item[$j]);
                        $paramItem['CategoryID'] = $CategoryID;
                        $paramItem['BranchID'] = $BranchIDTo;
                        $paramItem['BrandID'] = $BrandIDTo;
                        unset($paramItem['ItemID']);

                        $result = $DBSalon->table('Item')
                        ->insert($paramItem);

                        array_push($NewItemID, $DBSalon->select('select lastval() AS id')[0]->id);
                        array_push($OldItemID, $Item[$j]->ItemID);

                    }

                }

                $Service = $DBSalon->table('Service')
                ->where('BranchID',$BranchIDFrom)
                ->whereRaw('("Archived" is null)')
                ->get();

                for($i = 0;$i<count($Service);$i++)
                {
                    $paramService = $this->cloneParameter($Service[$i]);
                    $paramService['BranchID'] = $BranchIDTo;
                    $paramService['BrandID'] = $BrandIDTo;
                    unset($paramService['ServiceID']);

                    $result = $DBSalon->table('Service')
                    ->insert($paramService);

                    array_push($NewServiceID, $DBSalon->select('select lastval() AS id')[0]->id);
                    array_push($OldServiceID, $Service[$j]->ServiceID);

                    $SubService = $DBSalon->table('SubService')
                    ->whereRaw('("Archived" is null)')
                    ->where('ServiceID',$OldServiceID[$i])
                    ->get();

                    for($j=0;$j<count($SubService);$j++)
                    {
                        $paramSubService = $this->cloneParameter($SubService[$j]);
                        $paramSubService['BranchID'] = $BranchIDTo;
                        $paramSubService['BrandID'] = $BrandIDTo;
                        $paramSubService['ServiceID'] = $NewServiceID[$i];
                        unset($paramSubService['SubServiceID']);

                        $result = $DBSalon->table('SubService')
                        ->insert($paramSubService);

                        array_push($NewSubServiceID, $DBSalon->select('select lastval() AS id')[0]->id);
                        array_push($OldSubServiceID, $SubService[$j]->SubServiceID);

                    }
                }

                for($i=0;$i<count($OldItemID);$i++)
                {
                    $ServiceInventoryUsage = $DBSalon->table('ServiceInventoryUsage')
                    ->where('ItemID',$OldItemID[$i])
                    ->get();
                    for($j=0;$j<count($ServiceInventoryUsage);$j++)
                    {
                        $paramServiceInventoryUsage = $this->cloneParameter($ServiceInventoryUsage[$j]);
                        $paramServiceInventoryUsage['ItemID'] = $NewItemID[$i];
                        unset($paramServiceInventoryUsage['ServiceInventoryUsageID']);
                        $result = $DBSalon->table('ServiceInventoryUsage')
                        ->insert($paramServiceInventoryUsage);
                    }
                }

                for($i=0;$i<count($NewItemID);$i++)
                {
                    for($j=0;$j<count($OldServiceID);$j++)
                    {
                        $ServiceInventoryUsage = $DBSalon->table('ServiceInventoryUsage')
                        ->where('ItemID',$NewItemID[$i])
                        ->where('ServiceID',$OldServiceID[$j])
                        ->update(array('ServiceID' => $NewServiceID[$j]));
                    }
                    for($j=0;$j<count($OldSubServiceID);$j++)
                    {
                        $ServiceInventoryUsage = $DBSalon->table('ServiceInventoryUsage')
                        ->where('ItemID',$NewItemID[$i])
                        ->where('ServiceID',$OldSubServiceID[$j])
                        ->update(array('ServiceID' => $NewSubServiceID[$j]));
                    }
                }

            }
            else{
                if($Item == true)
                {
                    $category = $DBSalon->table('Category')
                    ->where('BranchID',$BranchIDFrom)
                    ->whereRaw('("Archived" is null)')
                    ->get();

                    for($i=0;$i<count($category);$i++)
                    {
                        $paramCategory = $this->cloneParameter($category[$i]);
                        $paramCategory['BranchID'] = $BranchIDTo;
                        $paramCategory['BrandID'] = $BrandIDTo;
                        unset($paramCategory['CategoryID']);

                        $result = $DBSalon->table('Category')
                        ->insert($paramCategory);
                        $CategoryID = $DBSalon->select('select lastval() AS id')[0]->id;

                        $Item = $DBSalon->table('Item')
                        ->where('BranchID',$BranchIDFrom)
                        ->where('CategoryID',$category[$i]->CategoryID)
                        ->whereRaw('("Archived" is null)')
                        ->get();

                        for($j=0;$j<count($Item);$j++)
                        {
                            $paramItem = $this->cloneParameter($Item[$j]);
                            $paramItem['CategoryID'] = $CategoryID;
                            $paramItem['BranchID'] = $BranchIDTo;
                            $paramItem['BrandID'] = $BrandIDTo;
                            unset($paramItem['ItemID']);

                            $result = $DBSalon->table('Item')
                            ->insert($paramItem);
                            array_push($NewItemID, $DBSalon->select('select lastval() AS id')[0]->id);
                            array_push($OldItemID, $Item[$j]->ItemID);
                        }

                    }
                }
                elseif($Category == true)
                {
                    $category = $DBSalon->table('Category')
                    ->where('BranchID',$BranchIDFrom)
                    ->whereRaw('("Archived" is null)')
                    ->get();

                    for($i=0;$i<count($category);$i++)
                    {
                        $paramCategory = $this->cloneParameter($category[$i]);
                        $paramCategory['BranchID'] = $BranchIDTo;
                        $paramCategory['BrandID'] = $BrandIDTo;
                        unset($paramCategory['CategoryID']);

                        $result = $DBSalon->table('Category')
                        ->insert($paramCategory);
                    }
                }
                if($SubService == true)
                {
                    $Service = $DBSalon->table('Service')
                    ->where('BranchID',$BranchIDFrom)
                    ->whereRaw('("Archived" is null)')
                    ->get();

                    for($i = 0;$i<count($Service);$i++)
                    {
                        $paramService = $this->cloneParameter($Service[$i]);
                        $paramService['BranchID'] = $BranchIDTo;
                        $paramService['BrandID'] = $BrandIDTo;
                        unset($paramService['ServiceID']);

                        $result = $DBSalon->table('Service')
                        ->insert($paramService);
                        array_push($NewServiceID, $DBSalon->select('select lastval() AS id')[0]->id);
                        array_push($OldServiceID, $Service[$i]->ServiceID);
                        $SubService = $DBSalon->table('SubService')
                        ->whereRaw('("Archived" is null)')
                        ->where('ServiceID',$OldServiceID[$i])
                        ->get();

                        for($j=0;$j<count($SubService);$j++)
                        {
                            $paramSubService = $this->cloneParameter($SubService[$j]);
                            $paramSubService['BranchID'] = $BranchIDTo;
                            $paramSubService['BrandID'] = $BrandIDTo;
                            $paramSubService['ServiceID'] = $NewServiceID[$j];
                            unset($paramSubService['SubServiceID']);

                            $result = $DBSalon->table('SubService')
                            ->insert($paramSubService);

                            array_push($NewSubServiceID, $DBSalon->select('select lastval() AS id')[0]->id);
                            array_push($OldSubServiceID, $SubService[$j]->SubServiceID);

                        }
                    }
                }
                elseif($Service == true)
                {
                    $Service = $DBSalon->table('Service')
                    ->where('BranchID',$BranchIDFrom)
                    ->whereRaw('("Archived" is null)')
                    ->get();

                    for($i = 0;$i<count($Service);$i++)
                    {
                        $paramService = $this->cloneParameter($Service[$i]);
                        $paramService['BranchID'] = $BranchIDTo;
                        $paramService['BrandID'] = $BrandIDTo;
                        unset($paramService['ServiceID']);

                        $result = $DBSalon->table('Service')
                        ->insert($paramService);
                    }
                }

            }


        }
        //ini untuk log copy master nya.
        // return "A";
        $Username = DB::table('User')
        ->where('UserID',$this->param->UserID)
        ->first()->Username;
        // for($i=0;$i<count($))
        $link = $request->url();
        $log = $DBLog->table('CopyMasterLog')
        ->insert(array('UserID' => $this->param->UserID, 'Username' => $Username,'Activity' => $link,
        'Parameter' => $input, 'Time' => $now));
        $endresult = array(
            'Status' => 0,
            'Errors' => array(),
            'Message' => "Success"
        );
        return Response()->json($endresult);
    }


    public function getSystemPermission(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [

        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');

        $ProductID = $input['ProductID'];


        $Parent = $DBFnB->table('SystemPermission AS SP')
        ->where(function($query) use ($ProductID){
                $query->orwhere('ProductID',$ProductID);
                $query->orwhere('ProductID','');
                $query->orwhere('ProductID',null);
            })
        ->where(function($query){
                $query->orwhereraw('("ParentID" is null or "ParentID" like \'\')');
            })
        ->whereraw('Floor("Order") = "Order"')
        ->where(function($query){
                $query->orwhereraw('"URI" is null or "URI" like \'\'');
            })
        ->orderby('Order','ASC')
        ->get()->toArray();

        for($i=0;$i<count($Parent);$i++){
            $Parent[$i]->Permission = array();

            $permission = $DBFnB->table('SystemPermission AS SP')
            ->where(function($query) use ($ProductID){
                    $query->orwhere('ProductID',$ProductID);
                    $query->orwhere('ProductID','');
                    $query->orwhere('ProductID',null);
                })
            ->where('ParentID', $Parent[$i]->SystemPermissionID)
            ->orderby('Order','ASC')
            ->get()->toArray();

            $Parent[$i]->Permission = $permission;
        }

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Permission' => $Parent
      );

      return Response()->json($endresult);

    }

    public function getBranchByAccountID(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
            'AccountID' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');
        $ProductID = @$input['ProductID'];
        $AccountID = $input['AccountID'];
        $BranchID = @$input['BranchID'];
        // $FnBBrand = $DBFnB->table('AccountOutlet AS AO')
        // ->where('AccountID',$AccountID)
        // ->wherenotnull('AO.BranchID')
        // ->get()->toArray();

        // $FnBBrand = $DBFnB->select('select "AccountID", "ProductID","BranchID","AccountOutletID", "IsEnterprise", "Persists","RowNumber"
        //                         FROM (SELECT*,ROW_NUMBER () OVER (PARTITION BY "BranchID", "ProductID", "AccountID"
        //                         ORDER BY "AccountOutletID") "RowNumber" FROM "AccountOutlet"
        //                         WHERE "AccountID" = '.$AccountID.' and "BranchID" is not null) DATA
        //                         WHERE "RowNumber" = 1');
        //temporary solution for now.
        if($ProductID === null)
        {
            $FnBBrand = $DBFnB->table('AccountOutlet AS AO')
            ->wherenotnull('BranchID')
            ->where('BranchID','>',0)
            ->where('AccountID',$AccountID)
            ->wherenotin('ProductID',['HQF','HQR','HQS'])
            ->select(DB::raw('*,ROW_NUMBER () OVER (PARTITION BY "BranchID", "ProductID", "AccountID" ORDER BY "AccountOutletID") "RowNumber"'))
            ->get()->toarray();
        }
        else{
            $FnBBrand = $DBFnB->table('AccountOutlet AS AO')
            ->wherenotnull('BranchID')
            ->where('BranchID','>',0)
            ->where('AccountID',$AccountID)
            ->where('ProductID',$ProductID)
            ->where('BranchID','!=',$BranchID)
            ->wherenotin('ProductID',['HQF','HQR','HQS'])
            ->select(DB::raw('*,ROW_NUMBER () OVER (PARTITION BY "BranchID", "ProductID", "AccountID" ORDER BY "AccountOutletID") "RowNumber"'))
            ->get()->toarray();
        }


        for($i=0;$i<count($FnBBrand);$i++)
        {
            if($FnBBrand[$i]->RowNumber == 2)
            {
                array_splice($FnBBrand,$i,1);
                $i = $i-1;
            }
            else{
                if($FnBBrand[$i]->ProductID == "RES")
                {
                    $BranchDetail = $DBFnB->table('Branch AS BR')
                    ->leftjoin('RestaurantMaster AS RM','BR.RestaurantID','=','RM.RestaurantID')
                    ->where('BranchID',$FnBBrand[$i]->BranchID)
                    ->first();
                }
                elseif($FnBBrand[$i]->ProductID == "HQF")
                {
                    $BranchDetail = $DBFnB->table('RestaurantMaster AS BR')
                    ->where('RestaurantID',$FnBBrand[$i]->BranchID)
                    ->first();
                }
                elseif($FnBBrand[$i]->ProductID == "RET")
                {
                    $BranchDetail = $DBRetail->table('Branch AS BR')
                    ->leftjoin('StoreMaster AS SM','BR.StoreID','=','SM.StoreID')
                    ->where('BranchID',$FnBBrand[$i]->BranchID)
                    ->first();
                }
                elseif($FnBBrand[$i]->ProductID == "HQR")
                {
                    $BranchDetail = $DBRetail->table('StoreMaster AS BR')
                    ->where('StoreID',$FnBBrand[$i]->BranchID)
                    ->first();
                }
                else{
                    $BranchDetail = $DBSalon->table('Branch AS BR')
                    ->leftjoin('BrandMaster AS BM','BR.BrandID','=','BM.BrandID')
                    ->where('BranchID',$FnBBrand[$i]->BranchID)
                    ->first();
                }
                if(!isset($BranchDetail))
                {$FnBBrand[$i]->BranchDetail = "Branch nya gaada di master!";}
                else{
                    $FnBBrand[$i]->BranchDetail = $BranchDetail;
                }
            }


        }


      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Branch' => $FnBBrand
      );

      return Response()->json($endresult);

    }

    public function getBranchDetail(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
            'BranchID' => 'required',
            'ProductID' => 'required',
            'AccountID' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');
        $Product = @$input['ProductID'];
        $BranchID = $input['BranchID'];
        $AccountID = $input['AccountID'];
        $FnBBrand = $DBFnB->table('AccountOutlet AS AO')
        ->where('AccountID',$AccountID)
        ->where('BranchID',$BranchID)
        ->where('ProductID',$Product)
        ->wherenotnull('AO.BranchID')
        ->get()->toArray();

        for($i=0;$i<count($FnBBrand);$i++)
        {
            if($FnBBrand[$i]->ProductID == "RES")
            {
                $BranchDetail = $DBFnB->table('Branch AS BR')
                ->leftjoin('RestaurantMaster AS RM','BR.RestaurantID','=','RM.RestaurantID')
                ->where('BranchID',$FnBBrand[$i]->BranchID)
                ->first();
            }
            elseif($FnBBrand[$i]->ProductID == "HQF")
            {
                $BranchDetail = $DBFnB->table('RestaurantMaster AS BR')
                ->where('RestaurantID',$FnBBrand[$i]->BranchID)
                ->first();
            }
            elseif($FnBBrand[$i]->ProductID == "RET")
            {
                $BranchDetail = $DBRetail->table('Branch AS BR')
                ->leftjoin('StoreMaster AS SM','BR.StoreID','=','SM.StoreID')
                ->where('BranchID',$FnBBrand[$i]->BranchID)
                ->first();
            }
            elseif($FnBBrand[$i]->ProductID == "HQR")
            {
                $BranchDetail = $DBRetail->table('StoreMaster AS BR')
                ->where('StoreID',$FnBBrand[$i]->BranchID)
                ->first();
            }
            else{
                $BranchDetail = $DBSalon->table('Branch AS BR')
                ->leftjoin('BrandMaster AS BM','BR.BrandID','=','BM.BrandID')
                ->where('BranchID',$FnBBrand[$i]->BranchID)
                ->first();
            }
            if(!isset($BranchDetail))
            {$FnBBrand[$i]->BranchDetail = "Branch nya gaada di master!";}
            else{
                $FnBBrand[$i]->BranchDetail = $BranchDetail;
            }

        }

        $permission = $DBFnB->table('AccountOutletPermission AS AOP')
        ->leftjoin('AccountOutlet AS AO','AOP.AccountOutletID','=','AO.AccountOutletID')
        ->where('AccountID',$AccountID)
        ->where('BranchID',$BranchID)
        ->where('ProductID',$Product)
        ->select(['SystemPermissionID','AccountID','ProductID','BranchID','IsEnterprise','Persists'])
        ->distinct()
        ->get();


      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Branch' => $FnBBrand,
          'Permission' => $permission
      );

      return Response()->json($endresult);

    }

    public function getLogTransaction(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
            'StartDate' => 'required',
            'EndDate' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');
        $dtime = strtotime($input['StartDate']);
        $StartDate = date('Y-m-d 00:00:00',$dtime);
        $dtime = strtotime($input['EndDate']);
        $EndDate = date('Y-m-d 23:59:59',$dtime);
        $BillNumber = @$input['BillNumber'];
        $BranchID = @$input['BranchID'];
        $Page = @$input['Page'];
        $NumRow = @$input['RowCount'];
        $searchMethod = '/index.php/restaurant-v7/action/input_order';
        if(@$input['RestaurantID'] != null)
        {
            $ProductID = "RES";
        }
        elseif(@$input['StoreID']!= null)
        {
            $ProductID = "RET";
        }
        else{
            $ProductID = "SER";
        }
        if($NumRow == null)
        {$NumRow = 100;
        $Page = ($Page-1)*100;}
        if($BillNumber == null)
        {
            $LogTransaction = $DBTLog->table('DeviceRequestAudit')
            ->where('Date','>=',$StartDate)
            ->where('Date','<=',$EndDate)
            ->where('Product',$ProductID)
            ->where('TokenDetail.BranchID',$BranchID)
            ->where('Method','like','/index.php/restaurant-v7/action/input_order')
            ->limit($NumRow)
            ->skip($Page)
            ->orderby('Date','Desc')
            ->get()->toArray();
        }
        else{
            $LogTransaction = $DBTLog->table('DeviceRequestAudit')
            ->where('Date','>=',$StartDate)
            ->where('Date','<=',$EndDate)
            ->where('Product',$ProductID)
            ->where('TokenDetail.BranchID',$BranchID)
            ->where('Method','like','/index.php/restaurant-v7/action/input_order')
            ->where('RequestData.Order.Billing.OrderBillNumber',$BillNumber)
            ->orderby('Date','Desc')
            ->get()->toArray();
        }

        //input order buat $FnB
        //input sales buat salon sama retail


      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'LogTransaction' => $LogTransaction
      );

      return Response()->json($endresult);

    }
}
