<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Jenssegers\Mongodb\MongodbServiceProvider;
use DB;
use Moloquent;
use Validator;
use Mail;
use App\Mail\OTPRequest;
use App\Mail\OTPApprove;
use App\Mail\OTPReject;

class OtpController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
        $link = $request->url();
        $Username = DB::table('User')
        ->where('UserID',$this->param->UserID)
        ->first()->Username;
        $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
        $log = DB::table('LogActivity')
        ->insert(array('UserID' => $this->param->UserID, 'Activity' => $link,
        'Parameter' => $request->getContent(), 'Time' => $now,'Username' => $Username));
    }

    public function getUser(Request $request){
        $input = json_decode($this->request->getContent(),true);

        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');

        $User = DB::table('User AS U')
        ->leftjoin('UserType UT','U.UserTypeID','=','UT.UserTypeID')
        ->where('UserTypeName',"Support")
        ->get();

        $endresult = array(
            'Status' => 0,
            'Errors' => array(),
            'Message' => "Success",
            'User' => $User
        );
        return Response()->json($endresult);
}

public function getAccount(Request $request){
    $input = json_decode($this->request->getContent(),true);

    $DBRetail = DB::connection('pgsqlretaildev');
    $DBFnB = DB::connection('pgsqlfnbdev');
    $DBSalon = DB::connection('pgsqlsalondev');
    $DBLog = DB::connection('mongoadmindev');

    $Account = $DBFnB->table('Account AS A')
    ->get();

    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'Account' => $Account
    );
    return Response()->json($endresult);
}

public function getAccountDetail(Request $request){
    $input = json_decode($this->request->getContent(),true);
    $rules = [
        'AccountID' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }
    $DBRetail = DB::connection('pgsqlretaildev');
    $DBFnB = DB::connection('pgsqlfnbdev');
    $DBSalon = DB::connection('pgsqlsalondev');
    $DBLog = DB::connection('mongoadmindev');

    $Account = $DBFnB->table('Account AS A')
    ->where('AccountID',$ID)
    ->get();

    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'Account' => $Account
    );
    return Response()->json($endresult);
}

public function insertUpdateOutlet(Request $request){
    $input = json_decode($this->request->getContent(),true);
    $rules = [
        'AccountID' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }
    $DBRetail = DB::connection('pgsqlretaildev');
    $DBFnB = DB::connection('pgsqlfnbdev');
    $DBSalon = DB::connection('pgsqlsalondev');
    $DBLog = DB::connection('mongoadmindev');

    $ID = $input['BranchID'];
    $param = array(
        'BranchID' => @$input['BranchID'],
        'Address' => @$input['Address'],
        'Contact' => @$input['Contact'],
        'Email' => @$input['Email'],
        'MaxDevice' => 100000,
        'MaxLocalDevice' => @$input['MaxLocalDevice'],
        'DueAmount' => @$input['DueAmount']
    );
    if($ID == null)
    {
        $result = $DBFnB->table('Branch')
        ->insert($param);
    }

    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'Account' => $Account
    );
    return Response()->json($endresult);
}

public function insertUpdateAccount(Request $request){
    $input = json_decode($this->request->getContent(),true);
    $rules = [
        'Fullname' => 'required',
        'Email' => 'required|email',
        'Password' => 'min:8|regex:/^.*(?=.{3,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$/'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }
    $DBRetail = DB::connection('pgsqlretaildev');
    $DBFnB = DB::connection('pgsqlfnbdev');
    $DBSalon = DB::connection('pgsqlsalondev');
    $DBLog = DB::connection('mongoadmindev');
    $ID = @$input['AccountID'];
    $param = array(
        'Username' => $input['Username'],
        'Fullname' => $input['Fullname'],
        'Email' => $input['Email'],
        'Password' => password_hash(@$input['Password'], PASSWORD_BCRYPT),
        'Phone' => $input['Phone']
    );
    $unique = array(
        'Table' => "Account",
        'ID' => $ID,
        'Column' => "Username",
        'String' => $input['Username']
    );
    $uniqueUsername = $this->uniqueFnBDB($unique);
    $unique['Column'] = "Email";
    $unique['String'] = $input['Email'];
    $uniqueUsername = $this->uniqueFnBDB($unique);
    if($ID == null)
    {
        $result = $DBFnB->table('Account AS A')
        ->insert($param);
    }
    else{
        $result = $DBFnB->table('Account AS A')
        ->where('AccountID',$ID)
        ->update($param);
    }

    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'Result' => $result
    );
    return Response()->json($endresult);
}

public function getTrialBranch(Request $request){
    $input = json_decode($this->request->getContent(),true);
    $rules = [
      'Type' => 'required',
      'Status' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }

    $DBRetail = DB::connection('pgsqlretail');
    $DBFnB = DB::connection('pgsqlfnb');
    $DBSalon = DB::connection('pgsqlsalon');
    $Type = $input['Type'];
    $Status = $input['Status'];
    if($Type == "FNB")
    {
        if($Status == 1)
        $Branch = $DBFnB->table('Branch')
        ->leftjoin('RestaurantMaster','Branch.RestaurantID','=','RestaurantMaster.RestaurantID')
        ->where('DevStatus', null)
        ->get();
        else{
            $Branch = $DBFnB->table('Branch')
            ->leftjoin('RestaurantMaster','Branch.RestaurantID','=','RestaurantMaster.RestaurantID')
            ->where('DevStatus', "Trial")
            ->get();
        }
    }
    elseif($Type == "RET")
    {
        if($Status == 1)
        $Branch = $DBRetail->table('Branch')
        ->leftjoin('StoreMaster','Branch.StoreID','=','StoreMaster.StoreID')
        ->where('DevStatus', null)
        ->get();
        else{
            $Branch = $DBRetail->table('Branch')
            ->leftjoin('StoreMaster','Branch.StoreID','=','StoreMaster.StoreID')
            ->where('DevStatus', "Trial")
            ->get();
        }
    }
    else{
        if($Status == 1)
        $Branch = $DBSalon->table('Branch')
        ->leftjoin('BrandMaster','Branch.BrandID','=','BrandMaster.BrandID')
        ->where('DevStatus', null)
        ->get();
        else{
            $Branch = $DBSalon->table('Branch')
            ->leftjoin('BrandMaster','Branch.BrandID','=','BrandMaster.BrandID')
            ->where('DevStatus', "Trial")
            ->get();
        }
    }

    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'Branch' => $Branch
    );
    return Response()->json($endresult);
}

public function getTrialBranchDetail(request $request){
    $input = json_decode($request->getContent(),true);
    $rules = [
        'BranchID' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }
    $ID = $input['BranchID'];
    $DBRetail = DB::connection('pgsqlretail');
    $DBFnB = DB::connection('pgsqlfnb');
    $DBSalon = DB::connection('pgsqlsalon');
    if(@$input['RestaurantID'] != null)
    {

        $Branch = $DBFnB->table('Branch')
        ->leftjoin('RestaurantMaster','Branch.RestaurantID','=','RestaurantMaster.RestaurantID')
        ->where('BranchID',$ID)
        ->get();

    }
    elseif(@$input['StoreID'] != null)
    {
        $Branch = $DBRetail->table('Branch')
        ->leftjoin('StoreMaster','Branch.BranchID','=','StoreMaster.StoreID')
        ->where('BranchID',$ID)
        ->get();
    }
    else{
        $Branch = $DBSalon->table('Branch')
        ->leftjoin('BrandMaster','Branch.BranchID','=','BrandMaster.BrandID')
        ->where('BranchID',$ID)
        ->get();
    }

  $endresult = array(
      'Status' => 0,
      'Errors' => array(),
      'Message' => "Success",
      'BranchDetail' => $Branch
  );

   return Response()->json($endresult);
}

public function getBanUnbanLog(Request $request){
    $input = json_decode($this->request->getContent(),true);
    $rules = [
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }

    $DBFnB = DB::connection('pgsqlfnb');
    $DBRetail = DB::connection('pgsqlretail');
    $DBSalon = DB::connection('pgsqlsalon');
    $DBLog = DB::connection('mongoadmin');
    
    // $timestamp = $dtime->getTimestamp();

    $result = $DBLog->table('BanUnbanLog')
    ->leftjoin('User','BanUnbanLog.UserID','=','User.UserID')
    ->select(['Parameter','UserID','BanUnbanLog.Username','Username','Activity','Reason','Time']);
    

    if(@$input['StartDate'] != null)
    {
        $dtime = strtotime($input['StartDate']);
        $StartDate = date('Y-m-d 00:00:01',$dtime);
        $dtime = strtotime($input['EndDate']);
        $EndDate = date('Y-m-d 23:59:59',$dtime);
        $result->where('Time','>=',$StartDate)
        ->where('Time','<=',$EndDate);
    }
    if(@$input['String']!= null)
    {
        $String = $input['String'];
        $Branch1 = $DBFnB->table('Branch')
        ->leftjoin('RestaurantMaster','RestaurantMaster.RestaurantID','=','Branch.RestaurantID')
        ->where(function($query) use ($String){
            for($i=0;$i<count($String);$i++)
            {
                $query->orwhereraw('lower("RestaurantName") like lower(\'%'.$String[$i].'%\')');
                $query->orwhereraw('lower("BranchName") like lower(\'%'.$String[$i].'%\')');
            }
            })
        ->get()->toArray();
        $Branch2 = $DBRetail->table('Branch')
        ->leftjoin('StoreMaster','StoreMaster.StoreID','=','Branch.StoreID')
        ->where(function($query) use ($String){
            for($i=0;$i<count($String);$i++)
            {
                $query->orwhereraw('lower("StoreName") like lower(\'%'.$String[$i].'%\')');
                $query->orwhereraw('lower("BranchName") like lower(\'%'.$String[$i].'%\')');
            }
            })
        ->get()->toArray();
        $Branch3 = $DBSalon->table('Branch')
        ->leftjoin('BrandMaster','BrandMaster.BrandID','=','Branch.BrandID')
        ->where(function($query) use ($String){
            for($i=0;$i<count($String);$i++)
            {
                $query->orwhereraw('lower("BrandName") like lower(\'%'.$String[$i].'%\')');
                $query->orwhereraw('lower("BranchName") like lower(\'%'.$String[$i].'%\')');
            }
            })
        ->get()->toArray();
        
        $Branch = array_merge($Branch1,$Branch2,$Branch3);
        
        $BranchID = [];
        for($i=0;$i<count($Branch);$i++)
        {
            $BranchID[$i] = $Branch[$i]->BranchID;
        }
        
        $result->wherein('Parameter.BranchID',$BranchID);
    }
    // print_r(@$BranchID);
    $banunban = $result->orderby('BanUnbanLogID','desc')
    ->get()->toArray();

    
    for($i = 0;$i<count($banunban);$i++){
        $parameter = $banunban[$i]['Parameter'];
        if(@$parameter['StoreID'] != null)
        {
            $banunban[$i]['StoreName'] = new \stdClass();
            $banunban[$i]['BranchName'] = new \stdClass();
            $banunban[$i]['StoreID'] = new \stdClass();
            $banunban[$i]['BranchID'] = new \stdClass();
            $StoreName = $DBRetail->table('StoreMaster')
            ->where('StoreID',$parameter['StoreID'])
            ->first();
            $BranchName = $DBRetail->table('Branch')
            ->where('BranchID',$parameter['BranchID'])
            ->first();
            $banunban[$i]['StoreName'] = $StoreName->StoreName;
            $banunban[$i]['BranchName'] = $BranchName->BranchName;
            $banunban[$i]['StoreID'] = $parameter['StoreID'];
            $banunban[$i]['BranchID'] = $parameter['BranchID'];
        }
        elseif(@$parameter['RestaurantID'] != null)
        {
            $banunban[$i]['RestaurantName'] = new \stdClass();
            $banunban[$i]['BranchName'] = new \stdClass();
            $banunban[$i]['RestaurantID'] = new \stdClass();
            $banunban[$i]['BranchID'] = new \stdClass();
            $RestaurantName = $DBFnB->table('RestaurantMaster')
            ->where('RestaurantID',$parameter['RestaurantID'])
            ->first();
            $BranchName = $DBFnB->table('Branch')
            ->where('BranchID',$parameter['BranchID'])
            ->first();
            $banunban[$i]['RestaurantName'] = @$RestaurantName->RestaurantName;
            $banunban[$i]['BranchName'] = $BranchName->BranchName;
            $banunban[$i]['RestaurantID'] = $parameter['RestaurantID'];
            $banunban[$i]['BranchID'] = $parameter['BranchID'];
        }
        else{
            $banunban[$i]['BrandName'] = new \stdClass();
            $banunban[$i]['BranchName'] = new \stdClass();
            $banunban[$i]['BrandName'] = new \stdClass();
            $banunban[$i]['BranchName'] = new \stdClass();
            $BrandName = $DBSalon->table('BrandMaster')
            ->where('BrandID',$parameter['BrandID'])
            ->first();
            $BranchName = $DBSalon->table('Branch')
            ->where('BranchID',$parameter['BranchID'])
            ->first();
            $banunban[$i]['BrandName'] = $BrandName->BrandName;
            $banunban[$i]['BranchName'] = $BranchName->BranchName;
            $banunban[$i]['RestaurantID'] = $parameter['RestaurantID'];
            $banunban[$i]['BranchID'] = $parameter['BranchID'];
        }
    }


    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'BanUnbanLog' => $banunban
    );
    return Response()->json($endresult);
}

public function getBanUnbanHistory(Request $request){
    $input = json_decode($this->request->getContent(),true);
    $rules = [
      'StartDate' => 'required|date',
      'EndDate' => 'required|date'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }

    $DBFnB = DB::connection('pgsqlfnb');
    $DBRetail = DB::connection('pgsqlretail');
    $DBSalon = DB::connection('pgsqlsalon');
    $DBLog = DB::connection('mongoadmin');
    $dtime = strtotime($input['StartDate']);
    $StartDate = date('Y-m-d 00:00:00',$dtime);
    $dtime = strtotime($input['EndDate']);
    $EndDate = date('Y-m-d 23:59:59',$dtime);

    // $BranchID = $input['BranchID'];
    // if(@$input['RestaurantID'] != null)
    // {$column = 'Parameter.RestaurantID';
    // $mainID = $input['RestaurantID'];}
    
    // elseif(@$input['StoreID'] != null)
    // {
    //     $column = 'Parameter.StoreID'; 
    //     $mainID = $input['StoreID'];
    // }
    // else
    // {
    //     $column = 'Parameter.BrandID';
    //     $mainID = $input['BrandID'];
    // }
    

    $banunban = $DBLog->table('BanUnbanLog')
    // ->where('Parameter.BranchID',$BranchID)
    // ->where($column,$mainID)
    ->where('Time','>=',$StartDate)
    ->where('Time','<=',$EndDate)
    ->select(['Parameter','BanUnbanLog.UserID','BanUnbanLog.Username','UserFullName','Activity','Reason','Time'])
    ->orderby('BanUnbanLogID','desc')
    ->get()->toArray();


    for($i = 0;$i<count($banunban);$i++){
        $parameter = $banunban[$i]['Parameter'];
        if(@$parameter['StoreID'] != null)
        {
            $banunban[$i]['StoreName'] = new \stdClass();
            $banunban[$i]['BranchName'] = new \stdClass();
            $banunban[$i]['StoreID'] = new \stdClass();
            $banunban[$i]['BranchID'] = new \stdClass();
            $StoreName = $DBRetail->table('StoreMaster')
            ->where('StoreID',$parameter['StoreID'])
            ->first();
            $BranchName = $DBRetail->table('Branch')
            ->where('BranchID',$parameter['BranchID'])
            ->first();
            $banunban[$i]['StoreName'] = @$StoreName->StoreName;
            $banunban[$i]['BranchName'] = @$BranchName->BranchName;
            $banunban[$i]['StoreID'] = $parameter['StoreID'];
            $banunban[$i]['BranchID'] = $parameter['BranchID'];
        }
        elseif(@$parameter['RestaurantID'] != null)
        {
            $banunban[$i]['RestaurantName'] = new \stdClass();
            $banunban[$i]['BranchName'] = new \stdClass();
            $banunban[$i]['RestaurantID'] = new \stdClass();
            $banunban[$i]['BranchID'] = new \stdClass();
            $RestaurantName = $DBFnB->table('RestaurantMaster')
            ->where('RestaurantID',$parameter['RestaurantID'])
            ->first();
            $BranchName = $DBFnB->table('Branch')
            ->where('BranchID',$parameter['BranchID'])
            ->first();
            $banunban[$i]['RestaurantName'] = @$RestaurantName->RestaurantName;
            $banunban[$i]['BranchName'] = @$BranchName->BranchName;
            $banunban[$i]['RestaurantID'] = $parameter['RestaurantID'];
            $banunban[$i]['BranchID'] = $parameter['BranchID'];
        }
        else{
            $banunban[$i]['BrandName'] = new \stdClass();
            $banunban[$i]['BranchName'] = new \stdClass();
            $banunban[$i]['BrandName'] = new \stdClass();
            $banunban[$i]['BranchName'] = new \stdClass();
            $BrandName = $DBSalon->table('BrandMaster')
            ->where('BrandID',$parameter['BrandID'])
            ->first();
            $BranchName = $DBSalon->table('Branch')
            ->where('BranchID',$parameter['BranchID'])
            ->first();
            $banunban[$i]['BrandName'] = @$BrandName->BrandName;
            $banunban[$i]['BranchName'] = @$BranchName->BranchName;
            $banunban[$i]['BrandID'] = $parameter['BrandID'];
            $banunban[$i]['BranchID'] = $parameter['BranchID'];
        }
    }


    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'BanUnbanLog' => $banunban
    );
    return Response()->json($endresult);
}

public function getActiveBranch(Request $request){
    $input = json_decode($this->request->getContent(),true);

    $String = @$input['String'];
    $DBRetail = DB::connection('pgsqlretail');
    $DBFnB = DB::connection('pgsqlfnb');
    $DBSalon = DB::connection('pgsqlsalon');
    $Status = @$input['Status'];
    if($Status == 0){
        if(count($String)==0)
        {
            $endresult = array(
                'Status' => 0,
                'Errors' => array(),
                'Message' => "Success",
                'Branch' => array()
            );
            return Response()->json($endresult);
        }
        elseif($String == "*")
        {
            $FnBBrand = $DBFnB->table('Branch AS BR')
            ->leftjoin('RestaurantMaster AS RM','BR.RestaurantID','=','RM.RestaurantID')
            ->select(['BR.RestaurantID','RestaurantName','Owner','BranchID','BranchName','Active'])
            ->where('Active',0)
            ->where('BranchID','>',0)
            ->get()->toArray();
            $RetailBrand = $DBRetail->table('Branch AS BR')
            ->leftjoin('StoreMaster AS SM','BR.StoreID','=','SM.StoreID')
            ->select(['BR.StoreID','StoreName','Owner','BranchID','BranchName','Active'])
            ->where('Active',0)
            ->where('BranchID','>',0)
            ->get()->toArray();
            $SalonBrand = $DBSalon->table('Branch AS BR')
            ->leftjoin('BrandMaster AS BM','BR.BrandID','=','BM.BrandID')
            ->select(['BR.BrandID','BrandName','Owner','BranchID','BranchName','Active'])
            ->where('Active',0)
            ->where('BranchID','>',0)
            ->get()->toArray();
        }
        else{
            for($i = 0;$i<count($String);$i++)
            {
                $String[$i] = strtolower($String[$i]);
            }
            $FnBBrand = $DBFnB->table('Branch AS BR')
            ->leftjoin('RestaurantMaster AS RM','BR.RestaurantID','=','RM.RestaurantID')
            ->select(['BR.RestaurantID','RestaurantName','Owner','BranchID','BranchName','Active'])
            ->where('Active',0)
            ->where('BranchID','>',0)
            ->where(function($query) use ($String){
                for($i=0;$i<count($String);$i++)
                {
                    $query->orwhereraw('lower("RestaurantName") like \'%'.$String[$i].'%\'');
                    $query->orwhereraw('lower("BranchName") like \'%'.$String[$i].'%\'');
                }
                })
            ->get()->toArray();

            $RetailBrand = $DBRetail->table('Branch AS BR')
            ->leftjoin('StoreMaster AS SM','BR.StoreID','=','SM.StoreID')
            ->select(['BR.StoreID','StoreName','Owner','BranchID','BranchName','Active'])
            ->where('Active',0)
            ->where('BranchID','>',0)
            ->where(function($query) use ($String){
                for($i=0;$i<count($String);$i++)
                {
                    $query->orwhereraw('lower("StoreName") like \'%'.$String[$i].'%\'');
                    $query->orwhereraw('lower("BranchName") like \'%'.$String[$i].'%\'');
                }
            })
            ->get()->toArray();

            $SalonBrand = $DBSalon->table('Branch AS BR')
            ->leftjoin('BrandMaster AS BM','BR.BrandID','=','BM.BrandID')
            ->select(['BR.BrandID','BrandName','Owner','BranchID','BranchName','Active'])
            ->where('Active',0)
            ->where('BranchID','>',0)
            ->where(function($query) use ($String){
                for($i=0;$i<count($String);$i++)
                {
                    $query->orwhereraw('lower("BrandName") like \'%'.$String[$i].'%\'');
                    $query->orwhereraw('lower("BranchName") like \'%'.$String[$i].'%\'');
                }
            })
            ->get()->toArray();
        }
    }
    else{
        if(count($String)==0)
        {
            $endresult = array(
                'Status' => 0,
                'Errors' => array(),
                'Message' => "Success",
                'Branch' => 0
            );
            return Response()->json($endresult);
        }
        elseif($String == "*")
        {
            $FnBBrand = $DBFnB->table('Branch AS BR')
            ->leftjoin('RestaurantMaster AS RM','BR.RestaurantID','=','RM.RestaurantID')
            ->select(['BR.RestaurantID','RestaurantName','Owner','BranchID','BranchName','Active'])
            ->where('Active',1)
            ->where('BranchID','>',0)
            ->get()->toArray();

            $RetailBrand = $DBRetail->table('Branch AS BR')
            ->leftjoin('StoreMaster AS SM','BR.StoreID','=','SM.StoreID')
            ->select(['BR.StoreID','StoreName','Owner','BranchID','BranchName','Active'])
            ->where('Active',1)
            ->where('BranchID','>',0)
            ->get()->toArray();

            $SalonBrand = $DBSalon->table('Branch AS BR')
            ->leftjoin('BrandMaster AS BM','BR.BrandID','=','BM.BrandID')
            ->select(['BR.BrandID','BrandName','Owner','BranchID','BranchName','Active'])
            ->where('Active',1)
            ->where('BranchID','>',0)
            ->get()->toArray();
        }
        else{
            for($i = 0;$i<count($String);$i++)
            {
                $String[$i] = strtolower($String[$i]);
            }
            $FnBBrand = $DBFnB->table('Branch AS BR')
            ->leftjoin('RestaurantMaster AS RM','BR.RestaurantID','=','RM.RestaurantID')
            ->select(['BR.RestaurantID','RestaurantName','Owner','BranchID','BranchName','Active'])
            ->where('Active',1)
            ->where('BranchID','>',0)
            ->where(function($query) use ($String){
                for($i=0;$i<count($String);$i++)
                {
                    $query->orwhereraw('lower("RestaurantName") like \'%'.$String[$i].'%\'');
                    $query->orwhereraw('lower("BranchName") like \'%'.$String[$i].'%\'');
                }
            })
            ->get()->toArray();



            $RetailBrand = $DBRetail->table('Branch AS BR')
            ->leftjoin('StoreMaster AS SM','BR.StoreID','=','SM.StoreID')
            ->select(['BR.StoreID','StoreName','Owner','BranchID','BranchName','Active'])
            ->where('Active',1)
            ->where('BranchID','>',0)
            ->where(function($query) use ($String){
                for($i=0;$i<count($String);$i++)
                {
                    $query->orwhereraw('lower("StoreName") like \'%'.$String[$i].'%\'');
                    $query->orwhereraw('lower("BranchName") like \'%'.$String[$i].'%\'');
                }
            })
            ->get()->toArray();


            $SalonBrand = $DBSalon->table('Branch AS BR')
            ->leftjoin('BrandMaster AS BM','BR.BrandID','=','BM.BrandID')
            ->select(['BR.BrandID','BrandName','Owner','BranchID','BranchName','Active'])
            ->where('Active',1)
            ->where('BranchID','>',0)
            ->where(function($query) use ($String){
                for($i=0;$i<count($String);$i++)
                {
                    $query->orwhereraw('lower("BrandName") like \'%'.$String[$i].'%\'');
                    $query->orwhereraw('lower("BranchName") like \'%'.$String[$i].'%\'');
                }
            })
            ->get()->toArray();
        }
    }



        $Branch = array_merge($FnBBrand,$RetailBrand,$SalonBrand);

    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'Branch' => $Branch
    );
    return Response()->json($endresult);



}

public function banUnbanBranch(Request $request){
    $input = json_decode($this->request->getContent(),true);
    $rules = [
      'BranchID' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }
    $DBRetail = DB::connection('pgsqlretail');
    $DBFnB = DB::connection('pgsqlfnb');
    $DBSalon = DB::connection('pgsqlsalon');
    $DBLog = DB::connection('mongoadmin');
    $BranchID = $input['BranchID'];
    $input2 = $request->getContent();
    $Reason = @$input['Reason'];
    $Username = DB::table('User')
    ->where('UserID',$this->param->UserID)
    ->first()->Username;
    $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
    if(@$input['RestaurantID'] != null){
        $check = $DBFnB->table('Token')
        ->where('BranchID',$BranchID)
        ->first();

        if($check === null)
        {
            $branch = $DBFnB->table('Branch')
            ->where('BranchID',$BranchID)
            ->first();
            // return response()->json($branch);
            if($branch->Active == 1)
            {
                $result = $DBFnB->table('Branch')
                ->where('BranchID',$BranchID)
                ->update(array('Active' => 0));
                $log = $DBLog->table('BanUnbanLog')
                ->insert(array('UserID' => $this->param->UserID, 'Activity' => "BAN",
                'Parameter' => $input, 'Time' => $now,'Username' => $Username, 'Reason' => $Reason));
            }
            else{
                $result = $DBFnB->table('Branch')
                ->where('BranchID',$BranchID)
                ->update(array('Active' => 1));
                $log = $DBLog->table('BanUnbanLog')
                ->insert(array('UserID' => $this->param->UserID, 'Activity' => "UNBAN",
                'Parameter' => $input, 'Time' => $now,'Username' => $Username, 'Reason' => $Reason));
            }
        }
        else{
            $branch = $DBFnB->table('Branch')
            ->where('BranchID',$BranchID)
            ->first();
            // return response()->json($branch);
            if($branch->Active == 1)
            {
                $result = $DBFnB->table('Token')
                ->where('BranchID',$BranchID)
                ->where('Disabled',null)
                ->update(array('Disabled' => "Y", 'DisablingMethod' => "BAN", 'LogoutDate' => $now, 'DisabledReason' => $Reason));
                $result = $DBFnB->table('Branch')
                ->where('BranchID',$BranchID)
                ->update(array('Active' => 0));
                $log = $DBLog->table('BanUnbanLog')
                ->insert(array('UserID' => $this->param->UserID, 'Activity' => "BAN",
                'Parameter' => $input, 'Time' => $now,'Username' => $Username, 'Reason' => $Reason));
            }
            else{
                $result = $DBFnB->table('Token')
                ->where('BranchID',$BranchID)
                ->where('DisablingMethod',"BAN")
                ->update(array('Disabled' => null, 'DisablingMethod' => null, 'LogoutDate' => null, 'DisabledReason' => null));
                $result = $DBFnB->table('Branch')
                ->where('BranchID',$BranchID)
                ->update(array('Active' => 1));
                $log = $DBLog->table('BanUnbanLog')
                ->insert(array('UserID' => $this->param->UserID, 'Activity' => "UNBAN",
                'Parameter' => $input, 'Time' => $now,'Username' => $Username, 'Reason' => $Reason));

            }
        }

    }

    if(@$input['StoreID'] != null){
        $check = $DBRetail->table('Token')
        ->where('BranchID',$BranchID)
        ->first();
        if($check === null)
        {
            $branch = $DBRetail->table('Branch')
            ->where('BranchID',$BranchID)
            ->first();
            if($branch->Active == 1)
            {
                $result = $DBRetail->table('Branch')
                ->where('BranchID',$BranchID)
                ->update(array('Active' => 0));
                $log = $DBLog->table('BanUnbanLog')
                ->insert(array('UserID' => $this->param->UserID, 'Activity' => "BAN",
                'Parameter' => $input, 'Time' => $now,'Username' => $Username, 'Reason' => $Reason));
            }
            else{
                $result = $DBRetail->table('Branch')
                ->where('BranchID',$BranchID)
                ->update(array('Active' => 1));
                $log = $DBLog->table('BanUnbanLog')
                ->insert(array('UserID' => $this->param->UserID, 'Activity' => "UNBAN",
                'Parameter' => $input, 'Time' => $now,'Username' => $Username, 'Reason' => $Reason));
            }
        }
        else{
            $branch = $DBRetail->table('Branch')
            ->where('BranchID',$BranchID)
            ->first();
            // return response()->json($branch);
            if($branch->Active == 1)
            {
                $result = $DBRetail->table('Token')
                ->where('BranchID',$BranchID)
                ->where('Disabled',null)
                ->update(array('Disabled' => "Y", 'DisablingMethod' => "BAN", 'LogoutDate' => $now, 'DisabledReason' => $Reason));
                $result = $DBRetail->table('Branch')
                ->where('BranchID',$BranchID)
                ->update(array('Active' => 0));
                $log = $DBLog->table('BanUnbanLog')
                ->insert(array('UserID' => $this->param->UserID, 'Activity' => "BAN",
                'Parameter' => $input, 'Time' => $now,'Username' => $Username, 'Reason' => $Reason));
            }
            else{
                $result = $DBRetail->table('Token')
                ->where('BranchID',$BranchID)
                ->where('DisablingMethod',"BAN")
                ->update(array('Disabled' => null, 'DisablingMethod' => null, 'LogoutDate' => null, 'DisabledReason' => null));
                $result = $DBRetail->table('Branch')
                ->where('BranchID',$BranchID)
                ->update(array('Active' => 1));
                $log = $DBLog->table('BanUnbanLog')
                ->insert(array('UserID' => $this->param->UserID, 'Activity' => "UNBAN",
                'Parameter' => $input, 'Time' => $now,'Username' => $Username, 'Reason' => $Reason));
            }
        }


    }

    if(@$input['BrandID'] != null){
        $check = $DBSalon->table('Token')
        ->where('BranchID',$BranchID)
        ->first();
        if($check === null)
        {
            $branch = $DBSalon->table('Branch')
            ->where('BranchID',$BranchID)
            ->first();
            if($branch->Active == 1)
            {
                $result = $DBSalon->table('Branch')
                ->where('BranchID',$BranchID)
                ->update(array('Active' => 0));
                $log = $DBLog->table('BanUnbanLog')
                ->insert(array('UserID' => $this->param->UserID, 'Activity' => "BAN",
                'Parameter' => $input, 'Time' => $now,'Username' => $Username, 'Reason' => $Reason));
            }
            else{
                $result = $DBSalon->table('Branch')
                ->where('BranchID',$BranchID)
                ->update(array('Active' => 1));
                $log = $DBLog->table('BanUnbanLog')
                ->insert(array('UserID' => $this->param->UserID, 'Activity' => "UNBAN",
                'Parameter' => $input, 'Time' => $now,'Username' => $Username, 'Reason' => $Reason));
            }
        }
        else{
            $branch = $DBSalon->table('Branch')
            ->where('BranchID',$BranchID)
            ->first();
            // return response()->json($branch);
            if($branch->Active == 1)
            {
                $result = $DBSalon->table('Token')
                ->where('BranchID',$BranchID)
                ->update(array('DisablingMethod' => "BAN", 'LogoutDate' => $now,'DisabledReason' => $Reason));
                $result = $DBSalon->table('Branch')
                ->where('BranchID',$BranchID)
                ->update(array('Active' => 0));
                $log = $DBLog->table('BanUnbanLog')
                ->insert(array('UserID' => $this->param->UserID, 'Activity' => "BAN",
                'Parameter' => $input, 'Time' => $now,'Username' => $Username, 'Reason' => $Reason));
            }
            else{
                $result = $DBSalon->table('Token')
                ->where('BranchID',$BranchID)
                ->where('DisablingMethod',"BAN")
                ->update(array('DisablingMethod' => null, 'LogoutDate' => null, 'DisabledReason' => null));
                $result = $DBSalon->table('Branch')
                ->where('BranchID',$BranchID)
                ->update(array('Active' => 1));
                $log = $DBLog->table('BanUnbanLog')
                ->insert(array('UserID' => $this->param->UserID, 'Activity' => "UNBAN",
                'Parameter' => $input, 'Time' => $now,'Username' => $Username, 'Reason' => $Reason));
            }
        }

    }

    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'Result' => $result
    );
    return Response()->json($endresult);
}

public function getOTPRequest(Request $request){
    $input = json_decode($this->request->getContent(),true);
    $rules = [
      'StartDate' => 'required',
      'EndDate' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }
    $DBRetail = DB::connection('pgsqlretail');
    $DBFnB = DB::connection('pgsqlfnb');
    $StartDate = $input['StartDate'];
    $EndDate = $input['EndDate'];
    $OTPRequest = $DBFnB->table('OTPRequest AS OR')
    ->leftjoin('OTPStatus AS OT','OR.OTPStatusID','=','OT.OTPStatusID')
    ->where('Date','>=',$StartDate)
    ->where('Date','<=',$EndDate)
    ->get();

    for($i=0;$i<count($OTPRequest);$i++)
    {
        $OTPRequest[$i]->RequesterName = new \stdClass();
        $Name = DB::table('User')
        ->where('UserID',$OTPRequest[$i]->RequesterID)
        ->first();
        $OTPRequest[$i]->RequesterName = $Name->UserFullName;
    }

    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'OTPRequest' => $OTPRequest
    );
    return Response()->json($endresult);
}

public function getOTPRequestDetail(Request $request){
    $input = json_decode($this->request->getContent(),true);
    $rules = [
      'OTPRequestID' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }
    $DBRetail = DB::connection('pgsqlretail');
    $DBFnB = DB::connection('pgsqlfnb');
    $DBSalon = DB::connection('pgsqlsalon');
    $ID = $input['OTPRequestID'];

    $OTPRequest = $DBFnB->table('OTPRequest AS OR')
    ->leftjoin('OTPStatus AS OT','OR.OTPStatusID','=','OT.OTPStatusID')
    ->where('OR.OTPRequestID',$ID)
    ->get();

    for($i=0;$i<count($OTPRequest);$i++)
    {
        $OTPRequest[$i]->RequesterName = new \stdClass();
        $Name = DB::table('User')
        ->where('UserID',$OTPRequest[$i]->RequesterID)
        ->first();
        $OTPRequest[$i]->RequesterName = $Name->UserFullName;

        $OTPRequest[$i]->Branch = array();
        $Branch = $DBFnB->table('OTP')
        ->where('OTPRequestID',$ID)
        ->distinct('BranchID')
        ->select(['BranchID','RestaurantID','StoreID','BrandID'])
        ->get();
        $OTPRequest[$i]->Branch = $Branch;
        $count = count($OTPRequest[$i]->Branch);

        for($j=0;$j<$count;$j++)
        {
            $otpname = $OTPRequest[$i]->Branch[$j];

            $otpname->BranchName = new \stdClass();
            if($otpname->RestaurantID != null)
            {
                    $name = $DBFnB->table('Branch')
                    ->where('BranchID',$otpname->BranchID)
                    ->first();
                    $otpname->BranchName = $name->BranchName;
                    $name = $DBFnB->table('RestaurantMaster')
                    ->where('RestaurantID',$otpname->RestaurantID)
                    ->first();
                    $otpname->RestaurantName = new \stdClass();
                    $otpname->RestaurantName = $name->RestaurantName;
            }
            elseif($otpname->StoreID != null){
                $name = $DBRetail->table('Branch')
                ->where('BranchID',$otpname->BranchID)
                ->first();
                $otpname->BranchName = $name->BranchName;
                $name = $DBRetail->table('StoreMaster')
                ->where('StoreID',$otpname->StoreID)
                ->first();
                $otpname->StoreName = new \stdClass();
                $otpname->StoreName = $name->StoreName;
            }
            else{
                $name = $DBSalon->table('Branch')
                ->where('BranchID',$otpname->BranchID)
                ->first();
                $otpname->BranchName = $name->BranchName;
                $name = $DBSalon->table('BrandMaster')
                ->where('BrandID',$otpname->BrandID)
                ->first();
                $otpname->BrandName = new \stdClass();
                $otpname->BrandName = $name->BrandName;
            }

        }
    }

    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'OTPRequest' => $OTPRequest
    );
    return Response()->json($endresult);
}

    public function insert(Request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
          'Description' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;

        if(@$input['Token'] === null)
        {
            $result = "No Token Valid Found";
        }
        else{
            $DBRetail = DB::connection('pgsqlretail');
            $DBFnB = DB::connection('pgsqlfnb');
            $DBSalon = DB::connection('pgsqlsalon');
            $RestaurantID = @$input['RestaurantID'];
            $StoreID = @$input['StoreID'];
            $UserID = DB::table('Authenticator')
            ->where('AuthenticatorToken',$input['Token'])
            ->first();
            $ID = @$input['OTPRequestID'];
            $param = array(
                'RequesterID' => $UserID->UserID,
                'RequestQty' => $input['RequestQty'],
                'Description' => @$input['Description'],
                'OTPStatusID' => "W",
                'Date' => $now
            );
            if($ID == null)
            {
                $result = $DBFnB->table('OTPRequest')
                ->insert($param);
                $ID = $DBFnB->select('select lastval() AS id')[0]->id;
            }
            else{
                $param['RequestStatusID'] = @$input['RequestStatusID'];
                $result = $DBFnB->table('OTPRequest')
                ->where('OTPRequestID',$ID)
                ->update($param);
            }

            $result = $DBFnB->table('OTP')
            ->where('OTPRequestID',$ID)
            ->delete();
            $Branch = $input['Branch'];
            //detail otp request
            for($i=0;$i<count($Branch);$i++)
            {
                for($j=0;$j<$input['RequestQty'];$j++)
                {
                    if(@$Branch[$i]['RestaurantID'] != null)
                    {
                        $RestaurantID = $Branch[$i]['RestaurantID'];
                        $BranchID = $Branch[$i]['BranchID'];
                        $RestaurantName = $DBFnB->table('RestaurantMaster')
                        ->where('RestaurantID',$RestaurantID)
                        ->first();
                        $RestaurantName = $RestaurantName->RestaurantName;
                        $BranchName = $DBFnB->table('Branch')
                        ->where('BranchID',$BranchID)
                        ->first();
                        $BranchName = $BranchName->BranchName;
                        $Branch[$i]['RestaurantName'] = $RestaurantName;
                        $Branch[$i]['BranchName'] = $BranchName;
                        $result = $DBFnB->table('OTP')
                        ->insert(array('BranchID' => $BranchID,'RestaurantID' => $RestaurantID,'OTPRequestID' => $ID,'MainID'=>$RestaurantID,'ProductID'=>"RES",
                                       'Requester'=>$UserID->UserID));
                    }
                    elseif(@$Branch[$i]['StoreID'] != null){
                        $StoreID = $Branch[$i]['StoreID'];
                        $BranchID = $Branch[$i]['BranchID'];
                        $StoreName = $DBRetail->table('StoreMaster')
                        ->where('StoreID',$StoreID)
                        ->first();
                        $StoreName = $StoreName->StoreName;
                        $BranchName = $DBRetail->table('Branch')
                        ->where('BranchID',$BranchID)
                        ->first();
                        $BranchName = $BranchName->BranchName;
                        $Branch[$i]['StoreName'] = $StoreName;
                        $Branch[$i]['BranchName'] = $BranchName;
                        $result = $DBFnB->table('OTP')
                        ->insert(array('BranchID' => $BranchID,'StoreID' => $StoreID,'OTPRequestID' => $ID,'MainID'=>$StoreID,'ProductID'=>"RET",
                                       'Requester'=>$UserID->UserID));
                    }
                    elseif(@$Branch[$i]['BrandID'] != null){
                        $BrandID = $Branch[$i]['BrandID'];
                        $BranchID = $Branch[$i]['BranchID'];
                        $BrandName = $DBSalon->table('BrandMaster')
                        ->where('BrandID',$BrandID)
                        ->first();
                        $BrandName = $BrandName->BrandName;
                        $BranchName = $DBSalon->table('Branch')
                        ->where('BranchID',$BranchID)
                        ->first();
                        $BranchName = $BranchName->BranchName;
                        $Branch[$i]['BrandName'] = $BrandName;
                        $Branch[$i]['BranchName'] = $BranchName;
                        $result = $DBFnB->table('OTP')
                        ->insert(array('BranchID' => $BranchID,'BrandID' => $BrandID,'OTPRequestID' => $ID,'MainID'=>$BrandID,'ProductID'=>"SER",
                                       'Requester'=>$UserID->UserID));
                    }
                }

            }
        }
        $objDemo = new \stdClass();

        $objDemo->sender = 'HelloBill';
        $objDemo->emailreceiver = 'kevin@hellobill.co.id';
        $objDemo->receiver = 'Kevin';
        $RequesterName = DB::table('User')
        ->where('UserID',$UserID->UserID)
        ->first();
        $objDemo->requestername = $RequesterName->UserFullName;
        $objDemo->RequestQty = $input['RequestQty'];
        $objDemo->Description = $input['Description'];
        $objDemo->OTPRequestID = $ID;
        $objDemo->detailrequest = array();
        $objDemo->detailrequest = $Branch;
        // return $i;
        $objDemo->countrestaurant = 2;
        $token = DB::table('Authenticator')
        ->where('UserID',32)
        ->first();
        $objDemo->tokenkokevin = $token->AuthenticatorToken;
        Mail::to($objDemo->emailreceiver)->cc('rickigozal97@gmail.com')
        ->send(new OTPRequest($objDemo));
        $endresult = array(
            'Status' => 0,
            'Errors' => array(),
            'Message' => "Success",
            'Result' => $result
        );
        return Response()->json($endresult);
    }

    public function approveOTP(Request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
          'OTPRequestID' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $ID = $input['OTPRequestID'];
        $check = $DBFnB->table('OTPRequest')
        ->where('OTPRequestID',$ID)
        ->first();

        if($check->OTPStatusID == "A" || $check->OTPStatusID == "R")
        {
            $endresult = array(
                'Status' => 0,
                'Errors' => array(),
                'Message' => "Success",
                'OTP' => 'OTP Request has been approved or rejected.'
            );
            return Response()->json($endresult);
        }
        $approve = $DBFnB->table('OTPRequest')
        ->where('OTPRequestID',$ID)
        ->update(array('OTPStatusID' => "A"));

        $OTP = $DBFnB->table('OTP')
        ->where('OTPRequestID', $ID)
        ->get();
        $count = count($OTP);
        $AccountOTP = array();
        for($i=0;$i<$count;$i++)
        {   $AccountOTP[$i] = new \stdClass;
            if($OTP[$i]->RestaurantID != null)
            {
                $accountID = $DBFnB->table('Account')
                ->where('RestaurantID',$OTP[$i]->RestaurantID)
                ->where('IsMaster',1)
                ->orderby('AccountID','asc')
                ->first();

                $name  = $DBFnB->table('Branch')
                ->where('BranchID',$OTP[$i]->BranchID)
                ->first();
                $name = $name->BranchName;
                $name = substr($name,0,3);

            }
            else{
                $accountID = $DBFnB->table('Account')
                ->where('StoreID',$OTP[$i]->StoreID)
                ->where('IsMaster',1)
                ->orderby('AccountID','asc')
                ->first();

                $name  = $DBRetail->table('Branch')
                ->where('BranchID',$OTP[$i]->BranchID)
                ->first();
                $name = $name->BranchName;
                $name = substr($name,0,3);
            }
            $t = bin2hex(openssl_random_pseudo_bytes(12));
		    $t2 = bin2hex(openssl_random_pseudo_bytes(36));
		    $t3 = bin2hex(openssl_random_pseudo_bytes(24));
		    $token =  $t.'.'.time().'.'.$t2.'_'.md5(time()).$t3;
            $username = "OTP".$name.$OTP[$i]->OTPID;
            $password = $this->genPassword(3);
            $passwordnew = password_hash(@$password, PASSWORD_BCRYPT);
            $now = date('Y-m-d H:i:s');
		    $expiry = (date('Y-m-d H:i:s', strtotime('12 Hour', strtotime($now))));
            $arr = array(
			"Impersonate"=>$accountID->AccountID,
			"Username"=>$username,
			"Password"=>$passwordnew,
			"CreatedDate"=>$now,
			"ExpiryDate"=>$expiry,
            "GeneratedToken"=>$token
		      );
              $result = $DBFnB->table('OTP')
              ->where('OTPID',$OTP[$i]->OTPID)
              ->update($arr);
              if($OTP[$i]->RestaurantID != null)
              {
                  $BranchName = $DBFnB->table('Branch')
                  ->where('BranchID',$OTP[$i]->BranchID)
                  ->first();
                  $Product = "Restaurant";
              }
              else{
                  $BranchName = $DBRetail->table('Branch')
                  ->where('BranchID',$OTP[$i]->BranchID)
                  ->first();
                  $Product = "Retail";
              }


              $AccountOTP[$i]->Product = $Product;
              $AccountOTP[$i]->BranchName = $BranchName->BranchName;
              $AccountOTP[$i]->Username = $username;
              $AccountOTP[$i]->Password = $password;
        }

        $objDemo = new \stdClass();
        $requesteremail = $DBFnB->table('OTPRequest')
        ->where('OTPRequestID',$ID)
        ->select(['RequesterID'])
        ->first();

        $email = DB::table('User')
        ->where('UserID',$requesteremail->RequesterID)
        ->first();
        $Request = $DBFnB->table('OTPRequest')
        ->where('OTPRequestID',$ID)
        ->first();
        $objDemo->sender = 'HelloBill';
        $objDemo->emailreceiver = $email->Email;
        $objDemo->receiver = $email->UserFullName;
        $objDemo->RequestQty = $Request->RequestQty;
        $objDemo->Description = $Request->Description;
        $objDemo->OTPRequestID = $ID;
        $objDemo->detailrequest = array();
        $objDemo->detailrequest = $AccountOTP;
        // return $i;
        Mail::to($objDemo->emailreceiver)->cc(array('rickigozal97@gmail.com','kevin@hellobill.co.id'))
        ->send(new OTPApprove($objDemo));

        $endresult = array(
            'Status' => 0,
            'Errors' => array(),
            'Message' => "Success",
            'OTP' => "Approved"
        );
        return Response()->json($endresult);



}


public function rejectOTP(Request $request){
    $input = json_decode($this->request->getContent(),true);
    $rules = [
      'OTPRequestID' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }
    $DBRetail = DB::connection('pgsqlretail');
    $DBFnB = DB::connection('pgsqlfnb');
    $ID = $input['OTPRequestID'];
    $check = $DBFnB->table('OTPRequest')
    ->where('OTPRequestID',$ID)
    ->first();
    if($check->OTPStatusID = "A" || $check->OTPStatusID = "R")
    {
        $endresult = array(
            'Status' => 0,
            'Errors' => array(),
            'Message' => "Success",
            'OTP' => 'OTP Request has been approved/rejected.'
        );
        return Response()->json($endresult);
    }
    $approve = $DBFnB->table('OTPRequest')
    ->where('OTPRequestID',$ID)
    ->update(array('OTPStatusID' => "R"));


    $objDemo = new \stdClass();
    $requesteremail = $DBFnB->table('OTPRequest')
    ->where('OTPRequestID',$ID)
    ->select(['RequesterID'])
    ->first();
    $email = DB::table('User')
    ->where('UserID',$requesteremail->RequesterID)
    ->first();
    $Request = $DBFnB->table('OTPRequest')
    ->where('OTPRequestID',$ID)
    ->first();
    $objDemo->sender = 'HelloBill';
    $objDemo->emailreceiver = $email->Email;
    $objDemo->receiver = $email->UserFullName;
    $objDemo->RequestQty = $Request->RequestQty;
    $objDemo->Description = $Request->Description;
    $objDemo->OTPRequestID = $ID;
    // return $i;
    Mail::to($objDemo->emailreceiver)->cc('kevin@hellobill.co.id')
    ->send(new OTPReject($objDemo));

    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'Result' => $approve
    );
    return Response()->json($endresult);



}


public function getBranch(Request $request){
    $input = json_decode($this->request->getContent(),true);

    $String = @$input['String'];
    $DBRetail = DB::connection('pgsqlretail');
    $DBFnB = DB::connection('pgsqlfnb');
    $DBSalon = DB::connection('pgsqlsalon');

    if(count($String)==0)
    {
        $endresult = array(
            'Status' => 0,
            'Errors' => array(),
            'Message' => "Success",
            'Branch' => 0
        );
        return Response()->json($endresult);
    }
    elseif($String == "*"){
        $FnBBrand = $DBFnB->table('Branch AS BR')
        ->leftjoin('RestaurantMaster AS RM','BR.RestaurantID','=','RM.RestaurantID')
        ->select(['BR.RestaurantID','RestaurantName','Owner','BranchID','BranchName'])
        ->where('BranchID','>',0)
        ->get()->toArray();

        $RetailBrand = $DBRetail->table('Branch AS BR')
        ->leftjoin('StoreMaster AS SM','BR.StoreID','=','SM.StoreID')
        ->select(['BR.StoreID','StoreName','Owner','BranchID','BranchName'])
        ->where('BranchID','>',0)
        ->get()->toArray();

        $SalonBrand = $DBSalon->table('Branch AS BR')
        ->leftjoin('BrandMaster AS BM','BR.BrandID','=','BM.BrandID')
        ->select(['BR.BrandID','BrandName','Owner','BranchID','BranchName'])
        ->where('BranchID','>',0)
        ->get()->toArray();
    }
    else{
        $FnBBrand = $DBFnB->table('Branch AS BR')
        ->leftjoin('RestaurantMaster AS RM','BR.RestaurantID','=','RM.RestaurantID')
        ->select(['BR.RestaurantID','RestaurantName','Owner','BranchID','BranchName'])
        ->where('BranchID','>',0)
        ->where(function($query) use ($String){
            for($i=0;$i<count($String);$i++)
            {
                $query->orwhereraw('lower("RestaurantName") like \'%'.strtolower($String[$i]).'%\'');
            }
        })
        ->orwhere(function($query1) use ($String){
            for($i=0;$i<count($String);$i++)
            {
                $query1->orwhereraw('lower("BranchName") like \'%'.strtolower($String[$i]).'%\'');
            }
        })
        ->get()->toArray();

        $RetailBrand = $DBRetail->table('Branch AS BR')
        ->leftjoin('StoreMaster AS SM','BR.StoreID','=','SM.StoreID')
        ->select(['BR.StoreID','StoreName','Owner','BranchID','BranchName'])
        ->where('BranchID','>',0)
        ->where(function($query) use ($String){
            for($i=0;$i<count($String);$i++)
            {
                $query->orwhereraw('lower("StoreName") like \'%'.strtolower($String[$i]).'%\'');
            }
        })
        ->orwhere(function($query1) use ($String){
            for($i=0;$i<count($String);$i++)
            {
                $query1->orwhereraw('lower("BranchName") like \'%'.strtolower($String[$i]).'%\'');
            }
        })
        ->get()->toArray();
        //
        $SalonBrand = $DBSalon->table('Branch AS BR')
        ->leftjoin('BrandMaster AS BM','BR.BrandID','=','BM.BrandID')
        ->select(['BR.BrandID','BrandName','Owner','BranchID','BranchName'])
        ->where('BranchID','>',0)
        ->where(function($query) use ($String){
            for($i=0;$i<count($String);$i++)
            {
                $query->orwhereraw('lower("BrandName") like \'%'.strtolower($String[$i]).'%\'');
            }
        })
        ->orwhere(function($query1) use ($String){
            for($i=0;$i<count($String);$i++)
            {
                $query1->orwhereraw('lower("BranchName") like \'%'.strtolower($String[$i]).'%\'');
            }
        })
        ->get()->toArray();

    }
    $Branch = array_merge($FnBBrand,$RetailBrand,$SalonBrand);

    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'Branch' => $Branch
    );
    return Response()->json($endresult);



}

}
