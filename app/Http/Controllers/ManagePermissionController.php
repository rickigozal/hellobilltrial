<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;
use PDF;

class ManagePermissionController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
        $link = $request->url();
        $Username = DB::table('User')
        ->where('UserID',$this->param->UserID)
        ->first()->Username;
        $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
        $log = DB::table('LogActivity')
        ->insert(array('UserID' => $this->param->UserID, 'Activity' => $link,
        'Parameter' => $request->getContent(), 'Time' => $now,'Username' => $Username));
    }
    public function getBranchTab(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [

        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');
        $Tab = array();
        $prID = ["Restaurant", "Retail", "Services"];
        $prName = ["RES", "RET", "SER"];
        $count = count($prID);
        for($i=0;$i<$count;$i++)
        {
            $Tab[] = array(
                'ProductName' => $prID[$i],
                'ProductID' => $prName[$i]
            );
        }

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Tab' => $Tab
      );

      return Response()->json($endresult);

    }

    public function getProductID(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [

        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');
        $Account = $DBFnB->table('AccountOutlet')
        ->distinct('ProductID')
        ->select(['ProductID'])
        ->get();

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Data' => $Account
      );

      return Response()->json($endresult);

    }

    public function getAccount(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [

        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');

        $Account = $DBFnB->table('Account')
        ->where('DeletedDate',null)
        ->get();

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Account' => $Account
      );

      return Response()->json($endresult);

    }

    public function insertUpdateAccountPermission(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
            'Permission' => 'required',
            'ProductID' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');
        $Permission = $input['Permission'];
        $ProductID = $input['ProductID'];
        $AccountID = $input['AccountID'];
        $Permission = $input['Permission'];
        

        for($i=0;$i<count($Permission);$i++)
        {
            $Data = $Permission[$i];
            $AccountOutlet = $DBFnB->table('AccountOutlet')
            ->where('ProductID',$ProductID)
            ->where('BranchID',$Data['BranchID'])
            ->where('AccountID',$AccountID)
            ->first();

            //delete semua permission account ini ke branch tertentu.
            if(isset($AccountOutlet))
            {
                $DBFnB->table('AccountOutletPermission')
                ->where('AccountOutletID', $AccountOutlet->AccountOutletID)
                ->delete();

                $AccountOutlet = $DBFnB->table('AccountOutlet')
                ->where('ProductID',$ProductID)
                ->where('BranchID',$Data['BranchID'])
                ->where('AccountID',$AccountID)
                ->delete();
            }


            if($Data['PermissionID'] == null)
            {
                $result = true;
                $DBFnB->table('Account')
                ->where('AccountID',$AccountID)
                ->where('HBType',null)
                ->update(array('HBType' => "1"));
            }
            else{
                $DBFnB->table('AccountOutlet')
                ->insert(array('BranchID' => $Data['BranchID'], 'ProductID' => $ProductID, 'AccountID' => $AccountID, 'Persists' => "1"));
                $AccountOutletID = $DBFnB->select('select lastval() AS id')[0]->id;

                $DBFnB->table('Account')
                ->where('AccountID',$AccountID)
                ->where('HBType',null)
                ->update(array('HBType' => "1"));
                for($j=0;$j<count($Data['PermissionID']);$j++)
                {
                    $param = array(
                        'AccountOutletID' => $AccountOutletID,
                        'SystemPermissionID' => $Data['PermissionID'][$j]
                    );
                    $result = $DBFnB->table('AccountOutletPermission')
                    ->insert($param);
                }
            }
        }


      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Row' => $result
      );

      return Response()->json($endresult);

    }

    public function getSystemPermission(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [

        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');

        $ProductID = $input['ProductID'];


        $Parent = $DBFnB->table('SystemPermission AS SP')
        ->where(function($query) use ($ProductID){
                $query->orwhere('ProductID',$ProductID);
                $query->orwhere('ProductID','');
                $query->orwhere('ProductID',null);
            })
        ->where(function($query){
                $query->orwhereraw('("ParentID" is null or "ParentID" = \'\')');
            })
        ->whereraw('Floor("Order") = "Order"')
        ->orderby('Order','ASC')
        ->get()->toArray();

        for($i=0;$i<count($Parent);$i++){
            $Parent[$i]->Permission = array();

            $permission = $DBFnB->table('SystemPermission AS SP')
            ->where(function($query) use ($ProductID){
                    $query->orwhere('ProductID',$ProductID);
                    $query->orwhere('ProductID','');
                    $query->orwhere('ProductID',null);
                })
            ->where('ParentID', $Parent[$i]->SystemPermissionID)
            ->orderby('Order','ASC')
            ->get()->toArray();

            $Parent[$i]->Permission = $permission;
        }

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Permission' => $Parent
      );

      return Response()->json($endresult);

    }

    public function getBranchByAccountID(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
            'AccountID' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');
        $ProductID = @$input['ProductID'];
        $AccountID = $input['AccountID'];
        // $FnBBrand = $DBFnB->table('AccountOutlet AS AO')
        // ->where('AccountID',$AccountID)
        // ->wherenotnull('AO.BranchID')
        // ->get()->toArray();

        // $FnBBrand = $DBFnB->select('select "AccountID", "ProductID","BranchID","AccountOutletID", "IsEnterprise", "Persists","RowNumber"
        //                         FROM (SELECT*,ROW_NUMBER () OVER (PARTITION BY "BranchID", "ProductID", "AccountID"
        //                         ORDER BY "AccountOutletID") "RowNumber" FROM "AccountOutlet"
        //                         WHERE "AccountID" = '.$AccountID.' and "BranchID" is not null) DATA
        //                         WHERE "RowNumber" = 1');
        //temporary solution for now.
        $FnBBrand = $DBFnB->table('AccountOutlet AS AO')
        ->wherenotnull('BranchID')
        ->where('BranchID','>',0)
        ->where('AccountID',$AccountID)
        ->where('ProductID',$ProductID)
        ->select(DB::raw('*,ROW_NUMBER () OVER (PARTITION BY "BranchID", "ProductID", "AccountID" ORDER BY "AccountOutletID") "RowNumber"'))
        ->get()->toarray();

        for($i=0;$i<count($FnBBrand);$i++)
        {
            if($FnBBrand[$i]->RowNumber == 2)
            {
                array_splice($FnBBrand,$i,1);
                $i = $i-1;
            }
            else{
                if($FnBBrand[$i]->ProductID == "RES")
                {
                    $BranchDetail = $DBFnB->table('Branch AS BR')
                    ->leftjoin('RestaurantMaster AS RM','BR.RestaurantID','=','RM.RestaurantID')
                    ->where('BranchID',$FnBBrand[$i]->BranchID)
                    ->first();
                }
                elseif($FnBBrand[$i]->ProductID == "HQF")
                {
                    $BranchDetail = $DBFnB->table('RestaurantMaster AS BR')
                    ->where('RestaurantID',$FnBBrand[$i]->BranchID)
                    ->first();
                }
                elseif($FnBBrand[$i]->ProductID == "RET")
                {
                    $BranchDetail = $DBRetail->table('Branch AS BR')
                    ->leftjoin('StoreMaster AS SM','BR.StoreID','=','SM.StoreID')
                    ->where('BranchID',$FnBBrand[$i]->BranchID)
                    ->first();
                }
                elseif($FnBBrand[$i]->ProductID == "HQR")
                {
                    $BranchDetail = $DBRetail->table('StoreMaster AS BR')
                    ->where('StoreID',$FnBBrand[$i]->BranchID)
                    ->first();
                }
                else{
                    $BranchDetail = $DBSalon->table('Branch AS BR')
                    ->leftjoin('BrandMaster AS BM','BR.BrandID','=','BM.BrandID')
                    ->where('BranchID',$FnBBrand[$i]->BranchID)
                    ->first();
                }
                if(!isset($BranchDetail))
                {$FnBBrand[$i]->BranchDetail = "Branch nya gaada di master!";}
                else{
                    $FnBBrand[$i]->BranchDetail = $BranchDetail;
                }
            }


        }


      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Branch' => $FnBBrand
      );

      return Response()->json($endresult);

    }

    public function getBranchAll(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
            'String' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');
        $ProductID = @$input['ProductID'];
        $AccountID = @$input['AccountID'];
        $String = @$input['String'];
       
        if(count($String)==0)
        {
            $endresult = array(
                'Status' => 0,
                'Errors' => array(),
                'Message' => "Success",
                'Branch' => 0);
        return Response()->json($endresult);
        }
        elseif($String == "*")
        {
            $FnBBrand = $DBFnB->table('Branch AS BR')
            ->leftjoin('RestaurantMaster AS RM','BR.RestaurantID','=','RM.RestaurantID')
            ->wherenotnull('BranchID')
            ->where('BranchID','>',0)
            ->get()->toarray();
        for($i=0;$i<count($FnBBrand);$i++)
        {
            $FnBBrand[$i]->ProductID = "RES";
        }

        $RETBrand = $DBRetail->table('Branch AS BR')
        -leftjoin('StoreMaster AS SM','BR.StoreID','=','SM.StoreID')
        ->wherenotnull('BranchID')
        ->where('BranchID','>',0)
        ->get()->toarray();

        for($i=0;$i<count($RETBrand);$i++)
        {
            $RETBrand[$i]->ProductID = "RET";
        }


        $SERBrand = $DBSalon->table('Branch AS BR')
        ->leftjoin('BrandMaster AS BM','BR.BrandID','=','BM.BrandID')
        ->wherenotnull('BranchID')
        ->where('BranchID','>',0)
        ->get()->toarray();

        for($i=0;$i<count($SERBrand);$i++)
        {
            $SERBrand[$i]->ProductID = "SER";
        }
        }
        else{
            $FnBBrand = $DBFnB->table('Branch AS BR')
            ->leftjoin('RestaurantMaster AS RM','BR.RestaurantID','=','RM.RestaurantID')
            ->wherenotnull('BranchID')
            ->where('BranchID','>',0)
            ->distinct('BR.RestaurantID','RestaurantName')
            ->select(['BR.RestaurantID','RestaurantName'])
            ->where(function($query1) use ($String){
                for($i=0;$i<count($String);$i++)
                {
                    $query1->orwhereraw('lower("BranchName") like \'%'.strtolower($String[$i]).'%\'');
                }
            })
            ->get()->toarray();
        for($i=0;$i<count($FnBBrand);$i++)
        {
            $FnBBrand[$i]->ProductID = "RES";
            $Branch = $DBFnB->table('Branch')
            ->where('RestaurantID',$FnBBrand[$i]->RestaurantID)
            ->wherenotnull('BranchID')
            ->where('BranchID','>',0)
            ->where(function($query1) use ($String){
                for($i=0;$i<count($String);$i++)
                {
                    $query1->orwhereraw('lower("BranchName") like \'%'.strtolower($String[$i]).'%\'');
                }
            })
            ->get()->toarray();
            $FnBBrand[$i]->Branch = $Branch;
            $FnBBrand[$i]->ProductName = "FnB";
        }
            

        $RETBrand = $DBRetail->table('Branch AS BR')
        ->leftjoin('StoreMaster AS SM','BR.StoreID','=','SM.StoreID')
        ->wherenotnull('BranchID')
        ->where('BranchID','>',0)
        ->distinct('BR.StoreID','StoreName')
            ->select(['BR.StoreID','StoreName'])
            ->where(function($query1) use ($String){
                for($i=0;$i<count($String);$i++)
                {
                    $query1->orwhereraw('lower("BranchName") like \'%'.strtolower($String[$i]).'%\'');
                }
            })
        ->get()->toarray();

        for($i=0;$i<count($RETBrand);$i++)
        {
            $RETBrand[$i]->ProductID = "RET";
            $Branch = $DBRetail->table('Branch')
            ->where('StoreID',$RETBrand[$i]->StoreID)
            ->wherenotnull('BranchID')
            ->where('BranchID','>',0)
            ->where(function($query1) use ($String){
                for($i=0;$i<count($String);$i++)
                {
                    $query1->orwhereraw('lower("BranchName") like \'%'.strtolower($String[$i]).'%\'');
                }
            })
            ->get()->toarray();
            $RETBrand[$i]->Branch = $Branch;
            $RETBrand[$i]->ProductName = "Retail";
        }

        
        $SERBrand = $DBSalon->table('Branch AS BR')
        ->leftjoin('BrandMaster AS BM','BR.BrandID','=','BM.BrandID')
        ->wherenotnull('BranchID')
        ->where('BranchID','>',0)
        ->distinct('BR.BrandID','BrandName')
            ->select(['BR.BrandID','BrandName'])
            ->where(function($query1) use ($String){
                for($i=0;$i<count($String);$i++)
                {
                    $query1->orwhereraw('lower("BranchName") like \'%'.strtolower($String[$i]).'%\'');
                }
            })
        ->get()->toarray();

        for($i=0;$i<count($SERBrand);$i++)
        {
            $SERBrand[$i]->ProductID = "SER";
            $Branch = $DBSalon->table('Branch')
            ->where('BrandID',$SERBrand[$i]->BrandID)
            ->wherenotnull('BranchID')
            ->where('BranchID','>',0)
            ->where(function($query1) use ($String){
                for($i=0;$i<count($String);$i++)
                {
                    $query1->orwhereraw('lower("BranchName") like \'%'.strtolower($String[$i]).'%\'');
                }
            })
            ->get()->toarray();
            $SERBrand[$i]->Branch = $Branch;
            $SERBrand[$i]->ProductName = "Beauty and Service";
        }
        }

        
        $Branch = array_merge($FnBBrand,$RETBrand,$SERBrand);
        if($ProductID == "RES")
        {$Branch = $FnBBrand;}
        elseif($ProductID == "RET")
        {$Branch = $RETBrand;}
        elseif($ProductID == "SER")
        {$Branch = $SERBrand;}

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Branch' => $Branch
      );

      return Response()->json($endresult);

    }

    public function getBranchDetail(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
            'BranchID' => 'required',
            'ProductID' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');
        $Product = @$input['ProductID'];
        $BranchID = $input['BranchID'];
        $AccountID = @$input['AccountID'];
        // $FnBBrand = $DBFnB->table('AccountOutlet AS AO')
        // ->where('AccountID',$AccountID)
        // ->where('BranchID',$BranchID)
        // ->where('ProductID',$Product)
        // ->wherenotnull('AO.BranchID')
        // ->get()->toArray();
        $Brand = [];
        for($i=0;$i<count($BranchID);$i++)
        {
            $testPermission = $DBFnB->table('AccountOutlet AS AO')
            ->where('BranchID',$BranchID[$i])
            ->where('ProductID',$Product)
            ->where('AccountID',$AccountID)
            ->get()->toarray();
            if(count($testPermission) > 0)
            {
                $FnBBrand = $DBFnB->table('AccountOutlet AS AO')
                ->where('BranchID',$BranchID[$i])
                ->where('ProductID',$Product)
                ->where('AccountID',$AccountID)
                ->select(DB::raw('*,ROW_NUMBER () OVER (PARTITION BY "BranchID", "ProductID", "AccountID" ORDER BY "AccountOutletID") "RowNumber"'))
                ->get()->toarray();
                if(count($FnBBrand) > 0)
                {
                    for($j=0;$j<count($FnBBrand);$j++)
                    {
                        if($FnBBrand[$j]->RowNumber != 1)
                        {
                            array_splice($FnBBrand,$j,1);
                            $j = $j-1;
                        }
                        else{
                            if($FnBBrand[$j]->ProductID == "RES")
                            {
                                $BranchDetail = $DBFnB->table('Branch AS BR')
                                ->leftjoin('RestaurantMaster AS RM','BR.RestaurantID','=','RM.RestaurantID')
                                ->where('BranchID',$FnBBrand[$j]->BranchID)
                                ->first();
                            }
                            elseif($FnBBrand[$j]->ProductID == "HQF")
                            {
                                $BranchDetail = $DBFnB->table('RestaurantMaster AS BR')
                                ->where('RestaurantID',$FnBBrand[$j]->BranchID)
                                ->first();
                            }
                            elseif($FnBBrand[$j]->ProductID == "RET")
                            {
                                $BranchDetail = $DBRetail->table('Branch AS BR')
                                ->leftjoin('StoreMaster AS SM','BR.StoreID','=','SM.StoreID')
                                ->where('BranchID',$FnBBrand[$j]->BranchID)
                                ->first();
                            }
                            elseif($FnBBrand[$j]->ProductID == "HQR")
                            {
                                $BranchDetail = $DBRetail->table('StoreMaster AS BR')
                                ->where('StoreID',$FnBBrand[$j]->BranchID)
                                ->first();
                            }
                            else{
                                $BranchDetail = $DBSalon->table('Branch AS BR')
                                ->leftjoin('BrandMaster AS BM','BR.BrandID','=','BM.BrandID')
                                ->where('BranchID',$FnBBrand[$j]->BranchID)
                                ->first();
                            }
                            if(!isset($BranchDetail))
                            {$FnBBrand[$j]->BranchDetail = "Branch nya gaada di master!";}
                            else{
                                $FnBBrand[$j]->BranchDetail = $BranchDetail;
                            }
                        }
        
        
                    }
        
                    $permission = $DBFnB->table('AccountOutletPermission AS AOP')
                    ->leftjoin('AccountOutlet AS AO','AO.AccountOutletID','=','AOP.AccountOutletID')
                    ->where('AOP.AccountOutletID',$FnBBrand[0]->AccountOutletID)
                    ->select(['SystemPermissionID','AccountID','ProductID','BranchID','IsEnterprise','Persists'])
                    ->distinct()
                    ->get();
                    
                    $FnBBrand[0]->Permission = $permission;
                    $Brand[$i] = $FnBBrand[0];
                    
                    
                }
            }
            else{
                $FnBBrand = array();
                $FnBBrand[0] = new \stdClass();
                if($Product == "RES")
                {
                    $BranchDetail = $DBFnB->table('Branch AS BR')
                    ->leftjoin('RestaurantMaster AS RM','BR.RestaurantID','=','RM.RestaurantID')
                    ->where('BranchID',$BranchID[$i])
                    ->first();
                    $FnBBrand[0]->ProductID = $Product;
                }
                elseif($Product == "HQF")
                {
                    $BranchDetail = $DBFnB->table('RestaurantMaster AS BR')
                    ->where('RestaurantID',$BranchID[$i])
                    ->first();
                    $FnBBrand[0]->ProductID = $Product;
                }
                elseif($Product == "RET")
                {
                    $BranchDetail = $DBRetail->table('Branch AS BR')
                    ->leftjoin('StoreMaster AS SM','BR.StoreID','=','SM.StoreID')
                    ->where('BranchID',$BranchID[$i])
                    ->first();
                    $FnBBrand[0]->ProductID = $Product;
                }
                elseif($Product == "HQR")
                {
                    $BranchDetail = $DBRetail->table('StoreMaster AS BR')
                    ->where('StoreID',$BranchID[$i])
                    ->first();
                    $FnBBrand[0]->ProductID = $Product;
                }
                else{
                    $BranchDetail = $DBSalon->table('Branch AS BR')
                    ->leftjoin('BrandMaster AS BM','BR.BrandID','=','BM.BrandID')
                    ->where('BranchID',$BranchID[$i])
                    ->first();
                    $FnBBrand[0]->ProductID = $Product;
                }
                if(!isset($BranchDetail))
                {$FnBBrand[0]->BranchDetail = "Branch nya gaada di master!";}
                else{
                        $FnBBrand[0]->BranchDetail = $BranchDetail;
                    }
                $FnBBrand[0]->Permission = [];
                $Brand[$i] = $FnBBrand[0];
                
            }
    
            
        }
        // $FnBBrand = $DBFnB->table('AccountOutlet AS AO')
        // ->where('BranchID',$BranchID)
        // ->where('ProductID',$Product)
        // ->where('AccountID',$AccountID)
        // ->select(DB::raw('*,ROW_NUMBER () OVER (PARTITION BY "BranchID", "ProductID", "AccountID" ORDER BY "AccountOutletID") "RowNumber"'))
        // ->get()->toarray();
        // $permission = [];
        // if(count($FnBBrand) > 0)
        // {
        //     for($i=0;$i<count($FnBBrand);$i++)
        //     {
        //         if($FnBBrand[$i]->RowNumber == 2)
        //         {
        //             array_splice($FnBBrand,$i,1);
        //             $i = $i-1;
        //         }
        //         else{
        //             if($FnBBrand[$i]->ProductID == "RES")
        //             {
        //                 $BranchDetail = $DBFnB->table('Branch AS BR')
        //                 ->leftjoin('RestaurantMaster AS RM','BR.RestaurantID','=','RM.RestaurantID')
        //                 ->where('BranchID',$FnBBrand[$i]->BranchID)
        //                 ->first();
        //             }
        //             elseif($FnBBrand[$i]->ProductID == "HQF")
        //             {
        //                 $BranchDetail = $DBFnB->table('RestaurantMaster AS BR')
        //                 ->where('RestaurantID',$FnBBrand[$i]->BranchID)
        //                 ->first();
        //             }
        //             elseif($FnBBrand[$i]->ProductID == "RET")
        //             {
        //                 $BranchDetail = $DBRetail->table('Branch AS BR')
        //                 ->leftjoin('StoreMaster AS SM','BR.StoreID','=','SM.StoreID')
        //                 ->where('BranchID',$FnBBrand[$i]->BranchID)
        //                 ->first();
        //             }
        //             elseif($FnBBrand[$i]->ProductID == "HQR")
        //             {
        //                 $BranchDetail = $DBRetail->table('StoreMaster AS BR')
        //                 ->where('StoreID',$FnBBrand[$i]->BranchID)
        //                 ->first();
        //             }
        //             else{
        //                 $BranchDetail = $DBSalon->table('Branch AS BR')
        //                 ->leftjoin('BrandMaster AS BM','BR.BrandID','=','BM.BrandID')
        //                 ->where('BranchID',$FnBBrand[$i]->BranchID)
        //                 ->first();
        //             }
        //             if(!isset($BranchDetail))
        //             {$FnBBrand[$i]->BranchDetail = "Branch nya gaada di master!";}
        //             else{
        //                 $FnBBrand[$i]->BranchDetail = $BranchDetail;
        //             }
        //         }


        //     }

        //     $permission = $DBFnB->table('AccountOutletPermission AS AOP')
        //     ->leftjoin('AccountOutlet AS AO','AO.AccountOutletID','=','AOP.AccountOutletID')
        //     ->where('AOP.AccountOutletID',$FnBBrand[0]->AccountOutletID)
        //     ->select(['SystemPermissionID','AccountID','ProductID','BranchID','IsEnterprise','Persists'])
        //     ->distinct()
        //     ->get();
        // }
        // else{
        //     $FnBBrand = array();
        //     $FnBBrand[0] = new \stdClass();
        //     if($Product == "RES")
        //     {
        //         $BranchDetail = $DBFnB->table('Branch AS BR')
        //         ->leftjoin('RestaurantMaster AS RM','BR.RestaurantID','=','RM.RestaurantID')
        //         ->where('BranchID',$BranchID)
        //         ->first();
        //         $FnBBrand[0]->ProductID = $Product;
        //     }
        //     elseif($Product == "HQF")
        //     {
        //         $BranchDetail = $DBFnB->table('RestaurantMaster AS BR')
        //         ->where('RestaurantID',$BranchID)
        //         ->first();
        //         $FnBBrand[0]->ProductID = $Product;
        //     }
        //     elseif($Product == "RET")
        //     {
        //         $BranchDetail = $DBRetail->table('Branch AS BR')
        //         ->leftjoin('StoreMaster AS SM','BR.StoreID','=','SM.StoreID')
        //         ->where('BranchID',$BranchID)
        //         ->first();
        //         $FnBBrand[0]->ProductID = $Product;
        //     }
        //     elseif($Product == "HQR")
        //     {
        //         $BranchDetail = $DBRetail->table('StoreMaster AS BR')
        //         ->where('StoreID',$BranchID)
        //         ->first();
        //         $FnBBrand[0]->ProductID = $Product;
        //     }
        //     else{
        //         $BranchDetail = $DBSalon->table('Branch AS BR')
        //         ->leftjoin('BrandMaster AS BM','BR.BrandID','=','BM.BrandID')
        //         ->where('BranchID',$BranchID)
        //         ->first();
        //         $FnBBrand[0]->ProductID = $Product;
        //     }
        //     $FnBBrand[0]->BranchDetail = $BranchDetail;
        // }


      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Branch' => $Brand
      );

      return Response()->json($endresult);

    }

    
}
