<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;
use Mail;
use App\Mail\SendInvoice;

class TicketingController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
        $link = $request->url();
        $Username = DB::table('User')
        ->where('UserID',$this->param->UserID)
        ->first()->Username;
        $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
        $log = DB::table('LogActivity')
        ->insert(array('UserID' => $this->param->UserID, 'Activity' => $link,
        'Parameter' => $request->getContent(), 'Time' => $now,'Username' => $Username));
    }

public function getTicketingStatus(request $request){


          $TicketStatus = DB::table('TicketStatus')
          ->get();

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Data' => $TicketStatus
      );
       return Response()->json($endresult);
    }

    public function getReportType(request $request){


        $ReportType = DB::table('ReportType')
        ->get();

          $endresult = array(
              'Status' => 0,
              'Errors' => array(),
              'Message' => "Success",
              'Data' => $ReportType
          );
           return Response()->json($endresult);
        }

        public function getPlatform(Request $request){
            $input = json_decode($request->getContent(),true);
            $rules = [

            ];

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                $errors = $validator->errors();
                $errorList = $this->checkErrors($rules, $errors);
                $additional = null;
                $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
                return response()->json($response);
            }

            $result = DB::table('Platform')
            ->get();

               $endresult = array(
                  'Status' => 0,
                  'Errors' => array(),
                  'Message' => "Success",
                  'Data' => $result
              );


        return Response()->json($endresult);

        }

public function getTicketing(Request $request){
    $input = json_decode($request->getContent(),true);
    $rules = [
        'StartDate' => 'required|date',
        'EndDate' => 'required|date'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }

    $result = DB::table('Ticket')
    ->leftjoin('TicketStatus','Ticket.TicketStatusID','=','TicketStatus.TicketStatusID')
    ->leftjoin('ReportType','Ticket.ReportTypeID','=','ReportType.ReportTypeID')
    ->leftjoin('User AS U','Ticket.ReporterID','=','U.UserID')
    ->leftjoin('Branch AS Br','Br.BranchID','=','Ticket.BranchID')
    ->leftjoin('Brand AS B','B.BrandID','=','Br.BrandID')
    ->leftjoin('Product AS P','P.ProductID','=','B.ProductID')
    ->where('Date','>=',$input['StartDate'])
    ->where('Date','<=',$input['EndDate']);

    if(@$input['PlatformID'] != null)
    {
        $result = $result->where('PlatformID',$input['PlatformID']);
    }
    if(@$input['ProductID'] != null)
    {
        $result = $result->where('Ticket.ProductID',$input['ProductID']);
    }
    if(@$input['Status'] != null)
    {
        $result = $result->where('Ticket.TicketStatusID',$input['Status']);
    }
    if(@$input['Direction'] != null && @$input['Column'] != null)
    {
        $result = $result->orderby(@$input['Column'],@$input['Direction']);
    }

    $result = $result->get();

    for($i=0;$i<count($result);$i++)
    {
        $Branch = DB::table('Branch AS br')
        ->leftjoin('Brand AS b','b.BrandID','=','br.BrandID')
        ->where('b.BrandID',$result[$i]->BrandID)
        ->get();
        $result[$i]->Branch = $Branch;
        $Resolver = DB::table('TicketResolver')
        ->where('TicketID',$result[$i]->TicketID)
        ->get();
        $result[$i]->Resolver = $Resolver;
    }


       $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Data' => $result
      );


return Response()->json($endresult);

}

public function getTicketingDetail(Request $request){

    $input = json_decode($request->getContent(),true);
    $rules = [
        'TicketID' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }

    $ID = $input['TicketID'];
    $ticket = DB::table('Ticket')
    ->leftjoin('TicketStatus','Ticket.TicketStatusID','=','TicketStatus.TicketStatusID')
    ->leftjoin('ReportType','Ticket.ReportTypeID','=','ReportType.ReportTypeID')
    ->leftjoin('Platform', 'Ticket.PlatformID','=','Platform.PlatformID')
    ->leftjoin('Product','Ticket.ProductID','=','Product.ProductID')
    ->where('TicketID',$ID)
    ->get();
    $images = DB::table('TicketImage')
    ->where('TicketID',$ID)
    ->wherenull('Archived')
    ->get();
    $resolver = DB::table('TicketResolver')
    ->where('TicketID',$ID)
    ->wherenull('Archived')
    ->get();

    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'Data' => array(
            'Header' => $ticket,
            'Images' => $images,
            'Resolver' => $resolver
        )
    );

return Response()->json($endresult);

}

public function getCustomer(Request $request){

    $input = json_decode($request->getContent(),true);
    $rules = [
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }

    $Customer = DB::select('SELECT distinct("CustomerBranchFullName"),"CustomerBranchPhone" FROM "public"."Ticket"');

    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'Data' => $Customer
    );

return Response()->json($endresult);

}

public function getReporterUser(Request $request){
    $UserType = DB::table('UserTypePermission')
    ->where('PermissionID',"IncludeInGetReporter")
    ->select(['UserTypeID'])
    ->get();
    for($i = 0; $i<count($UserType);$i++)
    {
        $UserTypeArray[$i] = $UserType[$i]->UserTypeID;
    }

    $User = DB::table('User')
    ->wherein('UserTypeID',$UserTypeArray)
    ->get();

    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'Data' => $User
    );

return Response()->json($endresult);

}

public function getResolverUser(Request $request){
    $UserType = DB::table('UserTypePermission')
    ->where('PermissionID',"IncludeInGetResolver")
    ->select(['UserTypeID'])
    ->get();
    for($i = 0; $i<count($UserType);$i++)
    {
        $UserTypeArray[$i] = $UserType[$i]->UserTypeID;
    }

    $User = DB::table('User')
    ->wherein('UserTypeID',$UserTypeArray)
    ->get();

    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'Data' => $User
    );

return Response()->json($endresult);

}

      public function getTicketDetailbyTicketID(Request $request){

          $input = json_decode($request->getContent(),true);
          $rules = [
              'TicketID' => 'required'
          ];

          $validator = Validator::make($input, $rules);
          if ($validator->fails()) {
              $errors = $validator->errors();
              $errorList = $this->checkErrors($rules, $errors);
              $additional = null;
              $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
              return response()->json($response);
          }
          $UserID = $this->param->UserID;
          $TicketID = @$input['TicketID'];


              $result = DB::table('TicketDetail AS TD')
              ->leftjoin('Ticket AS T','TD.TicketID','=','T.TicketID')
              ->leftjoin('Branch AS Br','Br.BranchID','=','T.BranchID')
              ->leftjoin('TicketStatus AS Ts','Ts.TicketStatusID','=','TD.TicketStatusID')
              ->get();
          // return $result;
//cek isi data branch untuk brand tertentu ada atau tidak.

          $endresult = array(
              'Status' => 0,
              'Errors' => array(),
              'Message' => "Success",
              'Data' => $result
          );

return Response()->json($endresult);

    }


    public function InsertUpdateTicket(Request $request){
       $input = json_decode($request->getContent(), true);
       $rules = [
        'Data' => 'required'
       ];

       $validator = Validator::make($input, $rules);
       if ($validator->fails()) {
           $errors = $validator->errors();
           $errorList = $this->checkErrors($rules, $errors);
           $additional = null;
           $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
           return response()->json($response);
       }
       $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
       
       for($i=0;$i<count($input['Data']);$i++)
       {
            
            $Data = @$input['Data'][$i];
            $ID = @$Data['TicketID'];
            $UserID = $this->param->UserID;
            $idate = @$Data['Date'];
            if($idate === null)
                {
                    $year = date('Y');
                    $month = date('m');
                    $date = date('d');
                    $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
                    $idate = $now;
                    $time = $now;
                }
            else{
                $year = substr($idate,0,4);
                $month = substr($idate,5,2);
                $date = substr($idate,8,2);
                $time = $Data['Times'];
                $idate = @$Data['Date'];
                }
                
                $num = DB::table('Ticket')
                ->whereraw('extract(month from "Date") ='.$month.' and extract(year from "Date") ='.$year)
                ->max('TicketNO');
                $year = substr($year,2,2);
                $num = substr($num,4,3);
                $num = $num+1;
                $num = str_pad($num, 3, '0', STR_PAD_LEFT);
                $ticketNO = $year.$month.$num.$date;

                $param = array (
                    'BranchID' => $Data['BranchID'],
                    'Date' => $idate,
                    'BrandID' => $Data['BrandID'],
                    'ProductID' => $Data['ProductID'],
                    'Summary' => $Data['Summary'],
                    'Description' => $Data['Description'],
                    'CustomerBranchPhone' => $Data['CustomerBranchPhone'],
                    'CustomerBranchFullName' => $Data['CustomerBranchFullName'],
                    'ReporterID' => $Data['ReporterID'],
                    'ReportTypeID' => $Data['ReportTypeID'],
                    'TicketStatusID' => @$Data['TicketStatusID'],
                    'TicketNO' => $ticketNO,
                    'Time' => $time,
                    'Devices' => $Data['Device'],
                    'PlatformID' => $Data['Platform']
                  );
                  $param2 = array (
                      'BranchID' => $Data['BranchID'],
                      'Date' => $idate,
                      'BrandID' => $Data['BrandID'],
                      'ProductID' => $Data['ProductID'],
                      'Summary' => $Data['Summary'],
                      'Description' => $Data['Description'],
                      'CustomerBranchPhone' => $Data['CustomerBranchPhone'],
                      'CustomerBranchFullName' => $Data['CustomerBranchFullName'],
                      'ReporterID' => $Data['ReporterID'],
                      'ReportTypeID' => $Data['ReportTypeID'],
                      'TicketStatusID' => @$Data['TicketStatusID'],
                      'Time' => $time,
                      'Devices' => $Data['Device'],
                      'PlatformID' => $Data['Platform']
                  );

                  if(@$Data['VisitDate'] != null)
                    {
                    $param['VisitDate'] = @$Data['VisitDate'];
                    $param['VisitTimeFrom'] = @$Data['VisitTimesFrom'];
                    $param['VisitTimeTo'] = @$Data['VisitTimesTo'];
                    $param['VisitFor'] = @$Data['VisitFor'];
                    $param2['VisitDate'] = @$Data['VisitDate'];
                    $param2['VisitTimeFrom'] = @$Data['VisitTimesFrom'];
                    $param2['VisitTimeTo'] = @$Data['VisitTimesTo'];
                    $param2['VisitFor'] = @$Data['VisitFor'];
                    }

                    if(@$Data['ResolutionSummary'] != null)
                        {
                            if(@$Data['ResolutionDate'] == null)
                            {
                                $param['ResolutionDate'] = $now;
                                $param['ResolutionTime'] = $now;
                                $param2['ResolutionDate'] = $now;
                                $param2['ResolutionTime'] = $now;
                            }
                            else{
                                $param['ResolutionDate'] = @$Data['ResolutionDate'];
                                $param['ResolutionTime'] = @$Data['ResolutionTimes'];
                                $param2['ResolutionDate'] = @$Data['ResolutionDate'];
                                $param2['ResolutionTime'] = @$Data['ResolutionTimes'];

                            }

                            $param['ResolutionSummary'] = @$Data['ResolutionSummary'];
                            $param['ResolutionDescription'] = @$Data['ResolutionDescription'];
                            $param2['ResolutionSummary'] = @$Data['ResolutionSummary'];
                            $param2['ResolutionDescription'] = @$Data['ResolutionDescription'];
                        }

                        if($ID == null)
                        {
                        $result = DB::table('Ticket')->insert($param);
                        $ID = $this->getLastVal();
                        }
                        else {
                          $result = DB::table('Ticket')->where('TicketID', $ID)->update($param2);
                             }
                             $ResolverID = @$Data['ResolverID'];
                         if($ResolverID != null)
                         {
                             DB::table('TicketResolver')
                             ->where('TicketID',$ID)
                             ->delete();
                             for($j=0;$j<count($ResolverID);$j++)
                             {
                                 $result = DB::table('TicketResolver')
                                 ->insert(array('UserID' => $ResolverID[$j], 'TicketID' => $ID));
                             }
                         }
                         else{
                            DB::table('TicketResolver')
                            ->where('TicketID',$ID)
                            ->delete();
                         }

                         if(@$Data['Images'] !== null){
                            $images = @$Data['Images'];
            
                                for($i=0;$i<count($images);$i++)
                                {
                                    if(@$images[$i]['ImageID'] === null)
                                    {
                                    $arr = array(
                                        'UserID' => $this->param->UserID,
                                        'ObjectID' => $ID,
                                        'Folder' => 'Ticket',
                                        'Filename' => $images[$i]['Filename'],
                                        'Data' =>  $images[$i]['Data']
                                    );
                                    $path = $this->upload_to_s3($arr);
                                    $param = array(
                                        'Image' => $path,
                                        'TicketID' => $ID,
                                        'Caption' => $images[$i]['Caption']
                                    );
                                    $result = DB::table('TicketImage')
                                    ->insert($param);
                                    }
                                    else{
                                        $TicketImageID = $images[$i]['ImageID'];
                                        if(@$images[$i]['Data'] != null)
                                        {
                                            $arr = array(
                                                'UserID' => $this->param->UserID,
                                                'ObjectID' => $TicketImageID,
                                                'Folder' => 'Ticket',
                                                'Filename' => $images[$i]['Filename'],
                                                'Data' =>  $images[$i]['Data']
                                            );
                                            $path = $this->upload_to_s3($arr);
                                            $param = array(
                                                'Image' => $path,
                                                'Caption' => $images[$i]['Caption']
                                            );
                                            $result = DB::table('TicketImage')
                                            ->where('TicketImageID',$TicketImageID)
                                            ->update($param);
                                        }
                                        else{
                                            $param = array(
                                                'Caption' => $images[$i]['Caption']
                                            );
                                            $result = DB::table('TicketImage')
                                            ->where('TicketImageID',$TicketImageID)
                                            ->update($param);
                                        }
                                        $response = $this->generateResponse(0, [], "Success", ['Ticketing'=>$result]);
                                    }
                            }
            
            
                                // return(@$Data['Images']['Data']);
                                // die();
                            }
            
                            if(@$Data['ImageDelete'] !== null){
                                $images = $Data['ImageDelete'];
                                for($i=0;$i<count($images);$i++)
                                {
                                    $param = array('Archived' => $now);
                                    $result = DB::table('TicketImage')
                                    ->where('TicketImageID',$images[$i])
                                    ->update($param);
            
                                    $response = $this->generateResponse(0, [], "Success", ['Ticketing'=>$result]);
                                }
                                }

        }
       
       

  
      



    $result = $this->checkReturn($result);
    $result['TicketNO'] = $ID;
    return Response()->json($result);
  }


    public function DeleteBranch(Request $request){
         $input = json_decode($this->request->getContent(),true);
         $rules = [
           'BranchID' => 'required'
         ];
         $validator = Validator::make($input, $rules);
         if ($validator->fails()) {
             $errors = $validator->errors();
             $errorList = $this->checkErrors($rules, $errors);
             $additional = null;
             $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
             return response()->json($response);
         }
         $BranchID = $input['BranchID'];
         $param = array('Status' => 'D','Archived' => now());
         $result = DB::table('Branch')->where('BranchID', $BranchID)->update($param);


        $result = $this->checkReturn($result);

        return Response()->json($result);

    }
}
