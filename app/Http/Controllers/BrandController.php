<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;
use PDF;

class BrandController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
        $link = $request->url();
        $Username = DB::table('User')
        ->where('UserID',$this->param->UserID)
        ->first()->Username;
        $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
        $log = DB::table('LogActivity')
        ->insert(array('UserID' => $this->param->UserID, 'Activity' => $link,
        'Parameter' => $request->getContent(), 'Time' => $now,'Username' => $Username));
    }

    public function exportBrandPOS(request $request){
        $input = json_decode($this->request->getContent(),true);
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $FnB = $DBFnB->select('Select rm."RestaurantName", b."BranchName", "DevStatus",
        (SELECT "ReceivedTime" from "Order" o where o."BranchID" = b."BranchID" and "ReceivedTime" is not null order by 1 asc limit 1) as "FirstTransaction",
        (SELECT "ReceivedTime" from "Order" o where o."BranchID" = b."BranchID" and "ReceivedTime" is not null order by 1 desc limit 1) as "LastTransaction"
        FROM "Branch" b
        join "RestaurantMaster" rm on rm."RestaurantID" = b."RestaurantID"
        where ("DevStatus" is null or "DevStatus" = \'Trial\')
        and ("Status" not in (\'closed\', \'unsub\') or "Status" is null )
        order by 1,2');

        $Retail = $DBRetail->select(
            'Select rm."StoreName", b."BranchName", "DevStatus",
            (SELECT "ReceivedTime" from "SalesTransaction" o where o."BranchID" = b."BranchID" and "ReceivedTime" is not null order by 1 asc limit 1) as "FirstTransaction",
            (SELECT "ReceivedTime" from "SalesTransaction" o where o."BranchID" = b."BranchID" and "ReceivedTime" is not null order by 1 desc limit 1) as "LastTransaction"
            FROM "Branch" b
            join "StoreMaster" rm on rm."StoreID" = b."StoreID"
            where ("DevStatus" is null or "DevStatus" = \'Trial\')
            and ("Status" not in (\'closed\', \'unsub\') or "Status" is null )
            order by 1,2');

        $Salon = $DBSalon->select(
            'Select rm."BrandName", b."BranchName", "DevStatus",
            (SELECT "ReceivedTime" from "SalesTransaction" o where o."BranchID" = b."BranchID" and "ReceivedTime" is not null order by 1 asc limit 1) as "FirstTransaction",
            (SELECT "ReceivedTime" from "SalesTransaction" o where o."BranchID" = b."BranchID" and "ReceivedTime" is not null order by 1 desc limit 1) as "LastTransaction"
            FROM "Branch" b
            join "BrandMaster" rm on rm."BrandID" = b."BrandID"
            where ("DevStatus" is null or "DevStatus" = \'Trial\')
            and ("Status" not in (\'closed\', \'unsub\') or "Status" is null )
            order by 1,2');

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'ReportExcel' => array(
              'FnB' => $FnB,
              'Retail' => $Retail,
              'Salon' => $Salon
          )
      );

      return Response()->json($endresult);

    }

    public function getBrand(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
          'isGetAll' => 'required|boolean'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $isGetAll = $input['isGetAll'];
        $result = DB::table('Brand')
        ->leftjoin('Product', 'Brand.ProductID','=','Product.ProductID')
        ->select(['BrandID','BrandName','Email','Brand.ProductID','ProductName','Contact','Image','Phone','Address','CreatedBy'])
        ->where('Brand.Archived',null);
        
        if($isGetAll == false)
        {
           
            $result->where('Brand.CreatedBy',$this->param->UserID);
        }

        $brand = $result->orderby('BrandID','desc')
        ->get();


      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Brand' => $brand
      );

       return Response()->json($endresult);
    }

    public function importBrand(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
          'Data' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        for($i=0;$i<count($input['Data']);$i++){
            $Brand = $input['Data'][$i];
            $param = array(
                    'BrandName' => $Brand['BrandName'],
                    'ProductID' => $Brand['ProductID'],
                    'Email' => $Brand['Email'],
                    'Address' => $Brand['Address'],
                    'Contact' => $Brand['Contact'],
                    'Phone' => $Brand['Phone'],
                    'CreatedBy' => $Brand['UserID']
            );
            $result = DB::table('Brand')
            ->insert($param);
        }

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Brand' => $result
      );

       return Response()->json($endresult);
    }

    public function getBrandbyProductID(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
          'ProductID' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $result = DB::table('Brand')
        ->where('ProductID',$input['ProductID'])
        ->wherenull('Archived')
        ->get();

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Brand' => $result
      );

       return Response()->json($endresult);
    }

    public function getBrandDetail(Request $request){

      $input = json_decode($this->request->getContent(),true);
      $rules = [
        'BrandID' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $isGetAll = $input['isGetAll'];
      $BrandID = $input['BrandID'];
      $result = DB::table('Brand')
      ->leftjoin('Product', 'Brand.ProductID','=','Product.ProductID')
      ->leftjoin('User','Brand.CreatedBy','=','User.UserID')
      ->select(['BrandID','BrandName','Brand.Email','Brand.ProductID','Contact','ProductName','Image','Brand.Phone','Brand.Address','CreatedBy'])
      ->where('BrandID',$BrandID);
      if($isGetAll == false)
      {$result->where('CreatedBy',$this->param->UserID);}
      
    
      $brand = $result->get();
      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Brand' => $brand
      );
      return Response()->json($endresult);
}

public function InsertUpdateBrand(Request $request){
    $input = json_decode($request->getContent(),true);
    $rules = [
        'BrandName' => 'required',
        'ProductID' => 'required',
        'Email' => 'required|email',
        'Phone' => 'required',
        'Address' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }
    $ID = @$input['BrandID'];
    $unique = array(
        'Table' => "Brand",
        'ID' => $ID,
        'Column' => "BrandName",
        'String' => $input['BrandName']
    );
    $uniqueBrandName = $this->unique($unique);
    // $unique['Column'] = "Email";
    // $unique['String'] = $input['Email'];
    // $uniqueEmail = $this->unique($unique);
    $param = array(
        'BrandName' => $input['BrandName'],
        'ProductID' => @$input['ProductID'],
        'Email' => $input['Email'],
        'Phone' => $input['Phone'],
        'Contact' => @$input['Contact'],
        'Address' => $input['Address'],
        'CreatedBy' => $this->param->UserID);
        if(@$input['UserID'] != null)
        {
            $param['CreatedBy'] = $input['UserID'];
        }



      if ($ID == null){
        $result = DB::table('Brand')->insert($param);
        $ID = $this->getLastVal();
        }
      else{$param = array(
          'BrandName' => $input['BrandName'],
          'ProductID' => @$input['ProductID'],
          'Email' => $input['Email'],
          'Phone' => $input['Phone'],
          'Contact' => @$input['Contact'],
          'Address' => $input['Address'],
          'CreatedBy' => $input['UserID']);

          $result = DB::table('Brand')->where('BrandID',$ID)->update($param);}
      if(@$input['DeleteImage'] === true){
            $param = array("Image" => null);
            $result = DB::table('Brand')->where('BrandID',$ID)->update($param);
        }

      if(@$input['Images'] !== null){
              $arr = array(
                  'UserID' => $this->param->UserID,
                  'ObjectID' => $ID,
                  'Folder' => 'Brand',
                  'Filename' => @$input['Images']['Filename'],
                  'Data' =>  @$input['Images']['Data']
              );
              // return(@$input['Images']['Data']);
              // die();
              $path = $this->upload_to_s3($arr);
              $data = ['Image' => $path];
              $result = DB::table('Brand')->where('BrandID',$ID)->update($data);

              $response = $this->generateResponse(0, [], "Success", ['Brand'=>$result]);
          }

          $result = $this->checkReturn($result);
          // dd($ID);
          // $result['BrandID'] = $ID;
          return Response()->json($result);

  }

  public function DeleteBrand(Request $request){
       $input = json_decode($this->request->getContent(),true);
       $rules = [
         'BrandID' => 'required'
       ];

       $validator = Validator::make($input, $rules);
       if ($validator->fails()) {
           $errors = $validator->errors();
           $errorList = $this->checkErrors($rules, $errors);
           $additional = null;
           $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
           return response()->json($response);
       }
       $BrandID = @$input['BrandID'];
       $param = array('Status' => 'D','Archived' => now());
       $result = DB::table('Brand')->where('BrandID', $BrandID)->update($param);
       $result2 = DB::table('Branch')->where('BrandID',$BrandID)->update($param);
      $result = $this->checkReturn($result);

      return Response()->json($result);

  }

}
