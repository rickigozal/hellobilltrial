<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;

class ReportController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
        $link = $request->url();
        $Username = DB::table('User')
        ->where('UserID',$this->param->UserID)
        ->first()->Username;
        $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
        $log = DB::table('LogActivity')
        ->insert(array('UserID' => $this->param->UserID, 'Activity' => $link,
        'Parameter' => $request->getContent(), 'Time' => $now,'Username' => $Username));
    }

    public function getTaxReportForBRI(){
      $input = json_decode($this->request->getContent(),true);
      $rules = [
        'StartDate' => 'required|date',
        'EndDate' => 'required|date'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $DBRetail = DB::connection('pgsqlretail');
      $DBFnB = DB::connection('pgsqlfnb');
      $DBSalon = DB::connection('pgsqlsalon');
      $DBLog = DB::connection('mongoadmin');
      $DBTLog = DB::connection('mongotransaction');
      $UserID = $this->param->UserID;
      $StartDate = $input['StartDate'];
      $EndDate = $input['EndDate'];
      $BranchID = $input['BranchID'];
      $RestaurantID = $input['RestaurantID'];
      $TaxReport = $DBFnB->table('OrderBill AS OB')
      ->select([
          DB::raw('to_char("Date", \'yyyy-MM-dd HH24:mm:ss\') as "Date", coalesce("OrderBillNumber",\'-No Number-\')"BillNumber",
          "Bill"-coalesce("Discount",0) as "Gross", "ServiceTax" as "ServiceCharge", coalesce("OrderBillNumber",\'-No Number-\') as "Void",
          coalesce("Discount",0) as "DiscountBill","OrderBillID"')
      ])
      ->where('Date','>=',$StartDate)
      ->where('Date','<=',$EndDate)
      ->where('BranchID',$BranchID)
      ->where('RestaurantID',$RestaurantID)
      ->orderby('Date','ASC')
      ->get()->toArray();

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'TaxReport' => $TaxReport
      );

       return Response()->json($endresult);
    }

    public function getBranchForTaxReport(){
        $input = json_decode($this->request->getContent(),true);
        $rules = [

        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');


        $Branch = $DBFnB->table('Branch')
        ->wherenotnull('BranchID')
        ->where('BranchID','>',0)
        ->get();

        $endresult = array(
            'Status' => 0,
            'Errors' => array(),
            'Message' => "Success",
            'Branch' => $Branch
        );

         return Response()->json($endresult);
    }


    public function getCommission(){
      $input = json_decode($this->request->getContent(),true);
      $rules = [
        'StartDate' => 'required|date',
        'EndDate' => 'required|date',
        'isGetAll' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $UserID = $this->param->UserID;
      $StartDate = $input['StartDate'];
      $EndDate = $input['EndDate'];
      $isGetAll = $input['isGetAll'];
      if($isGetAll == true){
          $CommissionLicense = DB::table('UserCommission AS UC')
          ->leftjoin('User AS U','U.UserID','=','UC.UserID')
          ->leftjoin('UserType AS UT','U.UserTypeID','=','UT.UserTypeID')
          ->leftjoin('License AS LC','LC.LicenseID','=','UC.LicenseID')
          ->leftjoin('Service AS S','S.ServiceID','=','UC.ServiceID')
          ->leftjoin('Invoice AS P','UC.ProformaID','=','P.ProformaID')
          ->leftjoin('Quotation AS Q','Q.QuotationID','=','P.QuotationID')
          // ->leftjoin('QuotationDetailUser AS QDU','QDU.QuotationID','=','Q.QuotationID')
          ->leftjoin('Branch AS BR','BR.BranchID','=','P.BranchID')
          ->leftjoin('Brand AS B','B.BrandID','=','Q.BrandID')
          ->select(['UC.ProformaID','InvoiceID','InvoiceNO','ReceiptID','ReceiptNO','UC.UserID','UserFullName','U.UserTypeID','UserTypeName',
          'UC.LicenseID','LicenseName','UC.ServiceID','ServiceName','CommissionValue','UC.CommissionPercentage','UC.CommissionValue',
          'BrandName','Q.BrandID','P.BranchID','BR.BranchName'])
          ->where('UC.Date','>=',$StartDate)
          ->where('UC.Date','<=',$EndDate)
          ->wherenotnull('UC.LicenseID')
          ->get();

          $CommissionService = DB::table('UserCommission AS UC')
          ->leftjoin('User AS U','U.UserID','=','UC.UserID')
          ->leftjoin('UserType AS UT','U.UserTypeID','=','UT.UserTypeID')
          ->leftjoin('License AS LC','LC.LicenseID','=','UC.LicenseID')
          ->leftjoin('Service AS S','S.ServiceID','=','UC.ServiceID')
          ->leftjoin('Invoice AS P','UC.ProformaID','=','P.ProformaID')
          ->leftjoin('Quotation AS Q','Q.QuotationID','=','P.QuotationID')
          // ->leftjoin('QuotationDetailUser AS QDU','QDU.QuotationID','=','Q.QuotationID')
          ->leftjoin('Branch AS BR','BR.BranchID','=','P.BranchID')
          ->leftjoin('Brand AS B','B.BrandID','=','Q.BrandID')
          ->select(['UC.ProformaID','InvoiceID','InvoiceNO','ReceiptID','ReceiptNO','UC.UserID','UserFullName','U.UserTypeID','UserTypeName',
          'UC.LicenseID','LicenseName','UC.ServiceID','ServiceName','CommissionValue','UC.CommissionPercentage','UC.CommissionValue',
          'BrandName','Q.BrandID','P.BranchID','BR.BranchName'])
          ->where('UC.Date','>=',$StartDate)
          ->where('UC.Date','<=',$EndDate)
          ->wherenotnull('UC.ServiceID')
          ->get();

          $PaidInvoice = DB::table('Invoice AS I')
          ->leftjoin('UserCommission AS UC','UC.ProformaID','=','I.ProformaID')
          ->leftjoin('User AS U','U.UserID','=','UC.UserID')
          ->select(DB::raw('sum("CommissionValue") AS "Commission", "I"."ProformaID","UC"."UserID"'))
          ->where('I.StatusID',"P2")
          ->where('I.DeliveryDate','>=',$StartDate)
          ->where('I.DeliveryDate','<=',$EndDate)
          ->groupby('I.ProformaID')
          ->groupby('UC.UserID')
          ->get();

          $UnpaidInvoice = DB::table('Invoice AS I')
          ->leftjoin('UserCommission AS UC','UC.ProformaID','=','I.ProformaID')
          ->leftjoin('User AS U','U.UserID','=','UC.UserID')
          ->select(DB::raw('sum("CommissionValue") AS "Commission", "I"."ProformaID","UC"."UserID"'))
          ->where('I.StatusID',"P")
          ->where('I.DeliveryDate','>=',$StartDate)
          ->where('I.DeliveryDate','<=',$EndDate)
          ->groupby('I.ProformaID')
          ->groupby('UC.UserID')
          ->get();

          $ExpiredInvoice = DB::table('Invoice AS I')
          ->leftjoin('UserCommission AS UC','UC.ProformaID','=','I.ProformaID')
          ->leftjoin('User AS U','U.UserID','=','UC.UserID')
          ->select(DB::raw('sum("CommissionValue") AS "Commission", "I"."ProformaID","UC"."UserID"'))
          ->where('I.StatusID',"E")
          ->where('I.DeliveryDate','>=',$StartDate)
          ->where('I.DeliveryDate','<=',$EndDate)
          ->groupby('I.ProformaID')
          ->groupby('UC.UserID')
          ->get();

          $HardwareSales = DB::table('Quotation AS Q')
          ->leftjoin('Invoice AS I','I.QuotationID','=','Q.QuotationID')
          ->leftjoin('QuotationDetail AS QD','Q.QuotationID','=','QD.QuotationID')
          ->leftjoin('QuotationDetailHardware AS QDH','QDH.QuotationDetailID','=','QD.QuotationDetailID')
          ->select(DB::raw('sum("Quantity") as Quantity,
                           sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal'))
          ->wherenotnull('I.PaidDate')
          ->get();

          $LicenseSales = DB::table('Quotation AS Q')
          ->leftjoin('Invoice AS I','I.QuotationID','=','Q.QuotationID')
          ->leftjoin('QuotationDetail AS QD','QD.QuotationID','=','Q.QuotationID')
          ->leftjoin('QuotationDetailLicense AS QDL', 'QDL.QuotationDetailID','=','QD.QuotationDetailID')
          ->select(DB::raw('sum("Quantity") as Quantity,
                           sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal'))
          ->wherenotnull('I.PaidDate')
          ->get();

          $ServiceSales = DB::table('Quotation AS Q')
          ->leftjoin('Invoice AS I','I.QuotationID','=','Q.QuotationID')
          ->leftjoin('QuotationDetail AS QD','QD.QuotationID','=','Q.QuotationID')
          ->leftjoin('QuotationDetailService AS QDS', 'QDS.QuotationDetailID','=','QD.QuotationDetailID')
          ->select(DB::raw('sum("Quantity") as Quantity,
                           sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal'))
          ->wherenotnull('I.PaidDate')
          ->get();

          $OtherSales = DB::table('Quotation AS Q')
          ->leftjoin('Invoice AS I','I.QuotationID','=','Q.QuotationID')
          ->leftjoin('QuotationDetail AS QD','QD.QuotationID','=','Q.QuotationID')
          ->leftjoin('QuotationDetailOther AS QDO', 'QDO.QuotationDetailID','=','QD.QuotationDetailID')
          ->select(DB::raw('sum("Quantity") as Quantity,
                           sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal'))
          ->wherenotnull('I.PaidDate')
          ->get();

      }
      else{
          $CommissionLicense = DB::table('UserCommission AS UC')
          ->leftjoin('User AS U','U.UserID','=','UC.UserID')
          ->leftjoin('UserType AS UT','U.UserTypeID','=','UT.UserTypeID')
          ->leftjoin('License AS LC','LC.LicenseID','=','UC.LicenseID')
          ->leftjoin('Service AS S','S.ServiceID','=','UC.ServiceID')
          ->leftjoin('Invoice AS P','UC.ProformaID','=','P.ProformaID')
          ->leftjoin('Quotation AS Q','Q.QuotationID','=','P.QuotationID')
          // ->leftjoin('QuotationDetailUser AS QDU','QDU.QuotationID','=','Q.QuotationID')
          ->leftjoin('Branch AS BR','BR.BranchID','=','P.BranchID')
          ->leftjoin('Brand AS B','B.BrandID','=','Q.BrandID')
          ->select(['UC.ProformaID','InvoiceID','InvoiceNO','ReceiptID','ReceiptNO','UC.UserID','UserFullName','U.UserTypeID','UserTypeName',
          'UC.LicenseID','LicenseName','UC.ServiceID','ServiceName','CommissionValue','UC.CommissionPercentage','UC.CommissionValue',
          'BrandName','Q.BrandID','P.BranchID','BR.BranchName'])
          ->where('UC.Date','>=',$StartDate)
          ->where('UC.Date','<=',$EndDate)
          ->where('UC.UserID',$this->param->UserID)
          ->wherenotnull('UC.LicenseID')
          ->get();

          $CommissionService = DB::table('UserCommission AS UC')
          ->leftjoin('User AS U','U.UserID','=','UC.UserID')
          ->leftjoin('UserType AS UT','U.UserTypeID','=','UT.UserTypeID')
          ->leftjoin('License AS LC','LC.LicenseID','=','UC.LicenseID')
          ->leftjoin('Service AS S','S.ServiceID','=','UC.ServiceID')
          ->leftjoin('Invoice AS P','UC.ProformaID','=','P.ProformaID')
          ->leftjoin('Quotation AS Q','Q.QuotationID','=','P.QuotationID')
          // ->leftjoin('QuotationDetailUser AS QDU','QDU.QuotationID','=','Q.QuotationID')
          ->leftjoin('Branch AS BR','BR.BranchID','=','P.BranchID')
          ->leftjoin('Brand AS B','B.BrandID','=','Q.BrandID')
          ->select(['UC.ProformaID','InvoiceID','InvoiceNO','ReceiptID','ReceiptNO','UC.UserID','UserFullName','U.UserTypeID','UserTypeName',
          'UC.LicenseID','LicenseName','UC.ServiceID','ServiceName','CommissionValue','UC.CommissionPercentage','UC.CommissionValue',
          'BrandName','Q.BrandID','P.BranchID','BR.BranchName'])
          ->where('UC.Date','>=',$StartDate)
          ->where('UC.Date','<=',$EndDate)
          ->where('UC.UserID',$this->param->UserID)
          ->wherenotnull('UC.ServiceID')
          ->get();

          $PaidInvoice = DB::table('Invoice AS I')
          ->leftjoin('UserCommission AS UC','UC.ProformaID','=','I.ProformaID')
          ->leftjoin('User AS U','U.UserID','=','UC.UserID')
          ->select(DB::raw('sum("CommissionValue") AS "Commission", "I"."ProformaID","UC"."UserID"'))
          ->where('I.StatusID',"P2")
          ->where('I.DeliveryDate','>=',$StartDate)
          ->where('I.DeliveryDate','<=',$EndDate)
          ->where('UC.UserID',$this->param->UserID)
          ->groupby('I.ProformaID')
          ->groupby('UC.UserID')
          ->get();

          $UnpaidInvoice = DB::table('Invoice AS I')
          ->leftjoin('UserCommission AS UC','UC.ProformaID','=','I.ProformaID')
          ->leftjoin('User AS U','U.UserID','=','UC.UserID')
          ->select(DB::raw('sum("CommissionValue") AS "Commission", "I"."ProformaID","UC"."UserID"'))
          ->where('I.StatusID',"P")
          ->where('I.DeliveryDate','>=',$StartDate)
          ->where('I.DeliveryDate','<=',$EndDate)
          ->where('UC.UserID',$this->param->UserID)
          ->groupby('I.ProformaID')
          ->groupby('UC.UserID')
          ->get();

          $ExpiredInvoice = DB::table('Invoice AS I')
          ->leftjoin('UserCommission AS UC','UC.ProformaID','=','I.ProformaID')
          ->leftjoin('User AS U','U.UserID','=','UC.UserID')
          ->select(DB::raw('sum("CommissionValue") AS "Commission", "I"."ProformaID","UC"."UserID"'))
          ->where('I.StatusID',"E")
          ->where('I.DeliveryDate','>=',$StartDate)
          ->where('I.DeliveryDate','<=',$EndDate)
          ->where('UC.UserID',$this->param->UserID)
          ->groupby('I.ProformaID')
          ->groupby('UC.UserID')
          ->get();

          $HardwareSales = DB::table('Quotation AS Q')
          ->leftjoin('Invoice AS I','I.QuotationID','=','Q.QuotationID')
          ->leftjoin('QuotationDetailUser AS QDU', 'Q.QuotationID','=','QDU.QuotationID')
          ->leftjoin('QuotationDetail AS QD','Q.QuotationID','=','QD.QuotationID')
          ->leftjoin('QuotationDetailHardware AS QDH','QDH.QuotationDetailID','=','QD.QuotationDetailID')
          ->select(DB::raw('sum("Quantity") as Quantity,
                           sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal'))
          ->where('QDU.UserID',$this->param->UserID)
          ->wherenotnull('I.PaidDate')
          ->get();

          $LicenseSales = DB::table('Quotation AS Q')
          ->leftjoin('Invoice AS I','I.QuotationID','=','Q.QuotationID')
          ->leftjoin('QuotationDetailUser AS QDU', 'Q.QuotationID','=','QDU.QuotationID')
          ->leftjoin('QuotationDetail AS QD','QD.QuotationID','=','Q.QuotationID')
          ->leftjoin('QuotationDetailLicense AS QDL', 'QDL.QuotationDetailID','=','QD.QuotationDetailID')
          ->select(DB::raw('sum("Quantity") as Quantity,
                           sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal'))
          ->where('QDU.UserID',$this->param->UserID)
          ->wherenotnull('I.PaidDate')
          ->get();

          $ServiceSales = DB::table('Quotation AS Q')
          ->leftjoin('Invoice AS I','I.QuotationID','=','Q.QuotationID')
          ->leftjoin('QuotationDetailUser AS QDU', 'Q.QuotationID','=','QDU.QuotationID')
          ->leftjoin('QuotationDetail AS QD','QD.QuotationID','=','Q.QuotationID')
          ->leftjoin('QuotationDetailService AS QDS', 'QDS.QuotationDetailID','=','QD.QuotationDetailID')
          ->select(DB::raw('sum("Quantity") as Quantity,
                           sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal'))
          ->where('QDU.UserID',$this->param->UserID)
          ->wherenotnull('I.PaidDate')
          ->get();

          $OtherSales = DB::table('Quotation AS Q')
          ->leftjoin('Invoice AS I','I.QuotationID','=','Q.QuotationID')
          ->leftjoin('QuotationDetailUser AS QDU', 'Q.QuotationID','=','QDU.QuotationID')
          ->leftjoin('QuotationDetail AS QD','QD.QuotationID','=','Q.QuotationID')
          ->leftjoin('QuotationDetailOther AS QDO', 'QDO.QuotationDetailID','=','QD.QuotationDetailID')
          ->select(DB::raw('sum("Quantity") as Quantity,
                           sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal'))
          ->where('QDU.UserID',$this->param->UserID)
          ->wherenotnull('I.PaidDate')
          ->get();


      }



      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Transaction' => array(
              'PaidTransaction' => $PaidInvoice,
              'ExpiredTransaction' => $ExpiredInvoice
          ),
          'Commission' => array(
              'LicenseCommission' => $CommissionLicense,
              'ServiceCommission' => $CommissionService
          ),
          'SalesTotalbyCategory' => array(
              'LicenseSales' => $LicenseSales,
              'HardwareSales' => $HardwareSales,
              'ServiceSales' => $ServiceSales,
              'OtherSales' => $OtherSales
          )
      );

       return Response()->json($endresult);
    }

public function getSalesCommission(Request $request){
    $input = json_decode($request->getContent(),true);
    $rules = [
        'StartDate' => 'required|date',
        'EndDate' => 'required|date'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }

  // $result = DB::select(
  //     'select "U"."UserID","UserFullName",COALESCE(sum("UC"."CommissionValue"), 0) "CommissionValue",COALESCE(sum("QDL"."Quantity"), 0) "LicenseQuantity",
  //     COALESCE(sum("QDS"."Quantity"), 0) "ServiceQuantity"
  //       from "User" AS "U"
  //       left join "UserType" AS "UT" ON "U"."UserTypeID" = "UT"."UserTypeID"
  //       left join ( SELECT * FROM "UserCommission" "UC"
  //       WHERE "UC"."Date" <= \''.$input['EndDate'].'\' and "UC"."Date" >= \''.$input['StartDate'].'\'
  //       ) "UC" ON "UC"."UserID" = "U"."UserID"
  //       left join "Invoice" AS "P" on "P"."ProformaID" = "UC"."ProformaID"
  //       left join "Quotation" AS "Q" on "Q"."QuotationID" = "P"."QuotationID"
  //       left join "QuotationDetail" AS "QD" on "Q"."QuotationID" = "QD"."QuotationID"
  //       left join "QuotationDetailLicense" AS "QDL" on "QDL"."QuotationDetailID" = "QD"."QuotationDetailID"
  //       left join "QuotationDetailService" AS "QDS" on "QDS"."QuotationDetailID" = "QD"."QuotationDetailID"
  //       where "UT"."UserTypeName" = \'Sales\' or "UT"."UserTypeName" = \'Telesales\'
  //       group by "U"."UserID"
  //       order by "U"."UserID"'
  //   );
  $UserTypeName = DB::table('UserTypePermission as utp')
  ->leftjoin('UserType AS ut','utp.UserTypeID','=','ut.UserTypeID')
  ->where('utp.PermissionID','like','IncludeInCommissionReport')
  ->get();

  foreach($UserTypeName as $d)
  {
      $UserType []= $d->UserTypeID;
  }
  
  $sub = DB::table('UserCommission AS UC')
  ->where('UC.Date','<=',$input['EndDate'])
  ->where('UC.Date','>=',$input['StartDate']);
    $result = DB::table('User AS U')
    ->leftjoin('UserType AS UT','U.UserTypeID','=','UT.UserTypeID')
    ->leftjoinSub($sub, 'UC', function($join){
        $join->on('UC.UserID', '=', 'U.UserID');
    })
    ->leftjoin('Invoice AS P','P.ProformaID','=','UC.ProformaID')
    ->select(DB::raw('"U"."UserID","UserFullName",COALESCE(sum("UC"."CommissionValue"), 0) AS "CommissionValue"'))
    ->wherein('UT.UserTypeID',$UserType)
    ->groupby('U.UserID')
    ->orderby('U.UserID')
    ->get();
    $StartDate = $input['StartDate'];
    $EndDate = $input['EndDate'];
    for($i = 0;$i<count($result);$i++)
    {
        $UserID = $result[$i]->UserID;
        $commission = DB::table('UserCommission AS UC')
        ->leftjoin('Invoice AS P','UC.ProformaID','=','P.ProformaID')
        ->where('UserID','=',$UserID)
        ->where('UC.Date','>=',$StartDate)
        ->where('UC.Date','<=',$EndDate)
        ->select(['UserID','UC.ProformaID','CommissionValue','CommissionPercentage','QuotationID','UC.BranchID','LicenseID','ServiceID'])
        ->get();
        $result[$i]->LicenseAmount = 0;
        $result[$i]->ServiceAmount = 0;
        $result[$i]->License = array();
        $result[$i]->Service = array();
        for($j = 0;$j<count($commission);$j++)
        {
            $license = DB::table('QuotationDetailLicense')
           ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
           ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
           ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
           ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
           ->select(DB::raw('"SubTotal","QuotationDetail"."BranchID","LicenseName"'))
           ->where('Quotation.QuotationID', $commission[$j]->QuotationID)
           ->where('QuotationDetail.BranchID',$commission[$j]->BranchID)
           ->where('QuotationDetailLicense.LicenseID',$commission[$j]->LicenseID)
           ->first();
           $license2[$j] = $license;
           if($license != null){
               $result[$i]->LicenseAmount += $license->SubTotal;
               $result[$i]->License[$j] = $license;
           }
           else{
               $result[$i]->LicenseAmount += 0;
           }


           //license quantity maksud nya itu total penualan license dalam harga, service juga sama.
           $service = DB::table('QuotationDetailService')
          ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
          ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
          ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
          ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
          ->select(DB::raw('"SubTotal","QuotationDetail"."BranchID","ServiceName"'))
          ->where('Quotation.QuotationID', $commission[$j]->QuotationID)
          ->where('QuotationDetail.BranchID','=',$commission[$j]->BranchID)
          ->where('QuotationDetailService.ServiceID',$commission[$j]->ServiceID)
          ->first();
          $service2[$j] = $service;
          if($service != null)
          {
               $result[$i]->ServiceAmount += $service->SubTotal;
               $result[$i]->Service[$j] = $service;
          }
          else{
              $result[$i]->ServiceAmount += 0;
          }

        }

    }

  $endresult = array(
      'Status' => 0,
      'Errors' => array(),
      'Message' => "Success",
      'Sales' => $result
  );
  return Response()->json($endresult);
}

public function getSalesCommissionDetail(Request $request){
  $input = json_decode($request->getContent(),true);
  $rules = [
      'UserID' => 'required',
      'EndDate' => 'required|date',
      'StartDate' => 'required|date'
  ];

  $validator = Validator::make($input, $rules);
  if ($validator->fails()) {
      $errors = $validator->errors();
      $errorList = $this->checkErrors($rules, $errors);
      $additional = null;
      $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
      return response()->json($response);
  }
  $UserID = $input['UserID'];
  $StartDate = $input['StartDate'];
  $EndDate = $input['EndDate'];


  // $result = DB::select('select "P"."ProformaID","InvoiceNO","UC"."LicenseID","LicenseName","UC"."ServiceID","ServiceName","P"."QuotationID","P"."PaidDate","QDL"."Quantity","QDL"."SubTotal",
  //                       "UC"."CommissionPercentage","UC"."CommissionValue","Q"."BrandID","BrandName","QD"."BranchID","BranchName","P"."Note"
  //                       from "UserCommission" AS "UC"
  //                       left join "User" AS "U" on "UC"."UserID" = "U"."UserID"
  //                       left join "Invoice" AS "P" on "P"."ProformaID" = "UC"."ProformaID"
  //                       left join "License" AS "L" on "L"."LicenseID" = "UC"."LicenseID"
  //                       left join "Service" AS "S" on "S"."ServiceID" = "UC"."ServiceID"
  //                       left join "Quotation" AS "Q" on "Q"."QuotationID" = "P"."QuotationID"
  //                       left join "Brand" AS "B" on "Q"."BrandID" = "B"."BrandID"
  //                       left join "QuotationDetail" AS "QD" on "Q"."QuotationID" = "QD"."QuotationID"
  //                       left join "QuotationDetailLicense" AS "QDL" on "QDL"."QuotationDetailID" = "QD"."QuotationDetailID"
  //                       left join "Branch" AS "Br" on "QD"."BranchID" = "Br"."BranchID"
  //                       where "U"."UserID" = '.$UserID.' and "UC"."Date" >= \''.$StartDate.'\' and "UC"."Date" <=\''.$EndDate.'\'');

                        // $result = DB::table('UserCommission AS UC')
                        // ->leftjoin('User AS U','UC.UserID','=','U.UserID')
                        // ->leftjoin('Invoice AS P','P.ProformaID','=','UC.ProformaID')
                        // ->leftjoin('License AS L','L.LicenseID','=','UC.LicenseID')
                        // ->leftjoin('Service AS S','S.ServiceID','=','UC.ServiceID')
                        // ->leftjoin('Quotation AS Q','Q.QuotationID','=','P.QuotationID')
                        // ->leftjoin('Brand AS B','Q.BrandID','=','B.BrandID')
                        // ->leftjoin('QuotationDetail AS QD','QD.QuotationID','=','Q.QuotationID')
                        // ->leftjoin('Branch AS Br','QD.BranchID','=','Br.BranchID')
                        // ->leftjoin('QuotationDetailLicense AS QDL','QD.QuotationDetailID','=','QDL.QuotationDetailID')
                        // ->leftjoin('QuotationDetailService AS QDS','QD.QuotationDetailID','=','QDS.QuotationDetailID')
                        // ->select(['P.ProformaID','InvoiceNO','UC.LicenseID','LicenseName','UC.ServiceID','ServiceName','P.QuotationID','P.PaidDate','QDL.Quantity','QDL.SubTotal',
                        //           'UC.CommissionPercentage','UC.CommissionValue','Q.BrandID','BrandName','QD.BranchID','BranchName','P.Note'])
                        // ->where('U.UserID','=',$UserID)
                        // ->where('UC.Date','>=',$StartDate)
                        // ->where('UC.Date','<=',$EndDate)
                        // ->get();

                        $result = DB::table('UserCommission AS UC')
                        ->leftjoin('Invoice AS P','UC.ProformaID','=','P.ProformaID')
                        ->leftjoin('Branch AS BR','UC.BranchID','=','BR.BranchID')
                        ->leftjoin('License AS L','UC.LicenseID','=','L.LicenseID')
                        ->leftjoin('Service AS S','UC.ServiceID','=','S.ServiceID')
                        ->where('UserID','=',$UserID)
                        ->where('UC.Date','>=',$StartDate)
                        ->where('UC.Date','<=',$EndDate)
                        ->select(['UserID','UC.ProformaID','CommissionValue','UC.CommissionPercentage','QuotationID','UC.BranchID','InvoiceNO','UC.BranchID','BranchName','UC.LicenseID',
                                  'LicenseName','PaidDate','Note','UC.ServiceID','ServiceName'])
                        ->get();

                        for($i = 0;$i<count($result);$i++)
                        {
                            if($result[$i]->LicenseID != null)
                            {
                                $license = DB::table('QuotationDetailLicense')
                               ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
                               ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                               ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
                               ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                               ->select(DB::raw('"SubTotal","LicenseName","QuotationDetailLicense"."Quantity"'))
                               ->where('Quotation.QuotationID', $result[$i]->QuotationID)
                               ->where('QuotationDetail.BranchID','=',$result[$i]->BranchID)
                               ->where('QuotationDetailLicense.LicenseID','=',$result[$i]->LicenseID)
                               ->first();
                                $result[$i]->SubTotal = $license->SubTotal;
                                $result[$i]->Quantity = $license->Quantity;
                            }
                            else{
                                $service = DB::table('QuotationDetailService')
                               ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
                               ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                               ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
                               ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                               ->select(DB::raw('"QuotationDetailService"."SubTotal","ServiceName","QuotationDetailService"."Quantity"'))
                               ->where('Quotation.QuotationID', $result[$i]->QuotationID)
                               ->where('QuotationDetail.BranchID','=',$result[$i]->BranchID)
                               ->where('QuotationDetailService.ServiceID','=',$result[$i]->ServiceID)
                               ->first();
                                $result[$i]->SubTotal = $service->SubTotal;
                                $result[$i]->Quantity = $service->Quantity;
                            }


                        }

  $endresult = array(
      'Status' => 0,
      'Errors' => array(),
      'Message' => "Success",
      'SalesCommissionDetail' => $result
  );
  return Response()->json($endresult);
}

public function getBranchLicense(Request $request){
  $input = json_decode($request->getContent(),true);
  $rules = [
      'isGetAll' => 'required'
  ];

  $validator = Validator::make($input, $rules);
  if ($validator->fails()) {
      $errors = $validator->errors();
      $errorList = $this->checkErrors($rules, $errors);
      $additional = null;
      $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
      return response()->json($response);
  }
$now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
$UnexpiredLicensePeriod = DB::table('QuotationDetailLicense AS QDL')
->leftjoin('License AS L','QDL.LicenseID','=','L.LicenseID')
->leftjoin('QuotationDetail AS QD','QD.QuotationDetailID','=','QDL.QuotationDetailID')
->leftjoin('Branch AS Br','Br.BranchID','=','QD.BranchID')
->leftjoin('Quotation AS Q','Q.QuotationID','=','QD.QuotationID')
->leftjoin('Brand AS B','Q.BrandID','=','B.BrandID')
->select(['QD.BranchID','BranchName','Q.BrandID','BrandName','QDL.LicenseID','LicenseName','StartDate','EndDate'])
->where('EndDate','>=',$now)
->orderby('EndDate')
->get();

$ExpiredLicensePeriod = DB::table('QuotationDetailLicense AS QDL')
->leftjoin('License AS L','QDL.LicenseID','=','L.LicenseID')
->leftjoin('QuotationDetail AS QD','QD.QuotationDetailID','=','QDL.QuotationDetailID')
->leftjoin('Branch AS Br','Br.BranchID','=','QD.BranchID')
->leftjoin('Quotation AS Q','Q.QuotationID','=','QD.QuotationID')
->leftjoin('Brand AS B','Q.BrandID','=','B.BrandID')
->select(['QD.BranchID','BranchName','Q.BrandID','BrandName','QDL.LicenseID','LicenseName','StartDate','EndDate'])
->where('EndDate','<=',$now)
->orderby('EndDate')
->get();

  $endresult = array(
      'Status' => 0,
      'Errors' => array(),
      'Message' => "Success",
      'UnexpiredLicenseList' => $UnexpiredLicensePeriod,
      'ExpiredLicenseList' => $ExpiredLicensePeriod
  );
  return Response()->json($endresult);
}

public function getTransactionQuery(Request $request){
  $input = json_decode($request->getContent(),true);
  $rules = [
      'Query' => 'nullable',
      'Column' => 'required',
      'Sorting' => 'required',
      'StartDate' => 'required|date',
      'EndDate' => 'required|date'
  ];

  $validator = Validator::make($input, $rules);
  if ($validator->fails()) {
      $errors = $validator->errors();
      $errorList = $this->checkErrors($rules, $errors);
      $additional = null;
      $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
      return response()->json($response);
  }
 $string = @$input['Query'];
 $column = @$input['Column'];
 $sorting = @$input['Sorting'];
 $StartDate = @$input['StartDate'];
 $EndDate = @$input['EndDate'];

if($string === null){$result = DB::table('Quotation')
->leftjoin('Invoice', 'Invoice.QuotationID','=','Quotation.QuotationID')
->leftjoin('SalesVisitation', 'SalesVisitation.SalesVisitationID','=', 'Quotation.SalesVisitationID')
->leftjoin('User', 'SalesVisitation.UserID' ,'=','User.UserID')
->leftjoin('Branch','SalesVisitation.BranchID','=','Branch.BranchID')
->leftjoin('UserType','User.UserTypeID','=','UserType.UserTypeID')
->select(['InvoiceNO','GrandTotal','Commission','GrandTotalReseller','PaidDate','UserFullName',
          'UserTypeName','BranchName','User.UserID','PIC','PassedPIC'])
->where('Invoice.Status',"Paid")
->where('Invoice.PaidDate','>=',$StartDate)
->where('Invoice.PaidDate','<=', $EndDate)
->orderby($column,$sorting)
->get();
}
else{
    $result = DB::table('Quotation')
    ->leftjoin('Invoice', 'Invoice.QuotationID','=','Quotation.QuotationID')
    ->leftjoin('SalesVisitation', 'SalesVisitation.SalesVisitationID','=', 'Quotation.SalesVisitationID')
    ->leftjoin('User', 'SalesVisitation.UserID' ,'=','User.UserID')
    ->leftjoin('Branch','SalesVisitation.BranchID','=','Branch.BranchID')
    ->leftjoin('UserType','User.UserTypeID','=','UserType.UserTypeID')
    ->select(['InvoiceNO','GrandTotal','Commission','GrandTotalReseller','PaidDate','UserFullName',
              'UserTypeName','BranchName','User.UserID','PIC','PassedPIC'])
    ->where('Invoice.Status',"Paid")
    ->where('Invoice.PaidDate','>=',$StartDate)
    ->where('Invoice.PaidDate','<=', $EndDate)
    ->whereRaw('"UserFullName" like \'%'.$string.'%\' or "UserTypeName" like \'%'.$string.'%\' or "BranchName" like \'%'.$string.'%\'
    or "InvoiceNO" like \'$'.$string.'$\' ' )
    ->orderby($column,$sorting)
    ->get();
}


  $endresult = array(
      'Status' => 0,
      'Errors' => array(),
      'Message' => "Success",
      'Transaction' => $result
  );
  return Response()->json($endresult);
}



public function InsertUpdateBrand(Request $request){
    $input = json_decode($request->getContent(),true);
    $rules = [
        'BrandCode' => 'required',
        'BrandName' => 'required',
        'ProductID' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }

    $param = array(
        'BrandCode' => $input['BrandCode'],
        'BrandName' => $input['BrandName'],
        'Image' => @$input['Image'],
        'ProductID' => @$input['ProductID']);

      $ID = @$input["BrandID"];
      if ($ID == null){$result = DB::table('Brand')->insert($param);}
      else {$result = DB::table('Brand')->where('BrandID', $ID)->update($param);}

    $result = $this->checkReturn($result);
    return Response()->json($result);

  }

  public function DeleteBrand(Request $request){
       $input = json_decode($this->request->getContent(),true);
       $rules = [
         'BrandID' => 'required'
       ];

       $validator = Validator::make($input, $rules);
       if ($validator->fails()) {
           $errors = $validator->errors();
           $errorList = $this->checkErrors($rules, $errors);
           $additional = null;
           $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
           return response()->json($response);
       }
       $BrandID = @$input['BrandID'];
       $param = array('Status' => 'D','Archived' => now());
       $result = DB::table('Brand')->where('BrandID', $BrandID)->update($param);

      $result = $this->checkReturn($result);

      return Response()->json($result);

  }


}
