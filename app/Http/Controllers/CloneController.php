<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;
use Mail;
use App\Mail\SendInvoice;

class CloneController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
        // $link = $request->url();
        // $Username = DB::table('User')
        // ->where('UserID',$this->param->UserID)
        // ->first()->Username;
        // $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
        // $log = DB::table('LogActivity')
        // ->insert(array('UserID' => $this->param->UserID, 'Activity' => $link,
        // 'Parameter' => $request->getContent(), 'Time' => $now,'Username' => $Username));
    }

public function cloneDeleteData(request $request){

    $DBRetail = DB::connection('pgsqlretaildev');
    $DBFnB = DB::connection('pgsqlfnbdev');
    $DBSalon = DB::connection('pgsqlsalondev');
    $DBLog = DB::connection('mongoadmindev');

    $DBFnB->select('insert into "DeleteData" ("BranchID","ProductID","RestaurantID","StoreID","BrandID","TransactionSalesDate","ScheduleTime")
    select "BranchID","ProductID","RestaurantID","StoreID","BrandID","TransactionSalesDate","ScheduleTime"
    from "DeleteData" where "BranchID" = 1 and "RestaurantID" = 1 limit 1');

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Data' => $TicketStatus
      );
       return Response()->json($endresult);
    }

    public function getReportType(request $request){


        $ReportType = DB::table('ReportType')
        ->get();

          $endresult = array(
              'Status' => 0,
              'Errors' => array(),
              'Message' => "Success",
              'Data' => $ReportType
          );
           return Response()->json($endresult);
        }

public function getTicketing(Request $request){
    $input = json_decode($request->getContent(),true);
    $rules = [
        'StartDate' => 'required|date',
        'EndDate' => 'required|date',
        'Status' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }
    $result = DB::table('Ticket')
    ->leftjoin('TicketStatus','Ticket.TicketStatusID','=','TicketStatus.TicketStatusID')
    ->leftjoin('ReportType','Ticket.ReportTypeID','=','ReportType.ReportTypeID')
    ->leftjoin('User AS U','Ticket.ReporterID','=','U.UserID')
    ->leftjoin('Branch AS Br','Br.BranchID','=','Ticket.BranchID')
    ->leftjoin('Brand AS B','B.BrandID','=','Br.BrandID')
    ->leftjoin('Product AS P','P.ProductID','=','B.ProductID')
    ->where('Date','>=',$input['StartDate'])
    ->where('Date','<=',$input['EndDate'])
    ->where('Ticket.TicketStatusID',$input['Status'])
    ->get();
    if($input['Status'] == 0)
    {
        $result = DB::table('Ticket')
        ->leftjoin('TicketStatus','Ticket.TicketStatusID','=','TicketStatus.TicketStatusID')
        ->leftjoin('ReportType','Ticket.ReportTypeID','=','ReportType.ReportTypeID')
        ->leftjoin('User AS U','Ticket.ReporterID','=','U.UserID')
        ->leftjoin('Branch AS Br','Br.BranchID','=','Ticket.BranchID')
        ->leftjoin('Brand AS B','B.BrandID','=','Br.BrandID')
        ->leftjoin('Product AS P','P.ProductID','=','B.ProductID')
        ->where('Date','>=',$input['StartDate'])
        ->where('Date','<=',$input['EndDate'])
        ->get();
    }
       $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Data' => $result
      );


return Response()->json($endresult);

}

public function getTicketingDetail(Request $request){

    $input = json_decode($request->getContent(),true);
    $rules = [
        'TicketID' => 'required'
    ];

    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $errors = $validator->errors();
        $errorList = $this->checkErrors($rules, $errors);
        $additional = null;
        $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
        return response()->json($response);
    }

    $ID = $input['TicketID'];
    $ticket = DB::table('Ticket')
    ->leftjoin('TicketStatus','Ticket.TicketStatusID','=','TicketStatus.TicketStatusID')
    ->leftjoin('ReportType','Ticket.ReportTypeID','=','ReportType.ReportTypeID')
    ->where('TicketID',$ID)
    ->get();
    $images = DB::table('TicketImage')
    ->where('TicketID',$ID)
    ->wherenull('Archived')
    ->get();
    $resolver = DB::table('TicketResolver')
    ->where('TicketID',$ID)
    ->wherenull('Archived')
    ->get();

    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'Data' => array(
            'Header' => $ticket,
            'Images' => $images,
            'Resolver' => $resolver
        )
    );

return Response()->json($endresult);

}

public function getReporterUser(Request $request){
    $UserType = DB::table('UserTypePermission')
    ->where('PermissionID',"IncludeInGetReporter")
    ->select(['UserTypeID'])
    ->get();
    for($i = 0; $i<count($UserType);$i++)
    {
        $UserTypeArray[$i] = $UserType[$i]->UserTypeID;
    }

    $User = DB::table('User')
    ->wherein('UserTypeID',$UserTypeArray)
    ->get();

    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'Data' => $User
    );

return Response()->json($endresult);

}

public function getResolverUser(Request $request){
    $UserType = DB::table('UserTypePermission')
    ->where('PermissionID',"IncludeInGetResolver")
    ->select(['UserTypeID'])
    ->get();
    for($i = 0; $i<count($UserType);$i++)
    {
        $UserTypeArray[$i] = $UserType[$i]->UserTypeID;
    }

    $User = DB::table('User')
    ->wherein('UserTypeID',$UserTypeArray)
    ->get();

    $endresult = array(
        'Status' => 0,
        'Errors' => array(),
        'Message' => "Success",
        'Data' => $User
    );

return Response()->json($endresult);

}

      public function getTicketDetailbyTicketID(Request $request){

          $input = json_decode($request->getContent(),true);
          $rules = [
              'TicketID' => 'required'
          ];

          $validator = Validator::make($input, $rules);
          if ($validator->fails()) {
              $errors = $validator->errors();
              $errorList = $this->checkErrors($rules, $errors);
              $additional = null;
              $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
              return response()->json($response);
          }
          $UserID = $this->param->UserID;
          $TicketID = @$input['TicketID'];


              $result = DB::table('TicketDetail AS TD')
              ->leftjoin('Ticket AS T','TD.TicketID','=','T.TicketID')
              ->leftjoin('Branch AS Br','Br.BranchID','=','T.BranchID')
              ->leftjoin('TicketStatus AS Ts','Ts.TicketStatusID','=','TD.TicketStatusID')
              ->get();
          // return $result;
//cek isi data branch untuk brand tertentu ada atau tidak.

          $endresult = array(
              'Status' => 0,
              'Errors' => array(),
              'Message' => "Success",
              'Data' => $result
          );

return Response()->json($endresult);

    }


    public function InsertUpdateTicket(Request $request){
       $input = json_decode($request->getContent(), true);
       $rules = [
         'BranchID' => 'required',
         'CustomerBranchPhone' => 'required|numeric',
         'CustomerBranchFullName' => 'required',
         'ReportTypeID' => 'required',
         'TicketStatusID' =>'required'
       ];

       $validator = Validator::make($input, $rules);
       if ($validator->fails()) {
           $errors = $validator->errors();
           $errorList = $this->checkErrors($rules, $errors);
           $additional = null;
           $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
           return response()->json($response);
       }
       $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
       $ID = @$input['TicketID'];
       $UserID = $this->param->UserID;
       $idate = @$input['Date'];
       if($idate === null)
       {
           $year = date('Y');
           $month = date('m');
           $date = date('d');
           $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
           $idate = $now;
           $time = $now;
       }
       else{
           $year = substr($idate,0,4);
           $month = substr($idate,5,2);
           $date = substr($idate,8,2);
           $time = $input['Times'];
           $idate = @$input['Date'];
       }



       $num = DB::table('Ticket')
       ->whereraw('extract(month from "Date") ='.$month.' and extract(year from "Date") ='.$year)
       ->max('TicketNO');
       $year = substr($year,2,2);
       $num = substr($num,4,3);
       $num = $num+1;
       $num = str_pad($num, 3, '0', STR_PAD_LEFT);
       $ticketNO = $year.$month.$num.$date;

       $param = array (
         'BranchID' => $input['BranchID'],
         'Date' => $idate,
         'BrandID' => $input['BrandID'],
         'ProductID' => $input['ProductID'],
         'Summary' => $input['Summary'],
         'Description' => $input['Description'],
         'CustomerBranchPhone' => $input['CustomerBranchPhone'],
         'CustomerBranchFullName' => $input['CustomerBranchFullName'],
         'ReporterID' => $input['ReporterID'],
         'ReportTypeID' => $input['ReportTypeID'],
         'TicketStatusID' => @$input['TicketStatusID'],
         'TicketNO' => $ticketNO,
         'Time' => $time
       );
       $param2 = array (
           'BranchID' => $input['BranchID'],
           'Date' => $idate,
           'BrandID' => $input['BrandID'],
           'ProductID' => $input['ProductID'],
           'Summary' => $input['Summary'],
           'Description' => $input['Description'],
           'CustomerBranchPhone' => $input['CustomerBranchPhone'],
           'CustomerBranchFullName' => $input['CustomerBranchFullName'],
           'ReporterID' => $input['ReporterID'],
           'ReportTypeID' => $input['ReportTypeID'],
           'TicketStatusID' => @$input['TicketStatusID'],
           'Time' => $time
       );
       if(@$input['VisitDate'] != null)
       {
        $param['VisitDate'] = @$input['VisitDate'];
        $param['VisitTimeFrom'] = @$input['VisitTimesFrom'];
        $param['VisitTimeTo'] = @$input['VisitTimesTo'];
        $param['VisitFor'] = @$input['VisitFor'];
        $param2['VisitDate'] = @$input['VisitDate'];
        $param2['VisitTimeFrom'] = @$input['VisitTimesFrom'];
        $param2['VisitTimeTo'] = @$input['VisitTimesTo'];
        $param2['VisitFor'] = @$input['VisitFor'];
       }

       if(@$input['ResolutionSummary'] != null)
       {
           if(@$input['ResolutionDate'] == null)
           {
               $param['ResolutionDate'] = $now;
               $param['ResolutionTime'] = $now;
               $param2['ResolutionDate'] = $now;
               $param2['ResolutionTime'] = $now;
           }
           else{
               $param['ResolutionDate'] = @$input['ResolutionDate'];
               $param['ResolutionTime'] = @$input['ResolutionTimes'];
               $param2['ResolutionDate'] = @$input['ResolutionDate'];
               $param2['ResolutionTime'] = @$input['ResolutionTimes'];

           }

        $param['ResolutionSummary'] = @$input['ResolutionSummary'];
        $param['ResolutionDescription'] = @$input['ResolutionDescription'];
        $param2['ResolutionSummary'] = @$input['ResolutionSummary'];
        $param2['ResolutionDescription'] = @$input['ResolutionDescription'];
       }

       if($ID == null)
       {
       $result = DB::table('Ticket')->insert($param);
       $ID = $this->getLastVal();
       }
       else {
         $result = DB::table('Ticket')->where('TicketID', $ID)->update($param2);
            }
            $ResolverID = @$input['ResolverID'];
        if($ResolverID != null)
        {
            for($i=0;$i<count($ResolverID);$i++)
            {
                $result = DB::table('TicketResolver')
                ->insert(array('UserID' => $ResolverID[$i], 'TicketID' => $ID));
            }
        }
            if(@$input['Images'] !== null){
                $images = @$input['Images'];

                    for($i=0;$i<count($images);$i++)
                    {
                        if(@$images[$i]['ImageID'] === null)
                        {
                        $arr = array(
                            'UserID' => $this->param->UserID,
                            'ObjectID' => $ID,
                            'Folder' => 'Ticket',
                            'Filename' => $images[$i]['Filename'],
                            'Data' =>  $images[$i]['Data']
                        );
                        $path = $this->upload_to_s3($arr);
                        $param = array(
                            'Image' => $path,
                            'TicketID' => $ID,
                            'Caption' => $images[$i]['Caption']
                        );
                        $result = DB::table('TicketImage')
                        ->insert($param);
                        }
                        else{
                            $ID = $images[$i]['ImageID'];
                            if(@$images[$i]['Data'] != null)
                            {
                                $arr = array(
                                    'UserID' => $this->param->UserID,
                                    'ObjectID' => $ID,
                                    'Folder' => 'Ticket',
                                    'Filename' => $images[$i]['Filename'],
                                    'Data' =>  $images[$i]['Data']
                                );
                                $path = $this->upload_to_s3($arr);
                                $param = array(
                                    'Image' => $path,
                                    'Caption' => $images[$i]['Caption']
                                );
                                $result = DB::table('TicketImage')
                                ->where('TicketImageID',$ID)
                                ->update($param);
                            }
                            else{
                                $param = array(
                                    'Caption' => $images[$i]['Caption']
                                );
                                $result = DB::table('TicketImage')
                                ->where('TicketImageID',$ID)
                                ->update($param);
                            }
                            $response = $this->generateResponse(0, [], "Success", ['Ticketing'=>$result]);
                        }
                }


                    // return(@$input['Images']['Data']);
                    // die();
                }

                if(@$input['ImageDelete'] !== null){
                    $images = $input['ImageDelete'];
                    for($i=0;$i<count($images);$i++)
                    {
                        $param = array('Archived' => $now);
                        $result = DB::table('TicketImage')
                        ->where('TicketImageID',$images[$i])
                        ->update($param);

                        $response = $this->generateResponse(0, [], "Success", ['Ticketing'=>$result]);
                    }
                    }



    $result = $this->checkReturn($result);
    $result['TicketNO'] = $ID;
    return Response()->json($result);
  }


    public function DeleteBranch(Request $request){
         $input = json_decode($this->request->getContent(),true);
         $rules = [
           'BranchID' => 'required'
         ];
         $validator = Validator::make($input, $rules);
         if ($validator->fails()) {
             $errors = $validator->errors();
             $errorList = $this->checkErrors($rules, $errors);
             $additional = null;
             $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
             return response()->json($response);
         }
         $BranchID = $input['BranchID'];
         $param = array('Status' => 'D','Archived' => now());
         $result = DB::table('Branch')->where('BranchID', $BranchID)->update($param);


        $result = $this->checkReturn($result);

        return Response()->json($result);

    }
}
