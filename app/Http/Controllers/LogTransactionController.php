<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Validator;
use PDF;

class LogTransactionController extends Controller
{
    public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
        $link = $request->url();
        $Username = DB::table('User')
        ->where('UserID',$this->param->UserID)
        ->first()->Username;
        $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
        $log = DB::table('LogActivity')
        ->insert(array('UserID' => $this->param->UserID, 'Activity' => $link,
        'Parameter' => $request->getContent(), 'Time' => $now,'Username' => $Username));
    }

    public function getPaginationLogTransaction (request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
            'StartDate' => 'required',
            'EndDate' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');
        $dtime = strtotime($input['StartDate']);
        $StartDate = date('Y-m-d 00:00:00',$dtime);
        $dtime = strtotime($input['EndDate']);
        $EndDate = date('Y-m-d 23:59:59',$dtime);
        $BillNumber = @$input['BillNumber'];
        $BranchID = @$input['BranchID'];
        $RowCount = @$input['RowCount'];
        if(@$input['RestaurantID'] != null)
        {
            $ProductID = "RES";
        }
        elseif(@$input['StoreID']!= null)
        {
            $ProductID = "RET";
        }
        else{
            $ProductID = "SER";
        }

        if($BillNumber == null)
        {
            $LogTransaction = $DBTLog->table('DeviceRequestAudit')
            ->where('Date','>=',$StartDate)
            ->where('Date','<=',$EndDate)
            ->where('Product',$ProductID)
            ->where('TokenDetail.BranchID',$BranchID)
            ->where('Method','like','%input_order')
            ->orderby('Date','Desc')
            ->count();

        }
        else{
            $LogTransaction = $DBTLog->table('DeviceRequestAudit')
            ->where('Date','>=',$StartDate)
            ->where('Date','<=',$EndDate)
            ->where('Product',$ProductID)
            ->where('TokenDetail.BranchID',$BranchID)
            ->where('Method','like','%input_order')
            ->where('RequestData.Order.Billing.OrderBillNumber',$BillNumber)
            ->orderby('Date','Desc')
            ->count();
        }

        //input order buat $FnB
        //input sales buat salon sama retail
        $page = $LogTransaction/$RowCount;
        $page = ceil($page);

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'TotalPage' => $page
      );

      return Response()->json($endresult);

    }

    public function getBranch(Request $request){
        $input = json_decode($this->request->getContent(),true);

        $String = @$input['String'];
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        //sementara salon nya di hide.
        if(count($String)==0)
        {
            $endresult = array(
                'Status' => 0,
                'Errors' => array(),
                'Message' => "Success",
                'Branch' => 0
            );
            return Response()->json($endresult);
        }
        elseif($String == "*"){
            $FnBBrand = $DBFnB->table('Branch AS BR')
            ->leftjoin('RestaurantMaster AS RM','BR.RestaurantID','=','RM.RestaurantID')
            ->select(['BR.RestaurantID','RestaurantName','Owner','BranchID','BranchName'])
            ->where('BranchID','>',0)
            ->get()->toArray();

            $RetailBrand = $DBRetail->table('Branch AS BR')
            ->leftjoin('StoreMaster AS SM','BR.StoreID','=','SM.StoreID')
            ->select(['BR.StoreID','StoreName','Owner','BranchID','BranchName'])
            ->where('BranchID','>',0)
            ->get()->toArray();

            // $SalonBrand = $DBSalon->table('Branch AS BR')
            // ->leftjoin('BrandMaster AS BM','BR.BrandID','=','BM.BrandID')
            // ->select(['BR.BrandID','BrandName','Owner','BranchID','BranchName'])
            // ->where('BranchID','>',0)
            // ->get()->toArray();
        }
        else{
            $FnBBrand = $DBFnB->table('Branch AS BR')
            ->leftjoin('RestaurantMaster AS RM','BR.RestaurantID','=','RM.RestaurantID')
            ->select(['BR.RestaurantID','RestaurantName','Owner','BranchID','BranchName'])
            ->where('BranchID','>',0)
            ->where(function($query) use ($String){
                for($i=0;$i<count($String);$i++)
                {
                    $query->orwhereraw('lower("RestaurantName") like \'%'.strtolower($String[$i]).'%\'');
                }
            })
            ->orwhere(function($query1) use ($String){
                for($i=0;$i<count($String);$i++)
                {
                    $query1->orwhereraw('lower("BranchName") like \'%'.strtolower($String[$i]).'%\'');
                }
            })
            ->get()->toArray();

            $RetailBrand = $DBRetail->table('Branch AS BR')
            ->leftjoin('StoreMaster AS SM','BR.StoreID','=','SM.StoreID')
            ->select(['BR.StoreID','StoreName','Owner','BranchID','BranchName'])
            ->where('BranchID','>',0)
            ->where(function($query) use ($String){
                for($i=0;$i<count($String);$i++)
                {
                    $query->orwhereraw('lower("StoreName") like \'%'.strtolower($String[$i]).'%\'');
                }
            })
            ->orwhere(function($query1) use ($String){
                for($i=0;$i<count($String);$i++)
                {
                    $query1->orwhereraw('lower("BranchName") like \'%'.strtolower($String[$i]).'%\'');
                }
            })
            ->get()->toArray();
            //
            // $SalonBrand = $DBSalon->table('Branch AS BR')
            // ->leftjoin('BrandMaster AS BM','BR.BrandID','=','BM.BrandID')
            // ->select(['BR.BrandID','BrandName','Owner','BranchID','BranchName'])
            // ->where('BranchID','>',0)
            // ->where(function($query) use ($String){
            //     for($i=0;$i<count($String);$i++)
            //     {
            //         $query->orwhereraw('lower("BrandName") like \'%'.strtolower($String[$i]).'%\'');
            //     }
            // })
            // ->orwhere(function($query1) use ($String){
            //     for($i=0;$i<count($String);$i++)
            //     {
            //         $query1->orwhereraw('lower("BranchName") like \'%'.strtolower($String[$i]).'%\'');
            //     }
            // })
            // ->get()->toArray();

        }
        $Branch = array_merge($FnBBrand,$RetailBrand);

        $endresult = array(
            'Status' => 0,
            'Errors' => array(),
            'Message' => "Success",
            'Branch' => $Branch
        );
        return Response()->json($endresult);



    }


    public function getLogTransaction(request $request){
        $input = json_decode($this->request->getContent(),true);
        $rules = [
            'metaData' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $DBRetail = DB::connection('pgsqlretail');
        $DBFnB = DB::connection('pgsqlfnb');
        $DBSalon = DB::connection('pgsqlsalon');
        $DBLog = DB::connection('mongoadmin');
        $DBTLog = DB::connection('mongotransaction');
        $MetaData = @$input['metaData'];
        $dtime = strtotime($MetaData['StartDate']);
        $StartDate = date('Y-m-d 00:00:00',$dtime);
        $dtime = strtotime($MetaData['EndDate']);
        $EndDate = date('Y-m-d 23:59:59',$dtime);
        $BillNumber = @$MetaData['BillNO'];
        $BranchID = @$MetaData['BranchID'];
        $StartFrom = @$input['StartFrom'];
        $PageSize = @$input['PageSize'];
        $OrderDirection = $input['OrderDirection'];
        $OrderBy = $input['OrderBy'];

        if($MetaData['ProductID'] == "RES")
        {
            $ProductID = "RES";
            $Method = 'input_order';
            $Column = array(
                'RequestData.Order.Billing.OrderBillNumber',
                'RequestData.Order.Billing.Date',
                'RequestData.Order.Billing.TotalBilling'
            );
            if($BillNumber != null)
            {
                $OrderBillColumn = 'RequestData.Order.Billing.OrderBillNumber';
            }
        }
        elseif($MetaData['ProductID'] == "RET")
        {
            $ProductID = "RET";
            $Method = 'input_sales';
            $Column = array(
                'RequestData.SalesTransactionNumber',
                'RequestData.Date',
                'RequestData.TotalSalesTransaction'
            );
            if($BillNumber != null)
            {
                $OrderBillColumn = 'RequestData.SalesTransactionNumber';
            }
        }
        else{
            $ProductID = "SER";
            $Method = 'input_sales';
            $endresult = array(
                'Status' => 1,
                'Errors' => array(
                    'ID' => 'Salon',
                    'Message' => 'There\'s No Log Transaction Yet for Salon.'
                ),
                'Message' => "Please choose FnB or Retail Branch."
            );
            return Response()->json($endresult);
        }
        if($PageSize == null)
        {$PageSize = 100;}
        $PageSize = (int)$PageSize;
        $StartFrom = (int)$StartFrom;

        if($BillNumber == null)
        {
            $LogTransaction = $DBTLog->table('DeviceRequestAudit')
            ->where('Date','>=',$StartDate)
            ->where('Date','<=',$EndDate)
            ->where('Product',$ProductID)
            ->where('TokenDetail.BranchID',$BranchID)
            ->where('Method','like','%'.$Method)
            ->limit($PageSize)
            ->skip($StartFrom)
            ->orderby($OrderBy,$OrderDirection)
            ->select($Column)
            ->get()->toArray();

            $DataCount = $DBTLog->table('DeviceRequestAudit')
            ->where('Date','>=',$StartDate)
            ->where('Date','<=',$EndDate)
            ->where('Product',$ProductID)
            ->where('TokenDetail.BranchID',$BranchID)
            ->where('Method','like','%'.$Method)
            ->orderby($OrderBy,$OrderDirection)
            ->count();

        }
        else{
            $LogTransaction = $DBTLog->table('DeviceRequestAudit')
            ->where('Date','>=',$StartDate)
            ->where('Date','<=',$EndDate)
            ->where('Product',$ProductID)
            ->where('TokenDetail.BranchID',$BranchID)
            ->where('Method','like','%'.$Method)
            ->where($OrderBillColumn,$BillNumber)
            ->select($Column)
            ->limit($PageSize)
            ->skip($StartFrom)
            ->orderby($OrderBy,$OrderDirection)
            ->get()->toArray();

            $DataCount = $DBTLog->table('DeviceRequestAudit')
            ->where('Date','>=',$StartDate)
            ->where('Date','<=',$EndDate)
            ->where('Product',$ProductID)
            ->where('TokenDetail.BranchID',$BranchID)
            ->where('Method','like','%'.$Method)
            ->where($OrderBillColumn,$BillNumber)
            ->orderby($OrderBy,$OrderDirection)
            ->count();
        }

        $MaxPage = $DataCount/$PageSize;
        $MaxPage = ceil($MaxPage);

        //input order buat $FnB
        //input sales buat salon sama retail

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'recordsFiltered' => $DataCount,
          'recordsTotal' => $DataCount,
          'LogTransaction' => $LogTransaction

      );

      return Response()->json($endresult);

    }
}
