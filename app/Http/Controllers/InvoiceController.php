<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Validator;

class InvoiceController extends Controller
{
        public function __construct(Request $request){
        $this->param = $this->checkToken($request);
        $this->request = $request;
        $link = $request->url();
        $Username = DB::table('User')
        ->where('UserID',$this->param->UserID)
        ->first()->Username;
        $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;
        $log = DB::table('LogActivity')
        ->insert(array('UserID' => $this->param->UserID, 'Activity' => $link,
        'Parameter' => $request->getContent(), 'Time' => $now,'Username' => $Username));
    }


    public function getInvoice(request $request){
        $input = json_decode($request->getContent(),true);
        $rules = [
            'isGetAll' => 'required'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }

        $UserID = $this->param->UserID;
        $isGetAll = $input['isGetAll'];
        // dd("A");
        $now = collect(\DB::select("Select timezone('Asia/Jakarta', now()) \"ServerTime\""))->first()->ServerTime;

        $result = DB::table('Invoice')
        ->whereNull('PaidDate')
        ->where('DueDate','<',$now)
        ->update(array('StatusID' => "E"));

        $status = DB::table('Invoice')
        ->where('StatusID',"E")
        ->select(['QuotationID'])
        ->get();
        if(count($status)>0)
        {
            for($i = 0; $i<count($status); $i++)
            {
                $statusnew[$i] = $status[$i]->QuotationID;
            }

            $result = DB::table('Quotation')
            ->whereIn('QuotationID',$statusnew)
            ->where('StatusID',"I")
            ->update(array('StatusID' => "RRI"));
        }


        $status2 = DB::table('Invoice')
        ->where('StatusID','<>',"E")
        ->select(['QuotationID'])
        ->get();

        if(count($status2)>0)
        {
            for($i = 0; $i<count($status2); $i++)
            {
                $statusnew2[$i] = $status2[$i]->QuotationID;
            }

            $result2 = DB::table('Quotation')
            ->whereIn('QuotationID',$statusnew2)
            ->where('StatusID',"RRI")
            ->update(array('StatusID' => "I"));
        }



        if($isGetAll === true){
            $query = DB::table('Invoice')
            ->leftjoin('Quotation', 'Quotation.QuotationID', '=','Invoice.QuotationID')
            ->leftjoin('Brand','Quotation.BrandID','=','Brand.BrandID')
            ->leftjoin('Status', 'Invoice.StatusID','=','Status.StatusID')
            ->leftjoin('Branch','Branch.BranchID','=','Invoice.BranchID')
            ->select(['InvoiceNO','InvoiceID','Quotation.QuotationID','Quotation.QuotationNO',
            'Invoice.GrandTotal','Invoice.StatusID','Quotation.BrandID','BrandName','Invoice.BranchID','BranchName',
            'StatusName','DeliveryDate','DueDate','PaidDate','Brand.Contact','Brand.Phone', 'Brand.Email','Brand.Address',
            'Invoice.GrandTotalDiscount','Invoice.URL','ProformaID','ProformaNO','ReceiptID','ReceiptNO','PONumber'])
            ->orderby('ProformaID','desc')
            ->where('Invoice.StatusID','<>',"E")
            ->limit(10)
            ->get();
            
            $user = DB::table('QuotationDetailUser AS QDU')
                ->leftjoin('User AS U','QDU.UserID','=','U.UserID')
                ->select(['QDU.UserID','UserFullName','Username','Email','Phone'])
                ->orderby('QuotationDetailUserID','asc')
                ->limit(10)
                ->get();
            
            $query2 = DB::table('Invoice')
            ->leftjoin('Quotation', 'Quotation.QuotationID', '=','Invoice.QuotationID')
            ->leftjoin('Brand','Quotation.BrandID','=','Brand.BrandID')
            ->leftjoin('Status', 'Invoice.StatusID','=','Status.StatusID')
            ->leftjoin('Branch','Branch.BranchID','=','Invoice.BranchID')
            ->leftjoin('QuotationDetailUser as QDU','QDU.QuotationID','=','Quotation.QuotationID')
            ->leftjoin('User as U','U.UserID','=','QDU.UserID')
            ->select(['InvoiceNO','InvoiceID','Quotation.QuotationID','Quotation.QuotationNO',
            'Invoice.GrandTotal','Invoice.StatusID','Quotation.BrandID','BrandName','Invoice.BranchID','BranchName',
            'StatusName','DeliveryDate','DueDate','PaidDate','Brand.Contact','Brand.Phone', 'Brand.Email','Brand.Address',
            'Invoice.GrandTotalDiscount','Invoice.URL','ProformaID','ProformaNO','ReceiptID','ReceiptNO','PONumber',
            'QDU.UserID','UserFullName','Username','U.Email as UserEmail','U.Phone as UserPhone'])
            ->orderby('ProformaID','desc')
            ->orderby('QuotationDetailUserID','asc')
            ->where('Invoice.StatusID','<>',"E");
            $result = $query2->get();
            $HeaderColumn = $this->getColumnName($query);
            $UserColumn = $this->getColumnName($user);
            // dd($UserColumn);
            $InvoiceList = [];
            foreach($result as $a){
                // dd($a);
                foreach($HeaderColumn as $b){
                    $InvoiceList[$a->ProformaID][$b] = $a->$b;
                }
                
                $InvoiceList[$a->ProformaID]['User'] = [];
            }
            // // dd($UserColumn);
            // foreach($result as $a){
            //         $InvoiceList[$a->ProformaID]['User'][$a->UserID]['UserID'] = $a->UserID;
            //         $InvoiceList[$a->ProformaID]['User'][$a->UserID]['Username'] = $a->Username;
            //         $InvoiceList[$a->ProformaID]['User'][$a->UserID]['UserFullName'] = $a->UserFullName;
            //         $InvoiceList[$a->ProformaID]['User'][$a->UserID]['Email'] = $a->UserEmail;
            //         $InvoiceList[$a->ProformaID]['User'][$a->UserID]['Phone'] = $a->UserPhone;
            // }

            foreach($result as $a){
                foreach($UserColumn as $b){
                    $InvoiceList[$a->ProformaID]['User'][$a->UserID][$b] = $a->$b;
                }
            }
            foreach($InvoiceList as &$a){
                $a['User'] = array_values($a['User']);
            }
            
            // dd($InvoiceList);
            // for($i=0;$i<count($result);$i++)
            // {
            //     $QuotationID = $result[$i]->QuotationID;
            //     $user = DB::table('QuotationDetailUser AS QDU')
            //     ->leftjoin('User AS U','QDU.UserID','=','U.UserID')
            //     ->select(['QDU.UserID','UserFullName','Username','Email','Phone'])
            //     ->where('QuotationID',$QuotationID)
            //     ->orderby('QuotationDetailUserID','asc')
            //     ->get();
            //     $result[$i]->User = $user;
            // }
            $result = array_values($InvoiceList);
        }
        else{
            $result = DB::table('Invoice')
            ->leftjoin('Quotation', 'Quotation.QuotationID', '=','Invoice.QuotationID')
            ->leftjoin('Brand','Quotation.BrandID','=','Brand.BrandID')
            ->leftjoin('Status', 'Invoice.StatusID','=','Status.StatusID')
            ->leftjoin('Branch','Branch.BranchID','=','Invoice.BranchID')
            ->leftjoin('QuotationDetailUser','QuotationDetailUser.QuotationID','=','Quotation.QuotationID')
            ->where('QuotationDetailUser.UserID',$UserID)
            ->distinct('InvoiceID')
            ->select(['InvoiceNO','InvoiceID','Quotation.QuotationID','Quotation.QuotationNO',
            'Invoice.GrandTotal','Invoice.StatusID','Quotation.BrandID','BrandName','Invoice.BranchID','BranchName',
            'StatusName','DeliveryDate','DueDate','PaidDate','Brand.Contact','Brand.Phone', 'Brand.Email','Brand.Address',
            'Invoice.GrandTotalDiscount','Invoice.URL','ProformaID','ProformaNO','ReceiptID','ReceiptNO','PONumber'])
            ->orderby('ProformaID','desc')
            ->where('Invoice.StatusID','<>',"E")
            ->get();
            for($i=0;$i<count($result);$i++)
            {
                $QuotationID = $result[$i]->QuotationID;
                $user = DB::table('QuotationDetailUser AS QDU')
                ->leftjoin('User AS U','QDU.UserID','=','U.UserID')
                ->select(['QDU.UserID','UserFullName','Username','Email','Phone'])
                ->where('QuotationID',$QuotationID)
                ->orderby('QuotationDetailUserID','asc')
                ->get();
                $result[$i]->User = $user;
            }
        }


      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Invoice' => $result
      );

       return Response()->json($endresult);

    }

    public function generateInvoiceID(request $request){
        $input = json_decode($request->getContent(),true);
        $rules = [
            'ProformaID' => 'required'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $ProformaID = $input['ProformaID'];
        $UserID = $this->param->UserID;

        $InvoiceID = DB::table('Invoice')
        ->where('ProformaID',$ProformaID)
        ->select(['InvoiceID'])
        ->first()->InvoiceID;



        if($InvoiceID === null)
        {
            $year = substr($input['PaidDate'],0,4);
            $month = substr($input['PaidDate'],5,2);
            $num = DB::table('Invoice')
            ->whereraw('CAST(SUBSTRING("InvoiceNO", 8,4) AS INTEGER)  ='.$year.' and CAST(SUBSTRING("InvoiceNO", 13,2) AS INTEGER) ='.$month)
            ->max('InvoiceID');
            $num = $num+1;
                $count = str_pad($num, 6, '0', STR_PAD_LEFT);
                $code = 'HB-INV-'.$year.'-'.$month.($count);
                $InvoiceNO = $code;
                $result = DB::table('Invoice')
                ->where('ProformaID',$ProformaID)
                ->update(array('InvoiceNO' => $InvoiceNO,'InvoiceID' => $num,'PaidDate' => @$input['PaidDate']));
        }
        else{
            $result = "InvoiceID already generated.";
            }

            if(@$input['PeriodLicense'] != null)
            {
                $countperiod = count($input['PeriodLicense']);
                $period = @$input['PeriodLicense'];

                for($i=0;$i<$countperiod;$i++){
                    $periodnew = $period[$i];

                    $startdate = DB::table('Invoice')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','Invoice.QuotationID')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationID','=','Quotation.QuotationID')
                    ->leftjoin('QuotationDetailLicense','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
                    ->select(['StartDate','EndDate','QuotationDetailLicenseID'])
                    ->where('QuotationDetail.BranchID',$periodnew['branch_id'])
                    ->where('Invoice.ProformaID',$ProformaID)
                    ->where('QuotationDetailLicense.LicenseID',$periodnew['license_id'])
                    ->get();
                    // print_r($startdate);


                        for($j=0;$j<count($startdate);$j++)
                        {
                            $periodnew['start_date'] = $this->emptyString($periodnew['start_date']);
                            $periodnew['end_date'] = $this->emptyString($periodnew['end_date']);

                            DB::table('QuotationDetailLicense')
                            ->where('QuotationDetailLicenseID',$startdate[$j]->QuotationDetailLicenseID)
                            ->update(array('StartDate' => $periodnew['start_date'],'EndDate' => $periodnew['end_date']));
                        }

            }
            }

            if(@$input['PeriodService'] != null)
            {
                $countperiod = count($input['PeriodService']);
                $period = @$input['PeriodService'];
                for($i=0;$i<$countperiod;$i++){
                    $periodnew = $period[$i];
                    $startdate = DB::table('Invoice')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','Invoice.QuotationID')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationID','=','Quotation.QuotationID')
                    ->leftjoin('QuotationDetailService','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
                    ->where('QuotationDetail.BranchID',$periodnew['branch_id'])
                    ->where('Invoice.ProformaID',$ProformaID)
                    ->where('QuotationDetailService.ServiceID',$periodnew['service_id'])
                    ->select(['StartDate','EndDate','QuotationDetailServiceID','ServiceID'])
                    ->get();


                        for($j=0;$j<count($startdate);$j++)
                        {
                            $periodnew['start_date'] = $this->emptyString($periodnew['start_date']);
                            $periodnew['end_date'] = $this->emptyString($periodnew['end_date']);

                            DB::table('QuotationDetailService')
                            ->where('QuotationDetailServiceID',$startdate[$j]->QuotationDetailServiceID)
                            ->update(array('StartDate' => $periodnew['start_date'],'EndDate' => $periodnew['end_date']));
                        }


            }

            }

      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'Invoice' => $result
      );

       return Response()->json($endresult);

    }

    public function generateInvoiceCommission(request $request){
        $input = json_decode($request->getContent(),true);
        $rules = [
            'ProformaID' => 'required',
            'Commission' => 'required'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $ProformaID = $input['ProformaID'];
        $UserID = $this->param->UserID;
        $Commission = $input['Commission'];
        $now = DB::table('Invoice')
        ->where('ProformaID',$ProformaID)
        ->first()->PaidDate;
        $check = DB::table('UserCommission')
        ->where('ProformaID',$ProformaID)
        ->get();
        if(count($check) == 0)
        {
            for($i=0;$i<count($this->coalesce($Commission,[])); $i++)
            {   //input service.
                $count = count($this->coalesce($Commission[$i]['Service'],[]));

                for($j=0;$j<$count;$j++)
                {
                    // $Commission = $Commission[$i][$i];

                    $param = array(
                        'UserID' => $Commission[$i]['UserID'],
                        'ProformaID' => $input['ProformaID'],
                        'ServiceID' => $Commission[$i]['Service'][$j]['ServiceID'],
                        'BranchID' => $Commission[$i]['Service'][$j]['BranchID'],
                        'CommissionValue' => $Commission[$i]['Service'][$j]['SubCommission'],
                        'CommissionPercentage' => $Commission[$i]['Service'][$j]['CommissionPercentage'],
                        'Date' => $now
                    );

                    $result = DB::table('UserCommission')
                    ->insert($param);

                }

                $count2 = count($this->coalesce($Commission[$i]['License'],[]));
                //input license
                for($j=0;$j<$count2;$j++)
                {
                    // $Commission = $Commission[$i];
                    $param = array(
                        'UserID' => $Commission[$i]['UserID'],
                        'ProformaID' => $input['ProformaID'],
                        'LicenseID' => $Commission[$i]['License'][$j]['LicenseID'],
                        'BranchID' => $Commission[$i]['License'][$j]['BranchID'],
                        'CommissionValue' => $Commission[$i]['License'][$j]['SubCommission'],
                        'CommissionPercentage' => $Commission[$i]['License'][$j]['CommissionPercentage'],
                        'Date' => $now
                    );
                    $result = DB::table('UserCommission')
                    ->insert($param);
                }
            }

            $result = DB::table('Invoice')
            ->where('ProformaID',$input['ProformaID'])
            ->update(array('StatusID' => "CG"));
            $endresult = array(
                'Status' => 0,
                'Errors' => array(),
                'Message' => "Success",
            );
        }
        else{
            $endresult = array(
                'Status' => 0,
                'Errors' => array(),
                'Message' => "Commission Already Generated",
            );
        }



       return Response()->json($endresult);

    }

    public function getInvoiceDetail(request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'ProformaID' => 'required'
      ];
      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $ProformaID = $input['ProformaID'];
      // return $InvoiceID;
      $result = DB::table('Invoice')
      ->leftjoin('Quotation', 'Quotation.QuotationID', '=','Invoice.QuotationID')
      ->leftjoin('Brand','Quotation.BrandID','=','Brand.BrandID')
      ->leftjoin('Status','Invoice.StatusID','=','Status.StatusID')
      ->leftjoin('Branch','Invoice.BranchID','Branch.BranchID')
      ->select(['InvoiceNO','InvoiceID','Quotation.QuotationID','Quotation.QuotationNO','ProformaID','ProformaNO',
      'Invoice.GrandTotal','Invoice.StatusID','Quotation.BrandID','BrandName','Invoice.BranchID','BranchName',
      'StatusName','DeliveryDate','DueDate','PaidDate','Brand.Contact','Brand.Phone', 'Brand.Email','Brand.Address',
      'Invoice.GrandTotalDiscount','Invoice.URL','ProformaNO','ReceiptID','ReceiptNO','PONumber','Note','Delivery'])
      ->where('ProformaID',$ProformaID)
      ->first();

      $user = DB::table('Invoice AS I')
      ->leftjoin('Quotation AS Q','I.QuotationID','=','Q.QuotationID')
      ->leftjoin('QuotationDetailUser AS QDU','QDU.QuotationID','=','Q.QuotationID')
      ->leftjoin('User AS U','U.UserID','=','QDU.UserID')
      ->select(['QDU.UserID','UserFullName','Username','Email','Phone','U.UserTypeID'])
      ->where('ProformaID',$ProformaID)
      ->orderby('QDU.QuotationDetailUserID')
      ->get();

      $result->User = $user;


      $result2 = DB::table('Invoice')
      ->leftjoin('Quotation', 'Quotation.QuotationID', '=','Invoice.QuotationID')
      ->leftjoin('Brand','Quotation.BrandID','=','Brand.BrandID')
      ->leftjoin('Status','Invoice.StatusID','=','Status.StatusID')
      ->leftjoin('Branch','Invoice.BranchID','Branch.BranchID')
      ->select(['InvoiceNO','InvoiceID','Quotation.QuotationID','Quotation.QuotationNO','ProformaID','ProformaNO',
      'Invoice.GrandTotal','Invoice.StatusID','Quotation.BrandID','BrandName','Invoice.BranchID','BranchName',
      'StatusName','DeliveryDate','DueDate','PaidDate','Branch.Contact','Branch.Phone', 'Branch.Email','Branch.Address',
      'Invoice.GrandTotalDiscount','Invoice.URL','ProformaNO','ReceiptID','ReceiptNO','PONumber','Note','Delivery'])
      ->where('ProformaID',$ProformaID)
      ->first();
      $result2->User = $user;

      $UserTypeID = $user[0]->UserTypeID;
      $Permission = DB::table('UserTypePermission')
      ->where('UserTypeID',$UserTypeID)
      ->where('PermissionID',"CreateQuotationShowName")
      ->get();

      if(count($Permission) == 0)
      {     $ShowName = false;
      }
      else{
          $ShowName = true;
      }
      $UserTypeID2 = $user[0]->UserTypeID;
      $Permission2 = DB::table('UserTypePermission')
      ->where('UserTypeID',$UserTypeID2)
      ->where('PermissionID',"CreateQuotationShowName")
      ->get();

      if(count($Permission2) == 0)
      {     $ShowName2 = false;
      }
      else{
          $ShowName2 = true;
      }
      $QuotationID = $result->QuotationID;

      $BranchID = $result->BranchID;
      $TotalPayment = DB::table('InvoicePayment')
      ->where('ProformaID',$ProformaID)
      ->sum('Paid');
      $QuotationID2 = $result2->QuotationID;
      $BranchID2 = $result2->BranchID;

      // return $QuotationID;
        if($BranchID === null)
        {
            $item =$result;
            // return $item;
            $item->PaymentLeft = $item->GrandTotal - $TotalPayment;
            $hardware = DB::table('QuotationDetailHardware')
            ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
            ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
            ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
            ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailHardware"."HardwareID","HardwareName",
                             sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"QuotationDetailHardware"."Price"'))
            ->where('Quotation.QuotationID', $QuotationID)
            ->groupby('QuotationDetailHardware.HardwareID')
            ->groupby('HardwareName')
            ->groupby('QuotationDetailHardware.Price')
            ->get();

            $item->hardware = $hardware;

            $license = DB::table('QuotationDetailLicense')
            ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
            ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
            ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
            ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
            ->select(DB::raw('"Quantity", "QuotationDetailLicense"."LicenseID","LicenseName","Free","StartDate","EndDate",
                             "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","QuotationDetailLicense"."Price"'))
            ->where('Quotation.QuotationID', $QuotationID)
            ->get();
            $item->license = $license;

            $service = DB::table('QuotationDetailService')
            ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
            ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
            ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
            ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
            ->select(DB::raw('"Quantity", "QuotationDetailService"."ServiceID","ServiceName","StartDate","EndDate",
                             "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","QuotationDetailService"."Price"'))
            ->where('Quotation.QuotationID', $QuotationID)
            ->get();
            $item->service = $service;
            $other = DB::table('QuotationDetailOther')
            ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailOther.QuotationDetailID')
            ->leftjoin('Other','QuotationDetailOther.OtherID','=','Other.OtherID')
            ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
            ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailOther"."OtherID","OtherName",
                             sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"QuotationDetailOther"."Price"'))
            ->where('Quotation.QuotationID', $QuotationID)
            ->groupby('QuotationDetailOther.OtherID')
            ->groupby('OtherName')
            ->groupby('QuotationDetailOther.Price')
            ->get();
            $item->other = $other;

            $item->ShowName = $ShowName;

            $endresult = array(
                'Status' => 0,
                'Errors' => array(),
                'Message' => "Success",
                'InvoiceDetail' => $result
            );

             return Response()->json($endresult);
        }
        else{
            $item =$result2;
            $item->PaymentLeft = $item->GrandTotal - $TotalPayment;
            $hardware = DB::table('QuotationDetailHardware')
            ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
            ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
            ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
            ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailHardware"."HardwareID","HardwareName",
                             sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"QuotationDetailHardware"."Price"'))
            ->where('Quotation.QuotationID', $QuotationID2)
            ->where('BranchID',$BranchID2)
            ->groupby('QuotationDetailHardware.HardwareID')
            ->groupby('HardwareName')
            ->groupby('QuotationDetailHardware.Price')
            ->get();

            $item->hardware = $hardware;

            $license = DB::table('QuotationDetailLicense')
            ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
            ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
            ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
            ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
            ->select(DB::raw('"Quantity", "QuotationDetailLicense"."LicenseID","LicenseName","Free","StartDate","EndDate",
                             "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","QuotationDetailLicense"."Price"'))
            ->where('Quotation.QuotationID', $QuotationID2)
            ->where('QuotationDetail.BranchID',$BranchID2)
            ->get();
            $item->license = $license;

            $service = DB::table('QuotationDetailService')
            ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
            ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
            ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
            ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
            ->select(DB::raw('"Quantity", "QuotationDetailService"."ServiceID","ServiceName","StartDate","EndDate",
                             "SubTotal", "DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","QuotationDetailService"."Price"'))
            ->where('Quotation.QuotationID', $QuotationID2)
            ->where('QuotationDetail.BranchID',$BranchID2)
            ->get();
            $item->service = $service;
            $other = DB::table('QuotationDetailOther')
            ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailOther.QuotationDetailID')
            ->leftjoin('Other','QuotationDetailOther.OtherID','=','Other.OtherID')
            ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
            ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailOther"."OtherID","OtherName",
                             sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"QuotationDetailOther"."Price"'))
            ->where('Quotation.QuotationID', $QuotationID2)
            ->where('BranchID',$BranchID2)
            ->groupby('QuotationDetailOther.OtherID')
            ->groupby('OtherName')
            ->groupby('QuotationDetailOther.Price')
            ->get();
            $item->other = $other;
            $item->ShowName = $ShowName2;
            $endresult = array(
                'Status' => 0,
                'Errors' => array(),
                'Message' => "Success",
                'InvoiceDetail' => $result2
            );

             return Response()->json($endresult);
        }
    }
      // return $item;

    public function getInvoiceDetailExcelRevised(request $request){
        $input = json_decode($request->getContent(),true);
        $rules = [
            'isGetAll' => 'required'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $isGetAll = $input['isGetAll'];

        $UserID = $this->param->UserID;
        $isGetAll = $input['isGetAll'];

        if($isGetAll === true){
            $header = DB::table('Invoice')
            ->leftjoin('Quotation', 'Quotation.QuotationID', '=','Invoice.QuotationID')
            ->leftjoin('Brand','Quotation.BrandID','=','Brand.BrandID')
            ->leftjoin('Status', 'Invoice.StatusID','=','Status.StatusID')
            ->leftjoin('Branch','Branch.BranchID','=','Invoice.BranchID')
            ->select(['InvoiceNO','InvoiceID','Quotation.QuotationID','Quotation.QuotationNO',
            'Invoice.GrandTotal','Invoice.StatusID','Quotation.BrandID','BrandName','Invoice.BranchID','BranchName','Branch.Contact AS BranchContact','Branch.Phone AS BranchPhone',
            'StatusName','DeliveryDate','DueDate','PaidDate','Brand.Contact AS BrandContact','Brand.Phone AS BrandPhone', 'Brand.Email','Brand.Address',
            'Invoice.GrandTotalDiscount','Invoice.URL','ProformaID','ProformaNO','ReceiptID','ReceiptNO','PONumber','Note','Delivery'])
            ->orderby('ProformaID','desc')
            ->limit(1)
            ->get();
            $HeaderColumn = $this->getcolumnname($header);
            $user = DB::table('QuotationDetailUser AS QDU')
                ->leftjoin('Quotation AS Q','Q.QuotationID','=','QDU.QuotationID')
                ->leftjoin('User AS U','QDU.UserID','=','U.UserID')
                ->leftjoin('Invoice AS I','I.QuotationID','=','Q.QuotationID')
                ->leftjoin('UserType AS UT','U.UserTypeID','=','UT.UserTypeID')
                ->select(['QDU.UserID','UserFullName','Username','Email as UserEmail','Phone as UserPhone','UT.UserTypeID','UserTypeName'])
                ->orderby('QuotationDetailUserID','asc')
                ->limit(10)
                ->get();
            $UserColumn = $this->getcolumnname($user);

            $query = DB::table('Invoice')
            ->leftjoin('Quotation', 'Quotation.QuotationID', '=','Invoice.QuotationID')
            ->leftjoin('QuotationDetailUser As QDU','QDU.QuotationID','=','Quotation.QuotationID')
            ->leftjoin('User AS U','QDU.UserID','=','U.UserID')
            ->leftjoin('UserType AS UT','U.UserTypeID','=','UT.UserTypeID')
            ->leftjoin('Brand','Quotation.BrandID','=','Brand.BrandID')
            ->leftjoin('Status', 'Invoice.StatusID','=','Status.StatusID')
            ->leftjoin('Branch','Branch.BranchID','=','Invoice.BranchID')
            ->select(['InvoiceNO','InvoiceID','Quotation.QuotationID','Quotation.QuotationNO',
            'Invoice.GrandTotal','Invoice.StatusID','Quotation.BrandID','BrandName','Invoice.BranchID','BranchName','Branch.Contact AS BranchContact','Branch.Phone AS BranchPhone',
            'StatusName','DeliveryDate','DueDate','PaidDate','Brand.Contact AS BrandContact','Brand.Phone AS BrandPhone', 'Brand.Email','Brand.Address',
            'Invoice.GrandTotalDiscount','Invoice.URL','ProformaID','ProformaNO','ReceiptID','ReceiptNO','PONumber','Note','Delivery',
            'QDU.UserID','UserFullName','Username','U.Email as UserEmail','U.Phone as UserPhone','UT.UserTypeID','UserTypeName',
                DB::raw('CASE WHEN (select "PermissionName" from "UserTypePermission" utp 
                left join "Permission" p on p."PermissionID" = utp."PermissionID" 
                where utp."UserTypeID" = "UT"."UserTypeID" and utp."PermissionID" = \'CreateQuotationShowName\') is null then false else true END AS ShowName'),
                DB::raw('"Invoice"."GrandTotal" - coalesce((select sum("Paid") from "InvoicePayment" p where p."ProformaID" = "Invoice"."ProformaID"), 0) as "PaymentLeft"')])
            ->orderby('ProformaID','desc')
            ->orderby('QuotationDetailUserID','asc');

            $HardwareQuery = DB::table('QuotationDetailHardware')
            ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
            ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
            ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
            ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailHardware"."HardwareID","HardwareName",
                             sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Hardware"."Price","DiscountName","Quotation"."QuotationID", "QuotationDetail"."BranchID"'))
            ->groupby('QuotationDetailHardware.HardwareID')
            ->groupby('HardwareName')
            ->groupby('Hardware.Price')
            ->groupby('DiscountName')
            ->groupby('Quotation.QuotationID')
            ->groupby('QuotationDetail.BranchID')
            ->get();

            $OtherQuery = DB::table('QuotationDetailOther')
            ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailOther.QuotationDetailID')
            ->leftjoin('Other','QuotationDetailOther.OtherID','=','Other.OtherID')
            ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
            ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailOther"."OtherID","OtherName",
                             sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Other"."Price","DiscountName",
                             "Quotation"."QuotationID", "QuotationDetail"."BranchID"'))
            ->groupby('QuotationDetailOther.OtherID')
            ->groupby('OtherName')
            ->groupby('DiscountName')
            ->groupby('Other.Price')
            ->groupby('Quotation.QuotationID')
            ->groupby('QuotationDetail.BranchID')
            ->get();

            $ServiceQuery = DB::table('QuotationDetailService')
            ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
            ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
            ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
            ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
            ->select(DB::raw('"Quantity", "QuotationDetailService"."ServiceID","ServiceName","StartDate","EndDate","QuotationDetailServiceID",
                             "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","Service"."Price","DiscountName",
                             "Quotation"."QuotationID", "QuotationDetail"."BranchID"'))
            ->get();

            $LicenseQuery = DB::table('QuotationDetailLicense')
            ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
            ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
            ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
            ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
            ->select(DB::raw('"Quantity", "QuotationDetailLicense"."LicenseID","LicenseName","Free","StartDate","EndDate","QuotationDetailLicenseID",
                                "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","License"."Price","DiscountName","Quotation"."QuotationID", "QuotationDetail"."BranchID"'))
            ->get();

            $LicenseColumn = DB::table('QuotationDetailLicense')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
                    ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                    ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(DB::raw('"Quantity", "QuotationDetailLicense"."LicenseID","LicenseName","Free","StartDate","EndDate",
                                     "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","License"."Price","DiscountName"'))
                    ->limit(10)
                    ->get();

            $HardwareColumn = DB::table('QuotationDetailHardware')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
                    ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailHardware"."HardwareID","HardwareName",
                                     sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Hardware"."Price","DiscountName"'))
                    ->groupby('QuotationDetailHardware.HardwareID')
                    ->groupby('HardwareName')
                    ->groupby('Hardware.Price')
                    ->groupby('DiscountName')
                    ->limit(10)
                    ->get();

            $OtherColumn = DB::table('QuotationDetailOther')
            ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailOther.QuotationDetailID')
            ->leftjoin('Other','QuotationDetailOther.OtherID','=','Other.OtherID')
            ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
            ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailOther"."OtherID","OtherName",
                             sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Other"."Price","DiscountName"'))
            ->groupby('QuotationDetailOther.OtherID')
            ->groupby('OtherName')
            ->groupby('DiscountName')
            ->groupby('Other.Price')
            ->limit(10)
            ->get();
            
            $ServiceColumn = DB::table('QuotationDetailService')
            ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
            ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
            ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
            ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
            ->select(DB::raw('"Quantity", "QuotationDetailService"."ServiceID","ServiceName","StartDate","EndDate",
                             "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","Service"."Price","DiscountName"'))
            ->limit(10)
            ->get();

            $HardwareColumn = $this->getcolumnname($HardwareColumn);
            $LicenseColumn = $this->getcolumnname($LicenseColumn);
            $ServiceColumn = $this->getcolumnname($ServiceColumn);
            $OtherColumn = $this->getcolumnname($OtherColumn);
           
            $result = $query->get();
            
            $InvoiceList = [];
            // dd($result);
            // print_r(json_encode($result));
            // die();
            foreach($result as $a){
                // dd($a);
                foreach($HeaderColumn as $b){
                    $InvoiceList[$a->ProformaID][$b] = $a->$b;
                }
                
                $InvoiceList[$a->ProformaID]['User'] = [];
                $InvoiceList[$a->ProformaID]['hardware'] = [];
                $InvoiceList[$a->ProformaID]['license'] = [];
                $InvoiceList[$a->ProformaID]['service'] = [];
                $InvoiceList[$a->ProformaID]['other'] = [];
                $InvoiceList[$a->ProformaID]['PaymentLeft'] = $a->PaymentLeft;
                // if($a->)
            }

            foreach($result as $a){
                foreach($UserColumn as $b){
                    $InvoiceList[$a->ProformaID]['User'][$a->UserID][$b] = $a->$b;
                }
                $InvoiceList[$a->ProformaID]['User'][$a->UserID]['ShowName'] = $a->showname;                    
            }

            foreach($InvoiceList as &$a){
                $a['User'] = array_values($a['User']);
            }
            
            foreach($InvoiceList as &$a){
                $a['ShowName'] = $a['User'][0]['ShowName'];
                // dd($a);
            }
            $InvoiceList = array_values($InvoiceList);
            // print_r(json_encode($ServiceQuery));
            // die();

            foreach($InvoiceList as &$a){
                foreach($HardwareQuery as &$b){
                    if($a['QuotationID'] == $b->QuotationID && $a['BranchID'] === null){
                        foreach($HardwareColumn as &$c){
                            if($c != 'quantity' && $c != 'subtotal' && $c != 'discounttotal' && $c != 'pricetotal')
                                $a['hardware'][$b->HardwareID.$b->BranchID][$c] = $b->$c;
                            elseif(@$a['hardware'][$b->HardwareID.$b->BranchID][$c] === null)
                                $a['hardware'][$b->HardwareID.$b->BranchID][$c] = $b->$c;
                            else
                                $a['hardware'][$b->HardwareID.$b->BranchID][$c] += $b->$c;
                        }
                    }
                    elseif($a['QuotationID'] == $b->QuotationID && $a['BranchID'] == $b->BranchID){
                        foreach($HardwareColumn as &$c){
                            if($c != 'quantity' && $c != 'subtotal' && $c != 'discounttotal' && $c != 'pricetotal')
                                $a['hardware'][$b->HardwareID.$b->BranchID][$c] = $b->$c;
                            elseif(@$a['hardware'][$b->HardwareID.$b->BranchID][$c] === null)
                                $a['hardware'][$b->HardwareID.$b->BranchID][$c] = $b->$c;
                            else
                                $a['hardware'][$b->HardwareID.$b->BranchID][$c] += $b->$c;
                        }
                    }
                }
            }
            // print_r(json_encode($LicenseQuery));
            // die();
            foreach($InvoiceList as &$a){
                foreach($LicenseQuery as &$b){
                    if($a['QuotationID'] == $b->QuotationID && $a['BranchID'] === null){
                        foreach($LicenseColumn as &$c){
                            if($c != 'quantity' && $c != 'subtotal' && $c != 'discounttotal' && $c != 'pricetotal')
                                $a['license'][$b->QuotationDetailLicenseID][$c] = $b->$c;
                            elseif(@$a['license'][$b->QuotationDetailLicenseID][$c] === null)
                                $a['license'][$b->QuotationDetailLicenseID][$c] = $b->$c;
                            else
                                $a['license'][$b->QuotationDetailLicenseID][$c] += $b->$c;
                        }
                    }
                    elseif($a['QuotationID'] == $b->QuotationID && $a['BranchID'] == $b->BranchID){
                        foreach($LicenseColumn as &$c){
                            if($c != 'quantity' && $c != 'subtotal' && $c != 'discounttotal' && $c != 'pricetotal')
                                $a['license'][$b->QuotationDetailLicenseID][$c] = $b->$c;
                            elseif(@$a['license'][$b->QuotationDetailLicenseID][$c] === null)
                                $a['license'][$b->QuotationDetailLicenseID][$c] = $b->$c;
                            else
                                $a['license'][$b->QuotationDetailLicenseID][$c] += $b->$c;
                        }
                    }
                }
            }

            foreach($InvoiceList as &$a){
                foreach($ServiceQuery as &$b){
                    if($a['QuotationID'] == $b->QuotationID && $a['BranchID'] === null){
                        foreach($ServiceColumn as &$c){
                            if($c != 'quantity' && $c != 'subtotal' && $c != 'discounttotal' && $c != 'pricetotal')
                                $a['service'][$b->QuotationDetailServiceID][$c] = $b->$c;
                            elseif(@$a['service'][$b->QuotationDetailServiceID][$c] === null)
                                $a['service'][$b->QuotationDetailServiceID][$c] = $b->$c;
                            else
                                $a['service'][$b->QuotationDetailServiceID][$c] += $b->$c;
                        }   
                    }
                    elseif($a['QuotationID'] == $b->QuotationID && $a['BranchID'] == $b->BranchID){
                    foreach($ServiceColumn as &$c){
                        if($c != 'quantity' && $c != 'subtotal' && $c != 'discounttotal' && $c != 'pricetotal')
                            $a['service'][$b->QuotationDetailServiceID][$c] = $b->$c;
                        elseif(@$a['service'][$b->QuotationDetailServiceID][$c] === null)
                            $a['service'][$b->QuotationDetailServiceID][$c] = $b->$c;
                        else
                            $a['service'][$b->QuotationDetailServiceID][$c] += $b->$c;
                    }
                }
                }
            }
            
            foreach($InvoiceList as &$a){
                foreach($OtherQuery as &$b){
                    if($a['QuotationID'] == $b->QuotationID && $a['BranchID'] === null){
                        foreach($OtherColumn as &$c){
                            if($c != 'quantity' && $c != 'subtotal' && $c != 'discounttotal' && $c != 'pricetotal')
                                $a['other'][$b->OtherID.$b->BranchID][$c] = $b->$c;
                            elseif(@$a['other'][$b->OtherID.$b->BranchID][$c] === null)
                                $a['other'][$b->OtherID.$b->BranchID][$c] = $b->$c;
                            else
                                $a['other'][$b->OtherID.$b->BranchID][$c] += $b->$c;
                        }
                    }
                    elseif($a['QuotationID'] == $b->QuotationID && $a['BranchID'] == $b->BranchID){
                        foreach($OtherColumn as &$c){
                            if($c != 'quantity' && $c != 'subtotal' && $c != 'discounttotal' && $c != 'pricetotal')
                                $a['other'][$b->OtherID.$b->BranchID][$c] = $b->$c;
                            elseif(@$a['other'][$b->OtherID.$b->BranchID][$c] === null)
                                $a['other'][$b->OtherID.$b->BranchID][$c] = $b->$c;
                            else
                                $a['other'][$b->OtherID.$b->BranchID][$c] += $b->$c;
                        }
                    }
                }
            }

            foreach($InvoiceList as &$a){
                $a['hardware'] = array_values($a['hardware']);
                $a['license'] = array_values($a['license']);
                $a['other'] = array_values($a['other']);
                $a['service'] = array_values($a['service']);
            }

            $header = $InvoiceList;
            // print_r(json_encode($InvoiceList));
            // die();
            // $headercount = count($header);
            // return $header;

            $endresult = array(
                'Status' => 0,
                'Errors' => array(),
                'Message' => "Success",
                'Header' => $header
                );

        }
        else{
            $header = DB::table('Invoice')
            ->leftjoin('Quotation', 'Quotation.QuotationID', '=','Invoice.QuotationID')
            ->leftjoin('QuotationDetailUser','QuotationDetailUser.QuotationID','=','Quotation.QuotationID')
            ->leftjoin('Brand','Quotation.BrandID','=','Brand.BrandID')
            ->leftjoin('Status', 'Invoice.StatusID','=','Status.StatusID')
            ->leftjoin('Branch','Branch.BranchID','=','Invoice.BranchID')
            ->where('QuotationDetailUser.UserID',$UserID)
            ->select(['InvoiceNO','InvoiceID','Quotation.QuotationID','Quotation.QuotationNO',
            'Invoice.GrandTotal','Invoice.StatusID','Quotation.BrandID','BrandName','Invoice.BranchID','BranchName','Branch.Contact AS BranchContact','Branch.Phone AS BranchPhone',
            'StatusName','DeliveryDate','DueDate','PaidDate','Brand.Contact AS BrandContact','Brand.Phone AS BrandPhone', 'Brand.Email','Brand.Address',
            'Invoice.GrandTotalDiscount','Invoice.URL','ProformaID','ProformaNO','ReceiptID','ReceiptNO','PONumber','Note'])
            ->orderby('ProformaID','desc')
            ->get();
            for($i=0;$i<count($header);$i++)
            {
                $ProformaID = $header[$i]->ProformaID;
                $user = DB::table('QuotationDetailUser AS QDU')
                ->leftjoin('Quotation AS Q','Q.QuotationID','=','QDU.QuotationID')
                ->leftjoin('User AS U','QDU.UserID','=','U.UserID')
                ->leftjoin('UserType AS UT','UT.UserTypeID','=','U.UserTypeID')
                ->leftjoin('Invoice AS I','I.QuotationID','=','Q.QuotationID')
                ->select(['QDU.UserID','UserFullName','Username','Email','Phone','U.UserTypeID','UserTypeName'])
                ->where('ProformaID',$ProformaID)
                ->orderby('QuotationDetailUserID','asc')
                ->get();
                $header[$i]->User = $user;
            }
            $headercount = count($header);

                        for($i=0;$i<$headercount;$i++)
                        {
                            $ProformaID = $header[$i]->ProformaID;

                            $UserTypeID = $header[$i]->User[0]->UserTypeID;
                            $Permission = DB::table('UserTypePermission')
                            ->where('UserTypeID',$UserTypeID)
                            ->where('PermissionID',"CreateQuotationShowName")
                            ->get();

                            if(count($Permission) == 0)
                            {
                                $ShowName = false;
                            }
                            else{
                                $ShowName = true;
                            }

                            $QuotationID = $header[$i]->QuotationID;
                            $BranchID = $header[$i]->BranchID;
                            $TotalPayment = DB::table('InvoicePayment')
                            ->where('ProformaID',$ProformaID)
                            ->sum('Paid');

                            if($BranchID === null)
                            {

                                $item =$header[$i];
                                // return $item;
                                $item->PaymentLeft = $item->GrandTotal - $TotalPayment;
                                $hardware = DB::table('QuotationDetailHardware')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
                                ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailHardware"."HardwareID","HardwareName",
                                                 sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Hardware"."Price","DiscountName"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->groupby('QuotationDetailHardware.HardwareID')
                                ->groupby('DiscountName')
                                ->groupby('Hardware.Price')
                                ->groupby('HardwareName')
                                ->get();

                                $item->hardware = $hardware;

                                $license = DB::table('QuotationDetailLicense')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
                                ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                                ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('"Quantity", "QuotationDetailLicense"."LicenseID","LicenseName","Free","StartDate","EndDate",
                                                 "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","License"."Price","DiscountName"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->get();
                                $item->license = $license;
                                $service = DB::table('QuotationDetailService')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
                                ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                                ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('"Quantity", "QuotationDetailService"."ServiceID","ServiceName","StartDate","EndDate",
                                                 "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","Service"."Price","DiscountName"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->get();
                                $item->service = $service;
                                $other = DB::table('QuotationDetailOther')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailOther.QuotationDetailID')
                                ->leftjoin('Other','QuotationDetailOther.OtherID','=','Other.OtherID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailOther"."OtherID","OtherName",
                                                 sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Other"."Price","DiscountName"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->groupby('QuotationDetailOther.OtherID')
                                ->groupby('OtherName')
                                ->groupby('Other.Price')
                                ->groupby('DiscountName')
                                ->get();
                                $item->other = $other;
                                $item->ShowName = $ShowName;



                            }
                            else{
                                $item =$header[$i];
                                $item->PaymentLeft = $item->GrandTotal - $TotalPayment;
                                $hardware = DB::table('QuotationDetailHardware')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
                                ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailHardware"."HardwareID","HardwareName",
                                                 sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Hardware"."Price","DiscountName"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->where('BranchID',$BranchID)
                                ->groupby('QuotationDetailHardware.HardwareID')
                                ->groupby('HardwareName')
                                ->groupby('DiscountName')
                                ->groupby('Hardware.Price')
                                ->get();

                                $item->hardware = $hardware;

                                $license = DB::table('QuotationDetailLicense')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
                                ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                                ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('"Quantity", "QuotationDetailLicense"."LicenseID","LicenseName","Free","StartDate","EndDate",
                                                 "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","License"."Price","DiscountName"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->where('QuotationDetail.BranchID',$BranchID)
                                ->get();
                                $item->license = $license;
                                $service = DB::table('QuotationDetailService')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
                                ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                                ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('"Quantity", "QuotationDetailService"."ServiceID","ServiceName","StartDate","EndDate",
                                                 "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","Service"."Price","DiscountName"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->where('QuotationDetail.BranchID', $BranchID)
                                ->get();
                                $item->service = $service;
                                $other = DB::table('QuotationDetailOther')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailOther.QuotationDetailID')
                                ->leftjoin('Other','QuotationDetailOther.OtherID','=','Other.OtherID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailOther"."OtherID","OtherName",
                                                 sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Other"."Price","DiscountName"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->where('BranchID',$BranchID)
                                ->groupby('QuotationDetailOther.OtherID')
                                ->groupby('OtherName')
                                ->groupby('Other.Price')
                                ->groupby('DiscountName')
                                ->get();
                                $item->other = $other;
                                $item->ShowName = $ShowName;

                            }

                        }
                        $endresult = array(
                            'Status' => 0,
                            'Errors' => array(),
                            'Message' => "Success",
                            'Header' => $header);
        }

        // return $headercount;


        return Response()->json($endresult);

        // return $QuotationID;


    }

    public function getInvoiceDetailExcel(request $request){
        $input = json_decode($request->getContent(),true);
        $rules = [
            'isGetAll' => 'required'
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $errorList = $this->checkErrors($rules, $errors);
            $additional = null;
            $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
            return response()->json($response);
        }
        $isGetAll = $input['isGetAll'];

        $UserID = $this->param->UserID;
        $isGetAll = $input['isGetAll'];

        if($isGetAll === true){
            $header = DB::table('Invoice')
            ->leftjoin('Quotation', 'Quotation.QuotationID', '=','Invoice.QuotationID')
            ->leftjoin('Brand','Quotation.BrandID','=','Brand.BrandID')
            ->leftjoin('Status', 'Invoice.StatusID','=','Status.StatusID')
            ->leftjoin('Branch','Branch.BranchID','=','Invoice.BranchID')
            ->select(['InvoiceNO','InvoiceID','Quotation.QuotationID','Quotation.QuotationNO',
            'Invoice.GrandTotal','Invoice.StatusID','Quotation.BrandID','BrandName','Invoice.BranchID','BranchName','Branch.Contact AS BranchContact','Branch.Phone AS BranchPhone',
            'StatusName','DeliveryDate','DueDate','PaidDate','Brand.Contact AS BrandContact','Brand.Phone AS BrandPhone', 'Brand.Email','Brand.Address',
            'Invoice.GrandTotalDiscount','Invoice.URL','ProformaID','ProformaNO','ReceiptID','ReceiptNO','PONumber','Note','Delivery'])
            ->orderby('ProformaID','desc')
            ->get();
            for($i=0;$i<count($header);$i++)
            {
                $ProformaID = $header[$i]->ProformaID;
                $user = DB::table('QuotationDetailUser AS QDU')
                ->leftjoin('Quotation AS Q','Q.QuotationID','=','QDU.QuotationID')
                ->leftjoin('User AS U','QDU.UserID','=','U.UserID')
                ->leftjoin('Invoice AS I','I.QuotationID','=','Q.QuotationID')
                ->leftjoin('UserType AS UT','U.UserTypeID','=','UT.UserTypeID')
                ->select(['QDU.UserID','UserFullName','Username','Email','Phone','UT.UserTypeID','UserTypeName'])
                ->where('ProformaID',$ProformaID)
                ->orderby('QuotationDetailUserID','asc')
                ->get();
                $header[$i]->User = $user;
            }
            $headercount = count($header);
            // return $header;

            for($i=0;$i<$headercount;$i++)
            {
                $ProformaID = $header[$i]->ProformaID;

                $UserTypeID = $header[$i]->User[0]->UserTypeID;

                $Permission = DB::table('UserTypePermission')
                ->where('UserTypeID',$UserTypeID)
                ->where('PermissionID',"CreateQuotationShowName")
                ->get();

                if(count($Permission) == 0)
                {
                    $ShowName = false;
                }
                else{
                    $ShowName = true;
                }

                $QuotationID = $header[$i]->QuotationID;
                $BranchID = $header[$i]->BranchID;
                $TotalPayment = DB::table('InvoicePayment')
                ->where('ProformaID',$ProformaID)
                ->sum('Paid');

                if($BranchID === null)
                {

                    $item =$header[$i];
                    // return $item;
                    $item->PaymentLeft = $item->GrandTotal - $TotalPayment;
                    $hardware = DB::table('QuotationDetailHardware')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
                    ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailHardware"."HardwareID","HardwareName",
                                     sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Hardware"."Price","DiscountName"'))
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->groupby('QuotationDetailHardware.HardwareID')
                    ->groupby('HardwareName')
                    ->groupby('Hardware.Price')
                    ->groupby('DiscountName')
                    ->get();

                    $item->hardware = $hardware;

                    $license = DB::table('QuotationDetailLicense')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
                    ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                    ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(DB::raw('"Quantity", "QuotationDetailLicense"."LicenseID","LicenseName","Free","StartDate","EndDate",
                                     "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","License"."Price","DiscountName"'))
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->get();
                    $item->license = $license;
                    $service = DB::table('QuotationDetailService')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
                    ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                    ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(DB::raw('"Quantity", "QuotationDetailService"."ServiceID","ServiceName","StartDate","EndDate",
                                     "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","Service"."Price","DiscountName"'))
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->get();
                    $item->service = $service;
                    $other = DB::table('QuotationDetailOther')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailOther.QuotationDetailID')
                    ->leftjoin('Other','QuotationDetailOther.OtherID','=','Other.OtherID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailOther"."OtherID","OtherName",
                                     sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Other"."Price","DiscountName"'))
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->groupby('QuotationDetailOther.OtherID')
                    ->groupby('OtherName')
                    ->groupby('DiscountName')
                    ->groupby('Other.Price')
                    ->get();
                    $item->other = $other;

                    $item->ShowName = $ShowName;

                }
                else{
                    $item = $header[$i];
                    $item->PaymentLeft = $item->GrandTotal - $TotalPayment;
                    $hardware = DB::table('QuotationDetailHardware')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
                    ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailHardware"."HardwareID","HardwareName",
                                     sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Hardware"."Price","DiscountName"'))
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->where('BranchID',$BranchID)
                    ->groupby('QuotationDetailHardware.HardwareID')
                    ->groupby('HardwareName')
                    ->groupby('Hardware.Price')
                    ->groupby('DiscountName')
                    ->get();

                    $item->hardware = $hardware;

                    $license = DB::table('QuotationDetailLicense')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
                    ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                    ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(DB::raw('"Quantity", "QuotationDetailLicense"."LicenseID","LicenseName","Free","StartDate","EndDate",
                                     "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","License"."Price","DiscountName"'))
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->where('QuotationDetail.BranchID',$BranchID)
                    ->get();
                    $item->license = $license;
                    $service = DB::table('QuotationDetailService')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
                    ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                    ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(DB::raw('"Quantity", "QuotationDetailService"."ServiceID","ServiceName","StartDate","EndDate",
                                     "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","Service"."Price","DiscountName"'))
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->where('QuotationDetail.BranchID',$BranchID)
                    ->get();
                    $item->service = $service;
                    $other = DB::table('QuotationDetailOther')
                    ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailOther.QuotationDetailID')
                    ->leftjoin('Other','QuotationDetailOther.OtherID','=','Other.OtherID')
                    ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                    ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailOther"."OtherID","OtherName",
                                     sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Other"."Price","DiscountName"'))
                    ->where('Quotation.QuotationID', $QuotationID)
                    ->where('BranchID',$BranchID)
                    ->groupby('QuotationDetailOther.OtherID')
                    ->groupby('OtherName')
                    ->groupby('DiscountName')
                    ->groupby('Other.Price')
                    ->get();
                    $item->other = $other;
                    $item->ShowName = $ShowName;

                }

            }
            $endresult = array(
                'Status' => 0,
                'Errors' => array(),
                'Message' => "Success",
                'Header' => $header
                );

        }
        else{
            $header = DB::table('Invoice')
            ->leftjoin('Quotation', 'Quotation.QuotationID', '=','Invoice.QuotationID')
            ->leftjoin('QuotationDetailUser','QuotationDetailUser.QuotationID','=','Quotation.QuotationID')
            ->leftjoin('Brand','Quotation.BrandID','=','Brand.BrandID')
            ->leftjoin('Status', 'Invoice.StatusID','=','Status.StatusID')
            ->leftjoin('Branch','Branch.BranchID','=','Invoice.BranchID')
            ->where('QuotationDetailUser.UserID',$UserID)
            ->select(['InvoiceNO','InvoiceID','Quotation.QuotationID','Quotation.QuotationNO',
            'Invoice.GrandTotal','Invoice.StatusID','Quotation.BrandID','BrandName','Invoice.BranchID','BranchName','Branch.Contact AS BranchContact','Branch.Phone AS BranchPhone',
            'StatusName','DeliveryDate','DueDate','PaidDate','Brand.Contact AS BrandContact','Brand.Phone AS BrandPhone', 'Brand.Email','Brand.Address',
            'Invoice.GrandTotalDiscount','Invoice.URL','ProformaID','ProformaNO','ReceiptID','ReceiptNO','PONumber','Note'])
            ->orderby('ProformaID','desc')
            ->get();
            for($i=0;$i<count($header);$i++)
            {
                $ProformaID = $header[$i]->ProformaID;
                $user = DB::table('QuotationDetailUser AS QDU')
                ->leftjoin('Quotation AS Q','Q.QuotationID','=','QDU.QuotationID')
                ->leftjoin('User AS U','QDU.UserID','=','U.UserID')
                ->leftjoin('UserType AS UT','UT.UserTypeID','=','U.UserTypeID')
                ->leftjoin('Invoice AS I','I.QuotationID','=','Q.QuotationID')
                ->select(['QDU.UserID','UserFullName','Username','Email','Phone','U.UserTypeID','UserTypeName'])
                ->where('ProformaID',$ProformaID)
                ->orderby('QuotationDetailUserID','asc')
                ->get();
                $header[$i]->User = $user;
            }
            $headercount = count($header);

                        for($i=0;$i<$headercount;$i++)
                        {
                            $ProformaID = $header[$i]->ProformaID;

                            $UserTypeID = $header[$i]->User[0]->UserTypeID;
                            $Permission = DB::table('UserTypePermission')
                            ->where('UserTypeID',$UserTypeID)
                            ->where('PermissionID',"CreateQuotationShowName")
                            ->get();

                            if(count($Permission) == 0)
                            {
                                $ShowName = false;
                            }
                            else{
                                $ShowName = true;
                            }

                            $QuotationID = $header[$i]->QuotationID;
                            $BranchID = $header[$i]->BranchID;
                            $TotalPayment = DB::table('InvoicePayment')
                            ->where('ProformaID',$ProformaID)
                            ->sum('Paid');

                            if($BranchID === null)
                            {

                                $item =$header[$i];
                                // return $item;
                                $item->PaymentLeft = $item->GrandTotal - $TotalPayment;
                                $hardware = DB::table('QuotationDetailHardware')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
                                ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailHardware"."HardwareID","HardwareName",
                                                 sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Hardware"."Price","DiscountName"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->groupby('QuotationDetailHardware.HardwareID')
                                ->groupby('DiscountName')
                                ->groupby('Hardware.Price')
                                ->groupby('HardwareName')
                                ->get();

                                $item->hardware = $hardware;

                                $license = DB::table('QuotationDetailLicense')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
                                ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                                ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('"Quantity", "QuotationDetailLicense"."LicenseID","LicenseName","Free","StartDate","EndDate",
                                                 "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","License"."Price","DiscountName"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->get();
                                $item->license = $license;
                                $service = DB::table('QuotationDetailService')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
                                ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                                ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('"Quantity", "QuotationDetailService"."ServiceID","ServiceName","StartDate","EndDate",
                                                 "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","Service"."Price","DiscountName"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->get();
                                $item->service = $service;
                                $other = DB::table('QuotationDetailOther')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailOther.QuotationDetailID')
                                ->leftjoin('Other','QuotationDetailOther.OtherID','=','Other.OtherID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailOther"."OtherID","OtherName",
                                                 sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Other"."Price","DiscountName"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->groupby('QuotationDetailOther.OtherID')
                                ->groupby('OtherName')
                                ->groupby('Other.Price')
                                ->groupby('DiscountName')
                                ->get();
                                $item->other = $other;
                                $item->ShowName = $ShowName;



                            }
                            else{
                                $item =$header[$i];
                                $item->PaymentLeft = $item->GrandTotal - $TotalPayment;
                                $hardware = DB::table('QuotationDetailHardware')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
                                ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailHardware"."HardwareID","HardwareName",
                                                 sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Hardware"."Price","DiscountName"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->where('BranchID',$BranchID)
                                ->groupby('QuotationDetailHardware.HardwareID')
                                ->groupby('HardwareName')
                                ->groupby('DiscountName')
                                ->groupby('Hardware.Price')
                                ->get();

                                $item->hardware = $hardware;

                                $license = DB::table('QuotationDetailLicense')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
                                ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                                ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('"Quantity", "QuotationDetailLicense"."LicenseID","LicenseName","Free","StartDate","EndDate",
                                                 "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","License"."Price","DiscountName"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->where('QuotationDetail.BranchID',$BranchID)
                                ->get();
                                $item->license = $license;
                                $service = DB::table('QuotationDetailService')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
                                ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
                                ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('"Quantity", "QuotationDetailService"."ServiceID","ServiceName","StartDate","EndDate",
                                                 "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","Service"."Price","DiscountName"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->where('QuotationDetail.BranchID', $BranchID)
                                ->get();
                                $item->service = $service;
                                $other = DB::table('QuotationDetailOther')
                                ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailOther.QuotationDetailID')
                                ->leftjoin('Other','QuotationDetailOther.OtherID','=','Other.OtherID')
                                ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
                                ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailOther"."OtherID","OtherName",
                                                 sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"Other"."Price","DiscountName"'))
                                ->where('Quotation.QuotationID', $QuotationID)
                                ->where('BranchID',$BranchID)
                                ->groupby('QuotationDetailOther.OtherID')
                                ->groupby('OtherName')
                                ->groupby('Other.Price')
                                ->groupby('DiscountName')
                                ->get();
                                $item->other = $other;
                                $item->ShowName = $ShowName;

                            }

                        }
                        $endresult = array(
                            'Status' => 0,
                            'Errors' => array(),
                            'Message' => "Success",
                            'Header' => $header);
        }

        // return $headercount;


        return Response()->json($endresult);

        // return $QuotationID;


    }

    public function getInvoicePreview(request $request){
      $input = json_decode($request->getContent(),true);
      $rules = [
          'QuotationID' => 'required',
          'SplitBill' => 'required'

      ];
      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      $QuotationID = $input['QuotationID'];
      $SplitBill = $input['SplitBill'];
     if($SplitBill == true)
     {
         $header = DB::table('Quotation')
         ->leftjoin('QuotationDetail','Quotation.QuotationID','=','QuotationDetail.QuotationID')
         ->leftjoin('Branch','Branch.BranchID','=','QuotationDetail.BranchID')
         ->select(['QuotationDetail.BranchID','BranchName','Branch.Phone','Branch.Address','Branch.Contact','Total','DiscountTotalBranch','Branch.Email',
                    'Quotation.QuotationID','QuotationNO'])
         ->where('Quotation.QuotationID',$QuotationID)
         ->get();


         for($i = 0; $i<count($header);$i++)
         {
             $item = $header[$i];
             $user = DB::table('QuotationDetailUser AS QDU')
             ->leftjoin('User AS U','QDU.UserID','=','U.UserID')
             ->leftjoin('UserType AS UT', 'UT.UserTypeID','=','U.UserTypeID')
             ->select(['QDU.UserID','UserFullName','Username','Email','Phone','UT.UserTypeID'])
             ->where('QuotationID',$QuotationID)
             ->orderby('QuotationDetailUserID','asc')
             ->get();
             $item->User = $user;
             $UserTypeID = $user[0]->UserTypeID;
             $Permission = DB::table('UserTypePermission')
             ->where('UserTypeID',$UserTypeID)
             ->where('PermissionID',"CreateQuotationShowName")
             ->get();

             if(count($Permission) == 0)
             {     $ShowName = false;
             }
             else{
                 $ShowName = true;
             }
             $hardware = DB::table('QuotationDetailHardware')
             ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
             ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
             ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
             ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailHardware"."HardwareID","HardwareName",
                              sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"QuotationDetailHardware"."Price"'))
             ->where('Quotation.QuotationID', $QuotationID)
             ->where('BranchID',$header[$i]->BranchID)
             ->groupby('QuotationDetailHardware.HardwareID')
             ->groupby('HardwareName')
             ->groupby('QuotationDetailHardware.Price')
             ->get();
             $item->hardware = $hardware;
             $license = DB::table('QuotationDetailLicense')
             ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
             ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
             ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
             ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
             ->select(DB::raw('"Quantity", "QuotationDetailLicense"."LicenseID","LicenseName","Free",
                              "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","QuotationDetailLicense"."Price"'))
             ->where('Quotation.QuotationID', $QuotationID)
             ->where('QuotationDetail.BranchID',$header[$i]->BranchID)
             ->get();
             $item->license = $license;
             $service = DB::table('QuotationDetailService')
             ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
             ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
             ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
             ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
             ->select(DB::raw('"Quantity", "QuotationDetailService"."ServiceID","ServiceName",
                              "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","QuotationDetailService"."Price"'))
             ->where('Quotation.QuotationID', $QuotationID)
             ->where('QuotationDetail.BranchID',$header[$i]->BranchID)
             ->get();
             $item->service = $service;
             $other = DB::table('QuotationDetailOther')
             ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailOther.QuotationDetailID')
             ->leftjoin('Other','QuotationDetailOther.OtherID','=','Other.OtherID')
             ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
             ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailOther"."OtherID","OtherName",
                              sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"QuotationDetailOther"."Price"'))
             ->where('Quotation.QuotationID', $QuotationID)
             ->where('BranchID',$header[$i]->BranchID)
             ->groupby('QuotationDetailOther.OtherID')
             ->groupby('OtherName')
             ->groupby('QuotationDetailOther.Price')
             ->get();
             $item->other = $other;
             $item->ShowName = $ShowName;
         }
     }
     else{
         $header = DB::table('Quotation')
         ->leftjoin('Brand', 'Quotation.BrandID','=','Brand.BrandID')
         ->leftjoin('Status','Quotation.StatusID','=','Status.StatusID')
         ->select(['Quotation.QuotationID','QuotationNO', 'Quotation.BrandID','BrandName','Brand.Contact','Brand.Email','GrandTotal','GrandTotalReseller','Brand.Address', 'Quotation.StatusID',
                   'StatusName','Quotation.QuotationDueDate','Quotation.QuotationDate','Brand.Phone',
                   'GrandTotalDiscount'])
         ->where('Quotation.QuotationID',$QuotationID)
         ->first();

         $user = DB::table('QuotationDetailUser AS QDU')
         ->leftjoin('User AS U','QDU.UserID','=','U.UserID')
         ->leftjoin('UserType AS UT', 'UT.UserTypeID','=','U.UserTypeID')
         ->select(['QDU.UserID','UserFullName','Username','Email','Phone','UT.UserTypeID'])
         ->where('QuotationID',$QuotationID)
         ->orderby('QuotationDetailUserID','asc')
         ->get();
         $header->User = $user;

         $UserTypeID = $user[0]->UserTypeID;
         $Permission = DB::table('UserTypePermission')
         ->where('UserTypeID',$UserTypeID)
         ->where('PermissionID',"CreateQuotationShowName")
         ->get();

         if(count($Permission) == 0)
         {     $ShowName = false;
         }
         else{
             $ShowName = true;
         }
         $item = $header;
         $hardware = DB::table('QuotationDetailHardware')
         ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailHardware.QuotationDetailID')
         ->leftjoin('Hardware','QuotationDetailHardware.HardwareID','=','Hardware.HardwareID')
         ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
         ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailHardware"."HardwareID","HardwareName",
                          sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"QuotationDetailHardware"."Price"'))
         ->where('Quotation.QuotationID', $QuotationID)
         ->groupby('QuotationDetailHardware.HardwareID')
         ->groupby('HardwareName')
         ->groupby('QuotationDetailHardware.Price')
         ->get();
         $item->hardware = $hardware;
         $license = DB::table('QuotationDetailLicense')
         ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailLicense.QuotationDetailID')
         ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
         ->leftjoin('License','QuotationDetailLicense.LicenseID','=','License.LicenseID')
         ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
         ->select(DB::raw('"Quantity", "QuotationDetailLicense"."LicenseID","LicenseName","Free",
                          "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","QuotationDetailLicense"."Price"'))
         ->where('Quotation.QuotationID', $QuotationID)
         ->get();
         $item->license = $license;
         $service = DB::table('QuotationDetailService')
         ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailService.QuotationDetailID')
         ->leftjoin('Branch','QuotationDetail.BranchID','=','Branch.BranchID')
         ->leftjoin('Service','QuotationDetailService.ServiceID','=','Service.ServiceID')
         ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
         ->select(DB::raw('"Quantity", "QuotationDetailService"."ServiceID","ServiceName",
                          "SubTotal", "DiscountTotal","DiscountTotal","QuotationDetail"."BranchID","BranchName","PriceTotal","QuotationDetailService"."Price"'))
         ->where('Quotation.QuotationID', $QuotationID)
         ->get();
         $item->service = $service;
         $other = DB::table('QuotationDetailOther')
         ->leftjoin('QuotationDetail','QuotationDetail.QuotationDetailID','=','QuotationDetailOther.QuotationDetailID')
         ->leftjoin('Other','QuotationDetailOther.OtherID','=','Other.OtherID')
         ->leftjoin('Quotation','Quotation.QuotationID','=','QuotationDetail.QuotationID')
         ->select(DB::raw('sum("Quantity") as Quantity, "QuotationDetailOther"."OtherID","OtherName",
                          sum("SubTotal") as SubTotal, sum("DiscountTotal") as DiscountTotal, sum("PriceTotal") as PriceTotal,"QuotationDetailOther"."Price"'))
         ->where('Quotation.QuotationID', $QuotationID)
         ->groupby('QuotationDetailOther.OtherID')
         ->groupby('OtherName')
         ->groupby('QuotationDetailOther.Price')
         ->get();
         $item->other = $other;
         $item->ShowName = $ShowName;

     }


      $endresult = array(
          'Status' => 0,
          'Errors' => array(),
          'Message' => "Success",
          'InvoicePreview' => $header
          );

       return Response()->json($endresult);

    }

    public function insertUpdateInvoice(request $request){
      $input = json_decode($request->getContent(), true);
      $rules = [
          'QuotationID' => 'required',
          'SplitBill' => 'required',
          'PONumber' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }
      // $result = $this->getInvoice2(false);
      // return $result;
      $ID = @$input['ProformaID'];
      $StatusID = "P";
      $year = date('Y');
      $month = date('m');
      $num = DB::table('Invoice')
      ->whereraw('extract(month from "DeliveryDate") ='.$month.' and extract(year from "DeliveryDate") ='.$year)
      ->max('ProformaNO');
      $num = substr($num,15);
      $num = $num+1;

      $DeliveryDate = now();
      $DueDate = Carbon::now()->addDays(10);
      $QuotationID = $input['QuotationID'];
      $SplitBill = $input['SplitBill'];
      if($ID == null){
          if($SplitBill === true){
              //hitung ada berapa branchnya.
              $Branch = DB::table('QuotationDetail')
              ->where('QuotationID',$QuotationID)
              ->select(['BranchID','Total','DiscountTotalBranch'])
              ->get();
              $BranchCount =  count($Branch);


              //insert tiap branch untuk jadi invoice nya.
              for($i = 0; $i<$BranchCount;$i++)
              {    $num = $num+$i;
                  $count = str_pad($num, 6, '0', STR_PAD_LEFT);
                  $code = 'HB-PINV-'.$year.'-'.$month.($count);
                  $InvoiceNO = $code;
                  // return $InvoiceNO;
                  $param = array(
                      'ProformaNO' => $InvoiceNO,
                      'QuotationID' => $QuotationID,
                      'DeliveryDate' => $DeliveryDate,
                      'DueDate' => $DueDate,
                      'StatusID' => "P",
                      'BranchID' => $Branch[$i]->BranchID,
                      'GrandTotal' => $Branch[$i]->Total+@$input['Delivery'],
                      'GrandTotalDiscount' => $Branch[$i]->DiscountTotalBranch,
                      'PONumber' => $input['PONumber'],
                      'Note' => @$input['Note'],
                      'Delivery' => @$input['Delivery']
                  );
                  // return $Branch;
                  $result = DB::table('Invoice')
                  ->insert($param);
                  $ID[$i] = $this->getLastVal();
                  $StatusQuotation = "I";
                  $StatusInvoice = "P";
                  $result = DB::table('Invoice')
                  ->where('ProformaID', $ID[$i])
                  ->update(array('DeliveryDate' => $DeliveryDate,'DueDate' => $DueDate, 'StatusID' => $StatusInvoice));

                  $StatusQuotation = DB::table('Quotation')
                  ->where('QuotationID',$QuotationID)
                  ->update(array('StatusID' => $StatusQuotation));

              }

          }
          else{
              $Total = DB::table('Quotation')
              ->where('QuotationID',$QuotationID)
              ->select(['GrandTotal','GrandTotalDiscount'])
              ->get();


              $num = $num;
              $count = str_pad($num, 6, '0', STR_PAD_LEFT);
              $code = 'HB-PINV-'.$year.'-'.$month.($count);
              $InvoiceNO = $code;

              $param = array(
                  'ProformaNO' => $InvoiceNO,
                  'QuotationID' => $QuotationID,
                  'DeliveryDate' => $DeliveryDate,
                  'DueDate' => $DueDate,
                  'StatusID' => "P",
                  'GrandTotal' => $Total[0]->GrandTotal+@$input['Delivery'],
                  'GrandTotalDiscount' => $Total[0]->GrandTotalDiscount,
                  'PONumber' => $input['PONumber'],
                  'Note' => @$input['Note'],
                  'Delivery' => @$input['Delivery']
              );
              $result = DB::table('Invoice')
              ->insert($param);
              $ID[0] = $this->getLastVal();
              $StatusQuotation = "I";
              $StatusInvoice = "P";
              $result = DB::table('Invoice')
              ->where('ProformaID', $ID[0])
              ->update(array('DeliveryDate' => $DeliveryDate,'DueDate' => $DueDate, 'StatusID' => $StatusInvoice));

              $StatusQuotation = DB::table('Quotation')
              ->where('QuotationID',$QuotationID)
              ->update(array('StatusID' => $StatusQuotation));

          }

      }
      else{
          $code = 'HB-PINV-'.$year.'-'.$month.($count);
          $InvoiceNO = $code;

          $param = array(
              'ProformaNO' => $InvoiceNO,
              'QuotationID' => $QuotationID,
              'DeliveryDate' => $DeliveryDate,
              'DueDate' => $DueDate,
              'StatusID' => "P",
              'GrandTotal' => @$input['GrandTotal']+@$input['Delivery'],
              'BranchID' => @$input['BranchID'],
              'GrandTotalDiscount' => @$input['GrandTotalDiscount'],
              'Note' => @$input['Note'],
              'Delivery' => @$input['Delivery']
          );
          $result = DB::table('Invoice')
          ->insert($param);
          $ID[0] = $this->getLastVal();
          $StatusQuotation = "I";
          $StatusInvoice = "P";
          $result = DB::table('Invoice')
          ->where('ProformaID', $ID)
          ->update(array('DeliveryDate' => $DeliveryDate,'DueDate' => $DueDate, 'StatusID' => $StatusInvoice));

          $StatusQuotation = DB::table('Quotation')
          ->where('QuotationID',$QuotationID)
          ->update(array('StatusID' => $StatusQuotation));


      }



          $result = $this->checkReturn($result);
          $result['ProformaID'] = $ID;
          return Response()->json($result);

    }

    public function updateInvoiceStatus(request $request){
      $input = json_decode($request->getContent(), true);
      $rules = [
          'ProformaID' => 'required',
          'Status' => 'required'
      ];

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          $errors = $validator->errors();
          $errorList = $this->checkErrors($rules, $errors);
          $additional = null;
          $response = $this->generateResponse(1, $errorList, "Please check input", $additional);
          return response()->json($response);
      }

      $ProformaID = $input['ProformaID'];
      $Status = $input['Status'];
      $DeliveryDate = now();
      $DueDate = Carbon::now()->addDays(30);

      if($Status == "S")
      {$result = DB::table('Invoice')
      ->where('ProformaID', $ProformaID)
      ->update(array('DeliveryDate' => $DeliveryDate,'DueDate' => $DueDate, 'Status' => $Status));
      }
      Elseif($Status == "ID")
      {$result = DB::table('Invoice')
      ->where('ProformaID', $ProformaID)
      ->update(array('Status' => $Status));}

          $result = $this->checkReturn($result);
          return Response()->json($result);
    }

     public function DeleteQuotation(Request $request){
       $input = json_decode($this->request->getContent(),true);
       $QuotationID = @$input['QuotationID'];
       $result = DB::table('Quotation')->where('QuotationID', $QuotationID)->update(array(
                  'Status' => 'D',
           ));

      $result = $this->checkReturn($result);

      return Response()->json($result);
    }
}
